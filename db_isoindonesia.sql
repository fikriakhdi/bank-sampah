-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 05 Nov 2019 pada 18.18
-- Versi server: 10.4.8-MariaDB
-- Versi PHP: 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_isoindonesia`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `faq`
--

CREATE TABLE `faq` (
  `id` int(11) NOT NULL,
  `question` varchar(100) NOT NULL,
  `answer` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `faq`
--

INSERT INTO `faq` (`id`, `question`, `answer`) VALUES
(1, 'Pakah yang dimaksud dengan sampah organik', 'sampah yang bisa diolah kembali menjadi sesuatu yang bermanfaat');

-- --------------------------------------------------------

--
-- Struktur dari tabel `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `content` text NOT NULL,
  `images` text NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateModified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `news`
--

INSERT INTO `news` (`id`, `title`, `content`, `images`, `dateCreated`, `dateModified`) VALUES
(2, 'Berita terkini', 'Berita terbaru hari ini adalah maling ternyata dimaling', 'assets/img/5db23b71662fe.jpg', '2019-10-25 07:01:53', '0000-00-00 00:00:00'),
(3, 'Kambaniru Hotel Indah', 'Hotel terbaik sepanjang masa dan tambang kenangan', 'assets/img/5db2fa415756e.jpg', '2019-10-25 20:36:01', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `point`
--

CREATE TABLE `point` (
  `id` int(11) NOT NULL,
  `userId` int(11) DEFAULT NULL,
  `pointValue` float NOT NULL,
  `type` enum('in','out') NOT NULL,
  `description` text NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateModified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `point`
--

INSERT INTO `point` (`id`, `userId`, `pointValue`, `type`, `description`, `dateCreated`, `dateModified`) VALUES
(7, 22, 10.8, 'in', 'Get Point From Trash Request', '2019-11-05 20:58:25', '2019-11-05 20:58:25'),
(8, 21, 3.6, 'in', 'Get Point From Trash Request', '2019-11-05 20:58:25', '2019-11-05 20:58:25'),
(9, 0, 3.6, 'in', 'Get Point From Trash Request', '2019-11-05 20:58:25', '2019-11-05 20:58:25'),
(10, 22, 10, 'out', 'Redeem', '2019-11-05 21:20:45', '2019-11-05 21:20:45'),
(11, 22, 10, 'out', 'Redeem', '2019-11-05 21:21:29', '2019-11-05 21:21:29');

-- --------------------------------------------------------

--
-- Struktur dari tabel `redeem`
--

CREATE TABLE `redeem` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `type` enum('product','cash') NOT NULL,
  `status` enum('pending','accepted','canceled') NOT NULL,
  `redeemCategoryId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateModified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `redeem`
--

INSERT INTO `redeem` (`id`, `userId`, `type`, `status`, `redeemCategoryId`, `dateCreated`, `dateModified`) VALUES
(5, 22, 'product', 'accepted', 2, '2019-11-05 21:21:29', '2019-11-05 21:25:11');

-- --------------------------------------------------------

--
-- Struktur dari tabel `redeemcategory`
--

CREATE TABLE `redeemcategory` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `description` text NOT NULL,
  `images` text NOT NULL,
  `price` float NOT NULL,
  `pointRequired` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateModified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `redeemcategory`
--

INSERT INTO `redeemcategory` (`id`, `title`, `description`, `images`, `price`, `pointRequired`, `dateCreated`, `dateModified`) VALUES
(2, 'Gunting', 'Gunting Stainless Steel', 'assets/img/5db3e78730f6a.jpg', 10000, 10, '2019-10-26 13:28:23', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `session`
--

CREATE TABLE `session` (
  `id` int(11) NOT NULL,
  `key` varchar(100) NOT NULL,
  `userId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateModified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `session`
--

INSERT INTO `session` (`id`, `key`, `userId`, `dateCreated`, `dateModified`) VALUES
(6, '255dbf01f708e13', 2, '2019-11-03 23:36:07', '2019-11-03 23:36:07'),
(7, '25dbf23a881253', 2, '2019-11-04 01:59:52', '2019-11-04 01:59:52'),
(8, '435dbf23bf31bcc', 21, '2019-11-04 02:00:15', '2019-11-04 02:00:15'),
(9, '875dbf24059f1ec', 21, '2019-11-04 02:01:25', '2019-11-04 02:01:25'),
(10, '55dbf640c5a56d', 21, '2019-11-04 06:34:36', '2019-11-04 06:34:36'),
(11, '385dbf646a2b239', 21, '2019-11-04 06:36:10', '2019-11-04 06:36:10'),
(12, '85dc0be0b15365', 21, '2019-11-05 07:10:51', '2019-11-05 07:10:51'),
(13, '525dc0c4a59684f', 22, '2019-11-05 07:39:01', '2019-11-05 07:39:01'),
(14, '635dc1837873a52', 22, '2019-11-05 21:13:12', '2019-11-05 21:13:12');

-- --------------------------------------------------------

--
-- Struktur dari tabel `setting`
--

CREATE TABLE `setting` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `value` text NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateModified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `setting`
--

INSERT INTO `setting` (`id`, `name`, `value`, `dateCreated`, `dateModified`) VALUES
(1, 'verification', '<html style=\"width:100%;font-family:arial, \'helvetica neue\', helvetica, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0;\"><head> \r\n          <meta charset=\"UTF-8\"> \r\n          <meta content=\"width=device-width, initial-scale=1\" name=\"viewport\"> \r\n          <meta name=\"x-apple-disable-message-reformatting\"> \r\n          <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\"> \r\n          <meta content=\"telephone=no\" name=\"format-detection\"> \r\n          <title>New email template {{year}}-09-13</title> \r\n          <!--[if (mso 16)]>\r\n            <style type=\"text/css\">\r\n            a {text-decoration: none;}\r\n            </style>\r\n            <![endif]--> \r\n          <link rel=\"shortcut icon\" type=\"image/png\" href=\"https://stripo.email/assets/img/favicon.png\"> \r\n          <style type=\"text/css\">\r\n        @media only screen and (max-width:600px) {p, ul li, ol li, a { font-size:16px!important; line-height:150%!important } h1 { font-size:30px!important; text-align:center; line-height:120%!important } h2 { font-size:26px!important; text-align:center; line-height:120%!important } h3 { font-size:20px!important; text-align:center; line-height:120%!important } h1 a { font-size:30px!important } h2 a { font-size:26px!important } h3 a { font-size:20px!important } .es-menu td a { font-size:16px!important } .es-header-body p, .es-header-body ul li, .es-header-body ol li, .es-header-body a { font-size:16px!important } .es-footer-body p, .es-footer-body ul li, .es-footer-body ol li, .es-footer-body a { font-size:16px!important } .es-infoblock p, .es-infoblock ul li, .es-infoblock ol li, .es-infoblock a { font-size:12px!important } *[class=\"gmail-fix\"] { display:none!important } .es-m-txt-c, .es-m-txt-c h1, .es-m-txt-c h2, .es-m-txt-c h3 { text-align:center!important } .es-m-txt-r, .es-m-txt-r h1, .es-m-txt-r h2, .es-m-txt-r h3 { text-align:right!important } .es-m-txt-l, .es-m-txt-l h1, .es-m-txt-l h2, .es-m-txt-l h3 { text-align:left!important } .es-m-txt-r img, .es-m-txt-c img, .es-m-txt-l img { display:inline!important } .es-button-border { display:inline-block!important } a.es-button { font-size:20px!important; display:inline-block!important; border-width:6px 25px 6px 25px!important } .es-btn-fw { border-width:10px 0px!important; text-align:center!important } .es-adaptive table, .es-btn-fw, .es-btn-fw-brdr, .es-left, .es-right { width:100%!important } .es-content table, .es-header table, .es-footer table, .es-content, .es-footer, .es-header { width:100%!important; max-width:600px!important } .es-adapt-td { display:block!important; width:100%!important } .adapt-img { width:100%!important; height:auto!important } .es-m-p0 { padding:0px!important } .es-m-p0r { padding-right:0px!important } .es-m-p0l { padding-left:0px!important } .es-m-p0t { padding-top:0px!important } .es-m-p0b { padding-bottom:0!important } .es-m-p20b { padding-bottom:20px!important } .es-mobile-hidden, .es-hidden { display:none!important } .es-desk-hidden { display:table-row!important; width:auto!important; overflow:visible!important; float:none!important; max-height:inherit!important; line-height:inherit!important } .es-desk-menu-hidden { display:table-cell!important } table.es-table-not-adapt, .esd-block-html table { width:auto!important } table.es-social { display:inline-block!important } table.es-social td { display:inline-block!important } }\r\n        #outlook a {\r\n          padding:0;\r\n        }\r\n        .ExternalClass {\r\n          width:100%;\r\n        }\r\n        .ExternalClass,\r\n        .ExternalClass p,\r\n        .ExternalClass span,\r\n        .ExternalClass font,\r\n        .ExternalClass td,\r\n        .ExternalClass div {\r\n          line-height:100%;\r\n        }\r\n        .es-button {\r\n          mso-style-priority:100!important;\r\n          text-decoration:none!important;\r\n        }\r\n        a[x-apple-data-detectors] {\r\n          color:inherit!important;\r\n          text-decoration:none!important;\r\n          font-size:inherit!important;\r\n          font-family:inherit!important;\r\n          font-weight:inherit!important;\r\n          line-height:inherit!important;\r\n        }\r\n        .es-desk-hidden {\r\n          display:none;\r\n          float:left;\r\n          overflow:hidden;\r\n          width:0;\r\n          max-height:0;\r\n          line-height:0;\r\n          mso-hide:all;\r\n        }\r\n        </style> \r\n         <meta property=\"og:title\" content=\"New email template {{year}}-09-13\"><meta property=\"og:description\" content=\"{{appName}}. Home Order Create Quest Hey {{user_name}}! We received a request to set your email to {{user_email}}. If this is correct, please confirm by clicking the button \"><meta property=\"og:url\" content=\"https://viewstripo.email/template/bedecdef-d597-49e6-99df-44daf6de31e0\"><meta property=\"og:type\" content=\"article\"></head> \r\n         <body cgbdi5ohajrqjr9chvixsudtxatjxoyy8nxovzjwn5k3qt2qdno8hmd1gnxvw831b=\"bXiSN7uTcnHmZIdorVjeL5\" style=\"width:100%;font-family:arial, \'helvetica neue\', helvetica, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0;\"> \r\n          <div class=\"es-wrapper-color\" t3utroc9ipufga7mm3jo1sm4gmpn5g7x8jndhy2rtreolxfybejd50ayhdgqy2e=\"MPHrs7hsLxc6zsy70wG0wD2BaTlbTEDYOacQRf6ZNlRCagAc2MIHFpazHQ8qMMJOPh522do9\" style=\"background-color:#FFFFFF;\"> \r\n           <!--[if gte mso 9]>\r\n              <v:background xmlns:v=\"urn:schemas-microsoft-com:vml\" fill=\"t\">\r\n                <v:fill type=\"tile\" color=\"#ffffff\"></v:fill>\r\n              </v:background>\r\n            <![endif]--> \r\n           <table class=\"es-wrapper\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" jzj15ojrwnmnle0ms3kwmi6b8t0csx4hy9cqmahoduypzntvodu15grr3jfdajtlo6xpynsouqo93ndw5trunyrz4=\"22nVwYlKzZ4JLGpwi7PQOnjLdH9LuFUPwhmAJ1MkPWMcVr3WcdNJ0UbnPlN8Z6iK21TEpA9pDnwY6P5UEDokx\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;padding:0;Margin:0;width:100%;height:100%;background-repeat:repeat;background-position:center top;\"> \r\n            <tbody c2lnne2cwyd5kae46clkcojwwgwyi1aqqamjl1zi1zioa4kpjqnmqxqjkmceyu8rj3epqzpqudxbi4bajjy4owvzliivewwt=\"8JPTW3YOBAMEooYS7gop08PqSUqbLo1Y6wfB91d29pPXmIApNhwsHHN8DGM80Mbt\"> \r\n             <tr um83bervv4o1hmddqslvptbko6clfqtnok5r7n63=\"mtYPJQ9wwiHCq5XZg2mFo8grY2jLuE4uzobJb2R3jAA\" style=\"border-collapse:collapse;\"> \r\n              <td valign=\"top\" suih6aesqbxbfrs0vpgdaz=\"RF4RbWi4bIdNQlOytNklB4PWeOUo6w7Rr9O2RH4Kt1kkIvHaYV5NbdGGjrHmXEjhR0V\" style=\"padding:0;Margin:0;\"> \r\n               <table class=\"es-header\" align=\"center\" cellspacing=\"0\" cellpadding=\"0\" dz1oloj4gwzimmk9m6gpzz7f0uqzwfkfavsze0ihkrw5i57bmcnvj3iometlipg2desucjfjm=\"4jPPFvrPKPJSyBkD5BrXKQH1eBB6wrErQOl1x0Odl54KkkcuYXFstcsipg9BDf5bv\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;background-color:transparent;background-repeat:repeat;background-position:center top;\"> \r\n                <tbody tlntwghgb9haptsg2jiwt6pwz82ejahkof9ygzfzcvnogoqf=\"UKcJtIfwQ78CukzFaXiCdzxzJYtGfJcgxjxQFS2yekLRkYn905KUR6taZygegSFFrpusRkfW6A67c\"> \r\n                 <tr n91vv3mhwe84d40fn0awkwqpxkj7afnlwwpkyctazjbhcuxbuksblofm8=\"4uwuExwgOjeGWm7d8F4EbXFOvcNGVL6LhciLSz44AWjzcg4QizT\" style=\"border-collapse:collapse;\"> \r\n                  <td class=\"es-adaptive\" align=\"center\" wjcgsdpva9aomeo3v0wxwojopa4y3mtro2dz4ro2qf3fi4rk8qcv3rbdvgjswqodj9oeufv54fy2c0bdsuls2tklccxngt0=\"gt1zbNImJgCZY6EJEBBUD6whJ9UkWPtcXvdCnAZdR5LNYTWLyQW4Zh1A7rKp49DAnUCe6ZPE\" style=\"padding:0;Margin:0;\"> \r\n                   <table class=\"es-header-body\" align=\"center\" width=\"600\" cellspacing=\"0\" cellpadding=\"0\" lr6eihpczfm8r62afqe4au2ka3flhgxlelurmryixy9p7ib6gewbyo8gdz3bqmuppsldg9xdt3p5zx=\"WdoJlfWlzqxmUXdFJ5z9WV5MNHeNATUWV6rWhZQ2HgQ3JtBBeIimOSFke\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:transparent;\"> \r\n                    <tbody 1ajniodeqgbe94zhdirmizryja1i5lyzkbcasyp6dx1m7nuxrwcoiscnkutt3lt8u4rdjhdrgj60ewro=\"9o0fo3kvZjwz4wjzOckEbIIXPUpqmEVG1PNqr7NfDBJr5tN7ejZVCobWNF3AQvI11n3FNMO5w4S0EjwmWfX\"> \r\n                     <tr 9qs7dyw3ha5kgklgqstorashj46dtto6grpe6brbi3dka9ujevgerej=\"Ugn6bjbnvjAW401KM9EN0Zhli2W77x0e0Gp1lKVpX8PznujXHQJDY9InPfIgw2iyGmlfIcdAT1j0b4n2\" style=\"border-collapse:collapse;\"> \r\n                      <td align=\"left\" vxowgelowdcqwg5vuwjkmerjqfl0jkyvpkpfwiotlk=\"nT05S7Z2GqCuVv0T2mnd\" style=\"Margin:0;padding-bottom:10px;padding-left:20px;padding-right:20px;padding-top:25px;\"> \r\n                       <!--[if mso]><table width=\"560\" cellpadding=\"0\"\r\n                                    cellspacing=\"0\"><tr><td width=\"205\" valign=\"top\"><![endif]--> \r\n                       <table class=\"es-left\" align=\"left\" cellspacing=\"0\" cellpadding=\"0\" a5bjasnfmkghdgiwzyjzu7vavrabiyml6dgadphr7kjh78p3qcua8bx4wdit6kzahjumsgcfmygtk1ctppvpadp=\"SC85USM0ImvvQ74azckNisPYXvsC4Qhq3TFu08yunEU1taQ1u\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left;\"> \r\n                        <tbody fkr3hddijvwdkalckl9vhf7sxxpqzaylmx75ortgpvotyev6fhlhyelwitaxtmlkuvx=\"eAyOpxhICpiyv1fmuUTusTHxQqPo7drkMPmphSxOjO7hOS2CHuUn2\"> \r\n                         <tr wnr6ioqqn23fzogxjxtseyxflzsmxdftevhmdxjqd0ulk3zhn3xohihmb8=\"lSKl3AQAcv9zBY29sCAGLmn65E74o7smS4hBkqNQIhB25djUFl3Nyel47IaSSWMdUejQZTphioXI1slLNA5N1fNXrFkk\" style=\"border-collapse:collapse;\"> \r\n                          <td class=\"es-m-p0r es-m-p20b\" align=\"center\" width=\"1000\" valign=\"top\" efumij2967rt4detnaps1qiortohuixpa1rsgnoxuhllmtvkyojhiqpp6f8wxlfcjgez8f6lzxhk4ggct5kajxi=\"3dbgGRW13ctz4zUjfbl6\" style=\"padding:0;Margin:0;\"> \r\n                           <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" xsxxuepncw3ru3aucjxinsykg95kpu9dbqkqt0haz2lj7lloz5ltahvumbjqjsmodynil=\"fkVTI8wQCyebce2g8V5UjjyZ3CVP58OAEI9koJDQBux8CrPWuzntwMkCOpAQztc9X\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n                            <tbody lokryrkk8w7qpgjqh1klmucr2xigq4teeldwhr=\"ru5L9PUTElBR8gF47dC8URTGAW8k00bksk7cHFQ3FmhzxdtPV5jP21BwnL6ck5Tl32phYieHbcnK8Hsf37DVq1ZIHkdd2gye\"> \r\n                             <tr ngf301rb6dn997lwmbqzkbc6yo9i2mukk=\"gFyW9Emyzy9cqv8qjHd8qymrj0iyw2uAOPCe8BW2IuEiTMdJ7d25Ch3c5uaXwqKX76\" style=\"border-collapse:collapse;\"> \r\n                              <td align=\"center\" 1xj2waexvcpyojy3nt1tmu6xlicyw87msb6cujqknaw8yizz1xnmgnaok9ehxlyi9jvzjkwqcvuv9e7lktoiilnihmr7zrihupn=\"reQfRNGrZYUOmrAe1uQzhsm8JEygtuYH2SGqXgQ2h1xIykMOwUbhxXKOjQqya\" style=\"padding:0;Margin:0;padding-left:15px;\"><p style=\"Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:20px;font-family:arial, \'helvetica neue\', helvetica, sans-serif;line-height:30px;color:#333333;\" hllpqwqbgyoy50erkrdnja7smp28tckgrwphxfbcsklwmtk41ycvzqpwfqulosfax=\"uEBZn2C811k41dA7ZZY4\"><strong rdf54hgvhuxrdxfkzcioh36xo=\"DkZfPeNwX7cGXyVnexxk8XFvF5BdZPutZbTHCJnCQj7gQQytB10hro\">{{appName}}.</strong></p></td> \r\n                             </tr> \r\n                           </tbody></table></td> \r\n                         </tr> \r\n                       </tbody></table> \r\n                       <!--[if mso]></td><td width=\"10\"></td><td width=\"345\" valign=\"top\"><![endif]--> \r\n                        \r\n                       <!--[if mso]></td></tr></table><![endif]--></td> \r\n                     </tr> \r\n                   </tbody></table></td> \r\n                 </tr> \r\n               </tbody></table> \r\n               <table class=\"es-content\" align=\"center\" cellspacing=\"0\" cellpadding=\"0\" pwihpdzfkd17lehmusdlw=\"1f1LiVq0F7X9GIFRe9E1Nln7BZWuGvH6q7jXo1qKG0ogzASakvmqDkq\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;\"> \r\n                <tbody cijswsdlvln1psj1l1jnxiubcu1htwnyrnoe00g9gawc3fvjsmjyvftsovfv=\"iz2Q8i5N6d25gEPvVE7GCpS6M8Cr8fPC4p5l1a\"> \r\n                 <tr dxwj6rvmczcapxpek0uw3zznupytnjojgn41smwmn8cadajnzhkrtxfpifzjcjajzi1ogfkimhyziosh4csli4=\"8LFeoItyjGPy9LN1uE0f5Y8gTcdG2bhwayzgZj5NeEm8VBe4qrIMR02YMdF1t5D\" style=\"border-collapse:collapse;\"> \r\n                  <td align=\"center\" uynweqdkm8d5qmywgjhgsi46vc6uf9iqa8bsrxmil=\"SGCuTseE7B00K5GHYPwl6kVIHd4sRC09UHyYvmLAQPOoODi89wSSU\" style=\"padding:0;Margin:0;\"> \r\n                   <table class=\"es-content-body\" align=\"center\" width=\"600\" cellspacing=\"0\" cellpadding=\"0\" bgcolor=\"#ffffff\" p0uurzxjc0rg0hsncu2brjuwhbs32epimpbe2ulnei7ggmnla7oglyo0lhbiphiyoyutdk=\"8mWGvfutsEXDAyWoZWD62iGoq8ZmnI3NUyB\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;\"> \r\n                    <tbody gnir2qne61ml3cdez9adh19xcmzic75xniszxzkzy2kxnw37ib760imrzqibkr2gatlvkqd=\"gpF6JkblIlJaDRIblKbcEQSi8RfIJ4aBdJnhU1qFY24QPD8YNFYv7rY2eazDQxc4RJK8VOX91HH3PUhED42b\"> \r\n                     <tr gzdlmbldssmqsy3g9f0eevhpajsevo9=\"ZDrmtl4OK5PNnanFKa7U5RD8Z3gIvEqQV89XdNuF\" style=\"border-collapse:collapse;\"> \r\n                      <td align=\"left\" grlynsbu8d0mber1vfjo7qd6hf4xm4l3=\"UMPNC9Sor0T83rejPGBxTPg0qOK8ekLWXPNLZwkteIGtELVqBUNCgB7UrkAvPPQAOjNlFwN5bT5PdJU1efr1ncL32pTy7ImEg\" style=\"padding:0;Margin:0;padding-top:20px;\"> \r\n                       <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" zx5qcxdnux98nnoifvvmizy0jyxxnkhvqv9ts7j70qcsucubkl=\"oieQxkeC8ZYTbPL5O26eIdG7Tyv7E0RQzTHowIsJoD254Kd5nlO\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n                        <tbody l6a6pya2bj5rnw5xt9xwa2qdmirjcaphnjl7wemrnpkcijijfpwzwf7cupjo1ihtq8jcu5mvc9afnp4sb8lsmg=\"DXXf8ZT1c1uXWlozlAUtFaP905bdPb2AUjKk3zRoy3\"> \r\n                         <tr m7nyasi0ef0wjlydavvtq8zrloxcoqkenxwpgbrf=\"9iEonHdELQFAewvtoOg0ZRQs1ftQT3VNc2f\" style=\"border-collapse:collapse;\"> \r\n                          <td align=\"center\" width=\"600\" valign=\"top\" gu3n9fammet3iak9baet9dpthbqefabw0pantu1fxctj2ft0sbsca2ji66gd9hyxfvzppbbqcnrz9yh=\"M6BjOTAFkhA0XIku98uNCc9EEXWqmrlpWx8hUyggkuk1D\" style=\"padding:0;Margin:0;\"> \r\n                           <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" uqjxgd57otadnqwf800c7thb8tdozhrhvpciwwcuixgxgpgfgnb4kk2wypgdmmg2jq8yhascbksoliic7h3db1jif=\"bYZueGR2lXgIcaa6gHG3DsKJ6fr5Bayj3vUuYbJ0OiWis6uHBqubc9JNDkiq4qkqYVHKgrCYxo4RgSZm\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n                            <tbody xow8y9svwjyhkurpymwvi6zqgfqux3pnucrtzzidaczlqtwokesq8o3wmiamweo=\"t3DzoAg72SpAg0JxRnUa4Vpan3lyZT7w2Vd7UTvdCYlx2c86kWpF\"> \r\n                             <tr jrgx7gyqeragbc1y2gcg9jusqyatevkri0kopamucrri4gd3o7hhmdww32tflvx0m2poqtzyrl1kr5qhk6gb3gl9k=\"xdhKaULJpyUiiEWQ2zANmPW6Avc3FdmWZpvAEU6YVpDG4\" style=\"border-collapse:collapse;\"> \r\n                              <td align=\"center\" 4tafxakmgx8uq0ggsrcsufwzg76=\"nsnEWONMx8BvzBn6zKg3eJ4qBkBV6OtPBv8D5HCacF3nrRWQzWApVkamV36VfUFftl38aDRF8xRt0385gWEOTBdr\" style=\"padding:0;Margin:0;padding-left:20px;padding-right:20px;\"><img class=\"adapt-img\" src=\"https://i.ibb.co/vJfGRzG/welcome.png\" alt=\"Image\" style=\"display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;\" title=\"Image\" width=\"260\" egxjd5sy44mvw6zkbt5p=\"dgoGvHOqaHMCsWoIABnbbGuodJhZ\"></td> \r\n                             </tr> \r\n                           </tbody></table></td> \r\n                         </tr> \r\n                       </tbody></table></td> \r\n                     </tr> \r\n                   </tbody></table></td> \r\n                 </tr> \r\n               </tbody></table> \r\n               <table class=\"es-content\" align=\"center\" cellspacing=\"0\" cellpadding=\"0\" wr6uzayntpow6ifpuoqhm57raobzce3wg1ger96l9jwg6bcixn39g74xqyfmwen=\"YgCbwPt6nWjf1hbqLhcmt9\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;\"> \r\n                <tbody og98l31obdakjxuaa79djfakwqbtcvcdgjes2oy05kvl5f84kwvaijhuj9pzvd6jztmwgn0dbzxlb1rkr=\"kQQZHZY8jup58WRivyfD7768j4OmcoRbiEKq7SVspylWsNhmz\"> \r\n                 <tr zmdikksn5sjjxquir1djs9esni21acbmrhxxvpxil6qo6qlfq=\"qSdJGc4zKXTO5Y5ji9GFmBsLXEcQfRA\" style=\"border-collapse:collapse;\"> \r\n                  <td align=\"center\" zsgbfblpuibdnuf0xeo7lgdc8q1xqgvdlndjgpxp6kovp=\"YT5S1zBG91PTBzFEA2gkiYZ65nUvQbEisWYaBn2MVPzgYa7vuO0IbEWZm7oin8k4rkpBdpe1KtB6NMrBcTYD\" style=\"padding:0;Margin:0;\"> \r\n                   <table class=\"es-content-body\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;border-left:1px solid transparent;border-right:1px solid transparent;border-top:1px solid transparent;border-bottom:1px solid transparent;\" align=\"center\" width=\"600\" cellspacing=\"0\" cellpadding=\"0\" bgcolor=\"#ffffff\" 4mfdnrfd9ifi7t5au4jhlf9azovb=\"qgTBjLHZXichEjw6GWYZjUwokpCWXceiuJMDpNHSGn\"> \r\n                    <tbody nia2cyluogshhyaqh8amhu1seort3eboyfyakcjllvknzy1msoxquirklgo=\"piPwtOF3Jkq2SwhyaMdVe8U2ADSW5T\"> \r\n                     <tr v08srd9jv3lba1jsgozvf8yfzb66rxdvwxaietl4dkjwrqjgrkhd5jybrkvmzmyqxb0esvpfdyppj=\"G9XuGHEXlW1PHkLOKd1g1kMVueDwnpWwOPtelye6hYbQDwXjbLP\" style=\"border-collapse:collapse;\"> \r\n                      <td align=\"left\" ihopdgylfhehuh4vli5uzlnekwswspayf5liudnosnf09no6xe=\"H3PESuJgsxJYdRXGGeOr8NhubNWuLaVl4aO1WzC13kkWfrcQDBdCtvLeOcOgYpo4FJ1rtxsva2gi05dwLS0fDQ0PKTc\" style=\"Margin:0;padding-top:20px;padding-bottom:40px;padding-left:40px;padding-right:40px;\"> \r\n                       <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" bfjjcivf3ix12v0phpvrwclzfnczptsayu8ws1xhosiiiifgilvsrohaaclum6cm115wbrnqn=\"h16InkLrcpH1Yl9QKxbKWaGAKNGr5NbVFusqZUnVmnUAX2OpEM7F6Xid7q3sQfJodUJWqJKP\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n                        <tbody v7sodceg1cffusefsgljc3wyeklztar0vjdx95bheebjnq9j310e8r3866qpmevu9yy2dnthjzh0u=\"5YK455zN4lM7iC6lBypqgfteZUCL7rSAcQjEqDyz3ebc3YYJU\"> \r\n                         <tr irzn6aqzq1u1pasxva2dzxedwf1hq5temn6cu7ldlogedtsv3x5uvernh5dok8yzb0=\"VZYFAFQdF6Z1aT9sjjareW7QoZljNgL\" style=\"border-collapse:collapse;\"> \r\n                          <td align=\"left\" width=\"518\" wqxyyeffic0ucc2zep9ggy1bvvoabnsg1kiigzdkza0qzdfaojvcmcg0bbueb5rgpyxnk7nvkghiuwqoeh9ojwusz=\"5vkyuqd7RL3Fyee3ZIs8pS7dJCL7IdLKgsgoKmxiUSGW7tTO5yBjie3CtzzY8Ds999BNV0yRM14vDJ0\" style=\"padding:0;Margin:0;\"> \r\n                           <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" gfkija4qovjhikty15bngddhzq6jmorcwu=\"Tmpzy2R4xxJuAdeS1DtgpTJPewMW4ZYDfoJ9YRROPz7cm24kKJuWjDdHmPschnpx39Zi2vLzbLO\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n                            <tbody xileubpldvbieutfuqk8lvoywdypxzcbw0xyyt3knz8so=\"P0qkOc3f6NotjbSIKqS3EmT0iwH0GhN54qgb92aZs16IZqh33b8RHbvCzPfstB7DDutHmFjV2BH7kbZLtcciVKvk6L\"> \r\n                             <tr wbv29piakqbjja0oillfqnn7ik3ujianwyeiaofqa7rb9v6zkmbno7=\"Eoktgtk3AubEDhyZ3AQEPxKJACDpUkBQ1RncbgYyuXHPjpnjPMo2g3aMg4ef2gx7G\" style=\"border-collapse:collapse;\"> \r\n                              <td class=\"es-m-txt-c\" align=\"center\" uclej9fvctbtbm1vpk8qkn75xtd2hlaeuqppfrwrcjermiuqzy0ifljd02av86dgkw2t1xdokq7=\"s7CkeCMDAH9MonrCkcgw3t5lgKagPwxnKqIFvTaheMnolFV1icM6f4dUx6WP0sf8Ph93jRH0yxaOmyK\" style=\"padding:0;Margin:0;\"><h2 q785vflpc5fawcjrjr2ucsx12wph5rv8ftu6npyerm4gelqsxgged0lbizxqafdr8=\"eK2gCEfQ9y7oa4iaHekPlR6LT6BlIFofCTP\" style=\"Margin:0;line-height:29px;mso-line-height-rule:exactly;font-family:arial, \'helvetica neue\', helvetica, sans-serif;font-size:24px;font-style:normal;font-weight:normal;color:#333333;\">Hey {{user_name}}!</h2></td> \r\n                             </tr> \r\n                             <tr hhibozjiobas0suvpp1xzuw5owxwqvdefvva6om3rho=\"zb7ysEQgbtcCdIGGguOS8jTlsBxkb2qH9OCCymB8ow\" style=\"border-collapse:collapse;\"> \r\n                              <td class=\"es-m-txt-c\" align=\"center\" ebyrkuyilgkodc7ibswhpw0ubpvht4zunyfghjpa3nbxs6sxt5qa5r3tdbyb1ofkqnbnxd3xripuyvasytxv9cj7agf=\"d8SwilIbGapULokIXUITTXBbrJ9ib3MataYVrhnB7hU\" style=\"padding:0;Margin:0;padding-top:15px;\"><p 8seqqa1iunsnj0n74hrpknwsrufncreoiwlxxhedla9toqofko6mpx4tvsjuatqnbmz3swecx737pozg0=\"Pp3IUhQ5owVuwNOluQ0xQfKHbISVcCiFULzjkio5GkTEvgpF\" style=\"Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial, \'helvetica neue\', helvetica, sans-serif;line-height:21px;color:#333333;\">We received a request to set your email to {{user_email}}. If this is correct, please confirm by clicking the button below. If you do not know why you got this email, please tell us straight away so we can fix this for you.</p></td> \r\n                             </tr> \r\n                             <tr hsxcgok3dna2o6mxqbweodtiyalfjsfynmereu1rjisj3kdg6jlsmsinxdfqy5jrjielb3qnyyb=\"jwGAsm4W0QZhGDXwXLtVV7rUhUrG9UHzT8wvWOCb5w99lrFi8LY8U7asj6Fhe1\" style=\"border-collapse:collapse;\"> \r\n                              <td align=\"center\" 7zh6xoazabudw5natnsiogbwl=\"qonQ6UnUa9VIcrVZpd0wgQvI14fC7kDz\" style=\"Margin:0;padding-left:10px;padding-right:10px;padding-bottom:15px;padding-top:20px;\"><span class=\"es-button-border\" yrahtfdfayyrvxu3mpt0he0saehbhgvkxrvgymkyvzjtvd=\"MnFCHw6PiMmaHX30udiI53kyBB2ZQbBTCNY3QWn6ewmgmpVU1mCKJg8ZTtbUMkhRvaMNhbZu4TioZm1gp7WfsM4Zbgt\" style=\"border-style:solid;border-color:#474745;background:#474745;border-width:0px;display:inline-block;border-radius:20px;width:auto;\"><a href=\"{{confirmation_url}}\" class=\"es-button\" target=\"_blank\" uhvprurysdorboh5e2eys1ih7va25y75w53zkrsikbbvtbf1wsz5ofnfsycwxhfpsfwjmgzkqtszdryuvuklm8dbzb3qqyc=\"VxRspiOcuW2DYyXIosfiHNPpmTojfPeAwFWgWVfovj1aqVI\" style=\"mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:helvetica, \'helvetica neue\', arial, verdana, sans-serif;font-size:16px;color:#EFEFEF;border-style:solid;border-color:#474745;border-width:6px 25px 6px 25px;display:inline-block;background:#474745;border-radius:20px;font-weight:normal;font-style:normal;line-height:19px;width:auto;text-align:center;\">Confirm Email</a></span></td> \r\n                             </tr> \r\n                           </tbody></table></td> \r\n                         </tr> \r\n                       </tbody></table></td> \r\n                     </tr> \r\n                   </tbody></table></td> \r\n                 </tr> \r\n               </tbody></table> \r\n               <table class=\"es-content\" align=\"center\" cellspacing=\"0\" cellpadding=\"0\" ycqrewadflx0mycmkpbxpvuna6eclaivs5r5bebthnf1wobjbvdvraamftscvxbmwonzli0n=\"KWmXPdCt97axB5rw21tdSpIETxzSmURazNW7DHeBN4vKbIQR14recDTJYFAO8XjBPgw23CtmmZrnwWxbZDEBxViVhUe9EnMfWm\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;\"> \r\n                <tbody xfzqf6p7a7ghs1tolwtmgqk9jkpq46ve2f9owhnd74n98x4r260lnnfe0cj896cf7c=\"RgHzrLKQzLD9DEmLaVGxVCKWIfda2r\"> \r\n                 <tr 5ljxs4pelz7jxoqq5hm2p6bna5q=\"ncyAYjDshZqBQFLnuPScqV1bdqrMeiNpr4bZTC02eG\" style=\"border-collapse:collapse;\"></tr> \r\n                 <tr k3knkoq3a1cvcnafxi7kiuxcji3ic2mwet3t8kpi0t0zpk=\"ln1wY8hoQf15XOeQ3X7CENFljibegpNZqr9j6JNWuGgCVtR0hh\" style=\"border-collapse:collapse;\"> \r\n                  <td style=\"padding:0;Margin:0;background-color:#F7F7F7;\" align=\"center\" bgcolor=\"#f7f7f7\" jolhwcmaq055h4xjmkvv5ajewr3xwcav8dqlstk1zphhy=\"UohREfZ5rwXmwqjeAZAWNTReVMbzUqv88bnKRMRIR7GCgUlTFpUC3xOzglMO6Wnyvj2iTmAiLQETYeWm\"> \r\n                   <table class=\"es-footer-body\" align=\"center\" width=\"600\" cellspacing=\"0\" cellpadding=\"0\" rp6oi6b2cydaxu0fmwmm7wyyzeg2kvshq7rrjnrxvp4c68sbgrt9wnsnpxmfbwz4a2=\"f9suYgBptBBNP7vpoyXSzeMnc2oBQIbKSnUZApA54LMYbGuz5UvxzRZwm4KoG8bx\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#F7F7F7;\"> \r\n                    <tbody suvqrneyp7qvle5ypmdoha7m3gm7enzt8nwssgawbw2icoin6krkvdkghoja=\"3vqbm3rI6ttpZJRBjYgGbGEffyDS05vk5Inv8kCiUNIcj4foXJNqsJBoU03jAUIpZtng11tHfMTSGIO8LdbxZnMIVtp8ZX\"> \r\n                     <tr rri1s3vf7qziqe9bf2g9qbnidsyjci0qskgvoblzbx7noxjmw5u0ngmf9wllobjtrhncd0ise7d=\"V68Xn7fI1sgfLVG4UWx7rccJeRiTFAJhx1HeNZHaFlx2XNq1yNy9gdrbzF0xTZNMjR0gZt9dg2d7wqwzFUGmBto7T1e\" style=\"border-collapse:collapse;\"> \r\n                      <td align=\"left\" 5kf3zwwnjm0snlkazytahsnnccc7k2t9sgmlfahbzwz5qxdwk9dcyqqqaeu4rmjk2gwq39il4vbtwjec29wecpxpqmdlzj=\"p1pNFhj2ZgNPR0EeY1QJYc6YRA2ITgLi\" style=\"Margin:0;padding-top:20px;padding-bottom:20px;padding-left:20px;padding-right:20px;\"> \r\n                       <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" zetegstphftfnvbpvdw9rppkgzrxh6crt4p7flxeg8ey7c4mnggfk5eas4udivsilsyczq=\"fnudYzLyhxtVIXv5gMcJBsAXBR8Be3gnM3WO7Pcy8BfUfKnnRVh\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n                        <tbody cc7ueqzxvmnxmomnlyrvfdrjaccg6xfroyawovl2mgawnjemtnoln4dyx0hzwjb=\"MupFEYi89yaoVVY66A2vN202k0JmFC8aPmJHSH5FSh62sn\"> \r\n                         <tr adaqhzoprpaj4u72radrru4kjrrdvksnvivqthmaz=\"UBTYAGiKM7T7rjLJ6UUr4LysoyhkeQsw7D6lpqpSP4MnCzDjFkIUVxppTmjy5TrEjZ1Ql0OAzOD11UmXVrSDzxpGILWah\" style=\"border-collapse:collapse;\"> \r\n                          <td align=\"center\" width=\"560\" valign=\"top\" 6lrcdsfapouz36nvnkjmhutwcabjrhkumkzyrcm2gtayo=\"jANXgQXBV0Dm1wb3usFoiUUdD\" style=\"padding:0;Margin:0;\"> \r\n                           <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" xkmewjmvtvj3aml0jyyotvk9eyr8f4px1apztz1s7pkpbbtkyrnkcoeaudsemhyvlmdao2m=\"Sflp2VQwIHEix2ozX6MjyRQttp14v8fHJGlXwhAcmxuKcpRXyiJ7ZUTeN5s\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n                            <tbody yizb2odqbjpesxlhuvx0m9rp9uqdiupiliqq5vmti8etuhe2yfecuipt7m3g9rm2fo0jsngaefzzwmlzyqrmm=\"kQPKcgdbK3xFGnolLJME1njw2wYy2KeB8\"> \r\n                             <tr mqahiwq1ma7oakeug6nlk4tf0nowtyaoijmhxnhkaziwx3cj=\"bUz8VJxQBwDZ4bmsdVWlBNkLApxqCNusrErTkw2MuGWL9bZ6Rp55IFltqEMoSVSPsIQESzLGvtYzB2wEzvtjWp47AbmUygf\" style=\"border-collapse:collapse;\"> \r\n                              \r\n                             </tr> \r\n                             <tr vcyymatulw20bmnijhv0jeuczzgyttzrupxcqa9ixxklewqje6fw=\"EqVXam4i40o7RbYW8rlv3r0ZkTjYUrx\" style=\"border-collapse:collapse;\"> \r\n                              <td align=\"center\" recfo0wi7c0zz13v4kafaemiennlnhjy0vvijjdtamp9y3uxyvrok6fhh1l5rpso81iegpi=\"HKJ5INOUqf3DLdTFITtqymDFHbsB5DkFMGIZziOriLmoDnHUiZACBjm\" style=\"padding:0;Margin:0;padding-top:10px;padding-bottom:10px;\"> \r\n                               <table class=\"es-table-not-adapt es-social\" cellspacing=\"0\" cellpadding=\"0\" ufjc97wz9g8wuw8rdatbkdtkszrbzqfwf1gtw0=\"yiiwAk1YzvBwWH7chE0QATeNqUJSHb3rCLoQFKN9s7Kw\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n                                <tbody zdgfxvllqknpx5abci9kp2chbnbwywm1szecfgmyfqakgylq6y01toetc84iztryr73bl8zwwmuzresm84wfmpskt=\"znm60p8OIm0IGN6XLXB5iJcTPEHBRFUiraSdmLG7xEe43ftpLF0RF12GH6wT9MCK2UaCpkSvo9gbk\"> \r\n                                 <tr lbqh49pdr8umxaapbqxr67yjqaghbu18av96xu1eupvhw71ip4bje=\"QqxklwJiWovtFHIoCUVNg792WkJJAgGPXw8nVRqFGP5OW79zH\" style=\"border-collapse:collapse;\"> \r\n                    </tr> \r\n                               </tbody></table></td> \r\n                             </tr> \r\n                             <tr rcg0bp5zd5wpc46xp3gvsf1vq7ruaqrdzniu2ekmpfmsqhosac=\"O9JB7cxgtfBZdn9vyd9JUQSQ0H\" style=\"border-collapse:collapse;\"> \r\n                              <td align=\"center\" aqy9uzygthjyjloxziqxxmzg=\"2VEmcWo3thBsunAELSplNf621XXicgVUHiigLSk9tlTTG3ArqOqGc0OYzVgg9LCAJ9klaaZ\" style=\"padding:0;Margin:0;padding-top:10px;padding-bottom:10px;\"><p ykc524a2kr1bqtthzobicrfsgl=\"MvQ5j4HLzQ0tIwJHupVpBBj\" style=\"Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial, \'helvetica neue\', helvetica, sans-serif;line-height:21px;color:#333333;\">{{year}}&nbsp;{{appName}} All rights reserved</p></td> \r\n                             </tr> \r\n                           </tbody></table></td> \r\n                         </tr> \r\n                       </tbody></table></td> \r\n                     </tr> \r\n                   </tbody></table></td> \r\n                 </tr> \r\n               </tbody></table></td> \r\n             </tr> \r\n           </tbody></table> \r\n          </div> \r\n          <div style=\"position:absolute;left:-9999px;top:-9999px;margin:0px;\" nt6hz4qrqya2xf2ww12kxxltowvk4exockpgqhvy20qovqkhdxwyghueuc1jbojrin3fo8dnln=\"hQN5OgpikQMhVtemoOZeyEtNkPFlLMTarQasgK2DSbt89h\"></div>  \r\n        </body></html>', '2019-11-04 00:00:00', '2019-11-04 00:00:00'),
(2, 'pembagian_mitra', '60', '2019-11-04 00:00:00', '2019-11-04 00:00:00'),
(3, 'pembagian_customer', '20', '2019-11-04 00:00:00', '2019-11-04 00:00:00'),
(4, 'pembagian_pengepul', '20', '2019-11-04 00:00:00', '2019-11-04 00:00:00');
INSERT INTO `setting` (`id`, `name`, `value`, `dateCreated`, `dateModified`) VALUES
(5, 'forgot_password', '<html style=\"width:100%;font-family:arial, \'helvetica neue\', helvetica, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0;\"><head> \r\n  <meta charset=\"UTF-8\"> \r\n  <meta content=\"width=device-width, initial-scale=1\" name=\"viewport\"> \r\n  <meta name=\"x-apple-disable-message-reformatting\"> \r\n  <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\"> \r\n  <meta content=\"telephone=no\" name=\"format-detection\"> \r\n  <title>forgot password</title> \r\n  <link rel=\"shortcut icon\" type=\"image/png\" href=\"https://stripo.email/assets/img/favicon.png\"> \r\n  <style type=\"text/css\">\r\n@media only screen and (max-width:600px) {p, ul li, ol li, a { font-size:16px!important; line-height:150%!important } h1 { font-size:30px!important; text-align:center; line-height:120%!important } h2 { font-size:26px!important; text-align:center; line-height:120%!important } h3 { font-size:20px!important; text-align:center; line-height:120%!important } h1 a { font-size:30px!important } h2 a { font-size:26px!important } h3 a { font-size:20px!important } .es-menu td a { font-size:16px!important } .es-header-body p, .es-header-body ul li, .es-header-body ol li, .es-header-body a { font-size:16px!important } .es-footer-body p, .es-footer-body ul li, .es-footer-body ol li, .es-footer-body a { font-size:16px!important } .es-infoblock p, .es-infoblock ul li, .es-infoblock ol li, .es-infoblock a { font-size:12px!important } *[class=\"gmail-fix\"] { display:none!important } .es-m-txt-c, .es-m-txt-c h1, .es-m-txt-c h2, .es-m-txt-c h3 { text-align:center!important } .es-m-txt-r, .es-m-txt-r h1, .es-m-txt-r h2, .es-m-txt-r h3 { text-align:right!important } .es-m-txt-l, .es-m-txt-l h1, .es-m-txt-l h2, .es-m-txt-l h3 { text-align:left!important } .es-m-txt-r img, .es-m-txt-c img, .es-m-txt-l img { display:inline!important } .es-button-border { display:inline-block!important } a.es-button { font-size:20px!important; display:inline-block!important; border-width:6px 25px 6px 25px!important } .es-btn-fw { border-width:10px 0px!important; text-align:center!important } .es-adaptive table, .es-btn-fw, .es-btn-fw-brdr, .es-left, .es-right { width:100%!important } .es-content table, .es-header table, .es-footer table, .es-content, .es-footer, .es-header { width:100%!important; max-width:600px!important } .es-adapt-td { display:block!important; width:100%!important } .adapt-img { width:100%!important; height:auto!important } .es-m-p0 { padding:0px!important } .es-m-p0r { padding-right:0px!important } .es-m-p0l { padding-left:0px!important } .es-m-p0t { padding-top:0px!important } .es-m-p0b { padding-bottom:0!important } .es-m-p20b { padding-bottom:20px!important } .es-mobile-hidden, .es-hidden { display:none!important } .es-desk-hidden { display:table-row!important; width:auto!important; overflow:visible!important; float:none!important; max-height:inherit!important; line-height:inherit!important } .es-desk-menu-hidden { display:table-cell!important } table.es-table-not-adapt, .esd-block-html table { width:auto!important } table.es-social { display:inline-block!important } table.es-social td { display:inline-block!important } }\r\n#outlook a {\r\n	padding:0;\r\n}\r\n.ExternalClass {\r\n	width:100%;\r\n}\r\n.ExternalClass,\r\n.ExternalClass p,\r\n.ExternalClass span,\r\n.ExternalClass font,\r\n.ExternalClass td,\r\n.ExternalClass div {\r\n	line-height:100%;\r\n}\r\n.es-button {\r\n	mso-style-priority:100!important;\r\n	text-decoration:none!important;\r\n}\r\na[x-apple-data-detectors] {\r\n	color:inherit!important;\r\n	text-decoration:none!important;\r\n	font-size:inherit!important;\r\n	font-family:inherit!important;\r\n	font-weight:inherit!important;\r\n	line-height:inherit!important;\r\n}\r\n.es-desk-hidden {\r\n	display:none;\r\n	float:left;\r\n	overflow:hidden;\r\n	width:0;\r\n	max-height:0;\r\n	line-height:0;\r\n	mso-hide:all;\r\n}\r\n</style> \r\n <meta property=\"og:title\" content=\"forgot password\"><meta property=\"og:description\" content=\"{{appName}}. Home Order Create Quest Hey {{user_name}}! We received a request to set reset your password. If this is correct, please confirm by clicking the button below. If\"><meta property=\"og:url\" content=\"https://viewstripo.email/template/4e95441e-b051-42c6-857e-b549e740397a\"><meta property=\"og:type\" content=\"article\"></head> \r\n <body pszmafc0dlasofuafabvmshbrwczfnxnybr=\"Tq88TJTIr4cyH448uXFLltj6PlOmtWFukajzSJkfnJq7h\" style=\"width:100%;font-family:arial, \'helvetica neue\', helvetica, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0;\"> \r\n  <div class=\"es-wrapper-color\" hzkyo077dyroitotkvfawceqlrh6utpp2gdityshney7x7lndbi2owhvyx5qj7zsdsfq0fswal2cqmf5s=\"Jtd5aSYjBOWnrs9SNfVd22j6nCyxCY9vlu1wWf2sCjCEhigPTks9u\" style=\"background-color:#FFFFFF;\"> \r\n   <!--[if gte mso 9]>\r\n			<v:background xmlns:v=\"urn:schemas-microsoft-com:vml\" fill=\"t\">\r\n				<v:fill type=\"tile\" color=\"#ffffff\"></v:fill>\r\n			</v:background>\r\n		<![endif]--> \r\n   <table class=\"es-wrapper\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" osyxey9wnjjjbhd46gzd=\"kW15ehXVPQtZknZVYcodxth7FrOBdi1D7awPYMN\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;padding:0;Margin:0;width:100%;height:100%;background-repeat:repeat;background-position:center top;\"> \r\n    <tbody w2d0my0x7bjytb8tauot9edlg0a0hocgx3m8xx=\"nhdKyN8rXzCMLW4GY57YasnwGvg27uUUwegaja11LCXuY6A7kRQkZADBPdJ3zJqJkLBDGx9\"> \r\n     <tr olpxein5n5igbdx8jxeiwiraw6watmkg0rjph5dwvi7muccv5aiyer0=\"cRa7p5cYyrQIU4L5EiI4OdQARF0FYCPlBYmbFY9cL8UiTnYHQ1arY9tAeiatrAobeiEkW1tydsTwNPWIW99okoVpnG80\" style=\"border-collapse:collapse;\"> \r\n      <td valign=\"top\" mbeb8cob4kqmvozdjurcbqsky3s4l1lk68n=\"3RixmIdp4LvVtPevYBZWMmUkETBzHe6MpagXgyAYdZY\" style=\"padding:0;Margin:0;\"> \r\n       <table class=\"es-header\" align=\"center\" cellspacing=\"0\" cellpadding=\"0\" nuqivnt5duzanttxz944nfl5v7cujptz0n49vwxhbwyi74w6l3nr6jyfedlofixdl1fr2yir0oph9vhxjn6iavx=\"RhqJcWmzUIeA8MRQgJUu5v3HkYyCLNL4GiBhQibb6xlQYndh9nDFu86XF4Y4GWXT8sxFp4ZcxKo7vkniH\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;background-color:transparent;background-repeat:repeat;background-position:center top;\"> \r\n        <tbody 5yxqpik8aq9euol2tjo9w=\"h3HWiY8njGhQkD3FBndTatmo3gPkGKYDzA2Nj62wfCG29oLtdFXVT1\"> \r\n         <tr naxrtbr6tdnddpt3gtz9hs85rkdwooqgmwywaieuyxidvuin=\"Yp6H2DtBvklrc6tvQtLfwfyTgCZfkybajpVNj\" style=\"border-collapse:collapse;\"> \r\n          <td class=\"es-adaptive\" align=\"center\" a8om5kgmu9doihm9zqjxhdc73pucmahxnd=\"hED4Hrp6dMokiMSbzBhBlYEYSyK6raWLht9JWaQM5vme1c65ak1b1DEY98r1O5\" style=\"padding:0;Margin:0;\"> \r\n           <table class=\"es-header-body\" align=\"center\" width=\"600\" cellspacing=\"0\" cellpadding=\"0\" wdymnbxpml5ktjslmtgouzefuntilenkmtzsujterflxd3cbtlozj8gqclnb5qygeh4lvzulizszmwo9injs3evlgiw0dfe=\"MpC0srlpMmPUoy20u2u0rQpUTpPTl1JOEIHntyzUuKj4nvIQM5xqhCKBfy1XdBMD3FLOwM\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:transparent;\"> \r\n            <tbody hmhjrlbvvowtt8zipnxavpnb6sdhxvasanh0admm3br2admmb5p0ka4n27tagtilqhfitnmnms5dido6l99eepw=\"voxJxaElOQf1GnkAMP9CU\"> \r\n             <tr cl8lqoxzqnks9zs9ysc5kvgdzqimv=\"WrA11XZ9ZlgnCamEF2sf2thE2DPEBlWz0O3WaHe2\" style=\"border-collapse:collapse;\"> \r\n              <td align=\"left\" 0ftnyacpejhorynsvp86oeoqocc4sqspmehcnwut6fxpgrt7906lixysyn=\"iOmdhgo3R7pTENMdynSb36GOTLsWKBbpIcTxp\" style=\"Margin:0;padding-bottom:10px;padding-left:20px;padding-right:20px;padding-top:25px;\"> \r\n               <!--[if mso]><table width=\"560\" cellpadding=\"0\"\r\n                            cellspacing=\"0\"><tr><td width=\"205\" valign=\"top\"><![endif]--> \r\n               <table class=\"es-left\" align=\"left\" cellspacing=\"0\" cellpadding=\"0\" 9rxnq4aaf3y1v42z9177e8prbjrlv1ztmh6rqqwcolpdemo9hhhnvn3ft3wo0jeg0ex7jdwnqvgwdsi9p9ghi=\"9lwn9oh97mBgS3CyYq0gCSTOIUvALabJwEvzfTYNE1jn7RIGhurAo0CptNB0FQbnzcC0KpIC8iksvH3wouYX0DttYmF3VcAVZ2\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left;\"> \r\n                <tbody gxrn9serbku750qsgy3gofwa9p697eny7ivqyyhztgoulfzorrjl3chgsvhkrnukuem0ocluh=\"R0Ix03zOFkF0ZABKy8Vg9svgMnakY82gMkOpT0ixvmbXOpSECkS1TTuHrn\"> \r\n                 <tr oi6n5i7czgq1aby3hyxm9b1onqgxrtmaypb6qptifpyy7bur4tkx2la1wvxjdkrdrytosjepf7ntb8c=\"6UaFhkeCGwEsqyy99jeYA5vP4KLKcLl5JYH6EvS6Qq8WeKzce\" style=\"border-collapse:collapse;\"> \r\n                  <td class=\"es-m-p0r es-m-p20b\" align=\"center\" width=\"1000\" valign=\"top\" ntrtjdawfqcs01nv1yffmixs18jutav2laackrxcebf9ff79fwuygova3svhvgj1dn=\"cmfo0UtC7OZtzCYYFkRHtzK121WgyBwIWWkbse\" style=\"padding:0;Margin:0;\"> \r\n                   <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" nppg49tu3zo1gdrsjgz8mavqylszzkl16q8omqzxflhcghlmaem7bmrv8snp=\"aYY0LLnpLzjV37YZgc6uhWoXCNB49A6m87WEinFJNYFaGAbO5F3yYqXFgeBQCfWnwwJy7Gx\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n                    <tbody vk1abgivlmwjexbx2qh89maufqsrdvwsih9umouvzgmdvcsfene9xhk6asdekobpbvzx1zzutmynxeahazarvig8tqnh=\"36rm6atamKYqdcLBzFpPiIZF4czrLdhBdTZv3JLoNvGIdeNKYrCOoX890sCc2X2UsFS805iC83uE4TKKfXj\"> \r\n                     <tr y2nqg8cjawuajrtf5hzoi9ypzad0crvt7pvb8isrvenymekqbvyur3vp3w=\"JrJ3to2XhUZbgvWPskxOMwrmwqoDX135Gy2YzkZ6Go6n3A8dCvjY0zLeZNURLW7zwEHQnYz\" style=\"border-collapse:collapse;\"> \r\n                      <td align=\"center\" 1pvekvz0nm3n1q4wbwuzcohhj21iqjie0cgwzfeuvedpdwmqspfx8ge=\"5mWz62PkJwVI21BKxyMGpOHKcJgm2SIV8mHvDXUvDwFNhnXGx2ha48lCqTAjyDz\" style=\"padding:0;Margin:0;padding-left:15px;\"><p style=\"Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:20px;font-family:arial, \'helvetica neue\', helvetica, sans-serif;line-height:30px;color:#333333;\" egjo7tzleprxz8qahiac7zpduras3qti9j6wrx75kauzabvzw8guvur9wwbzawsdiedmbqwcy717k=\"DfWnWLwQRobjGx28SnJq6FpYSdC6mszi5PVFQloRI9A4qyOzhm\"><strong ub3zqna7hqt9mtbsoscjvs=\"3Okev7qAzSJQwW9oQfgUSrn9EPRqyd4kVBxFCJYoUF2zgHks2dYoV8E59HYxLh1NxLJeXOn9z\">{{appName}}.</strong></p></td> \r\n                     </tr> \r\n                   </tbody></table></td> \r\n                 </tr> \r\n               </tbody></table> \r\n               <!--[if mso]></td><td width=\"10\"></td><td width=\"345\" valign=\"top\"><![endif]--> \r\n               <table align=\"right\" cellspacing=\"0\" cellpadding=\"0\" 2xpxjj1h25omuafhd8il032a2wdcz5hvprqmxheo8mxrjgi5gtoqm=\"iwCqTwgQWMk4PmefhFqKyGOj2d1qh445DDtdPtBcVGY7DW3k7FgdOUcv8zlnekJO4HlhbV9iq3u7raeK3PgD6J\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n                <tbody wyevfke4rv6ngsenrws7yugyiqrfwvpnxqlhfubqudtzjcxrj65jebren6eyw5pxfq4g1tsbvtvolnjnuipdhf110ggpj4g1=\"1nVfpFQpDnpuerqlLg4NjTYDO72jokd4LNuHcKAAM5Gfk7m1AATukaeBbpph\"> \r\n                 <tr ep6wh15m9x7kprqdhazisqgzraoyqrezhpxfn53tm9gag8vlin5iynhzdkrdvxzwp=\"Y2ogGD64BFlP9czGcJe61wyQYkFwEykB8eyEXpdPtJsY6KAlCoDYJmGcmMqDJQJ8gksU9\" style=\"border-collapse:collapse;\"> \r\n                  <td align=\"left\" width=\"345\" c4iynoi9g01vrn25mzjadvg9ab4n4shinl5ops8gs9c2f3l=\"5nts5gPkskuxfIuiEAXkBQHsN8b1CPS7U9q6Q5zCw2NOPhbpoR4SGpuVURany7otEV4x3u4Yox4KVbYS3GwXl\" style=\"padding:0;Margin:0;\"> \r\n                   <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" mxsc2j4mmcjg0xkumbwa99d0o9pyrktlstbadnqoldxsy2r6wt2hnit3hmrrn8rxzoqrlbzeodmmquwbovqgv0dzvkknmosy=\"FKXtIjGSPIR6uM5fMvx6wtFi8c0NV\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n                    <tbody 7s7zxfkxuk4ifjmqjn3vbak=\"DhmARJMg1T63Gx861teB97qlMHOzq51qwS4uBidy2YP\"> \r\n                     <tr fdxcqd2irblwlzvkl5lnhsl3kltzhnjemoycomjg2vopbafownmuua9c=\"kVQ7j9KOnJdEIUiKLKGqmO6nyTOI5olCzHg7nQQnk2Y9oqnu0vb2v8erwCdHTCFiS0ch2nuDS7GaBzenXex\" style=\"border-collapse:collapse;\"> \r\n                      <td dta4ajkd0osktrceo0omesxbbm=\"WJlJsrrOQjWVjAR43VRwlYwjZwycEOMg2dlo3wumN2u6l4edKfADlGKcBbVgUJwWY38rt4nWvMbVMWp\" style=\"padding:0;Margin:0;\"> \r\n                       <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"es-menu\" nkqef5okljtpcavtvcibwbgmo4ghvefrrf3errm0qrikgrsms5qjnoadzwcjkdz0k9xcm9wwbpe0y1ze2t=\"ZQUYrAXER5g8i9L5SSBCHwxIvfK\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n                        <tbody k7zzo9hoquilrlsvwmz92jqgi7v6nbny6lxb9ojfbttgr72e22qnpvohprv7avh50mjyjrz32yamz2o8ac4hd3w6inb=\"oLAOzfIdbX6OJJKgmbjL\"> \r\n                          \r\n                       </tbody></table></td> \r\n                     </tr> \r\n                   </tbody></table></td> \r\n                 </tr> \r\n               </tbody></table> \r\n               <!--[if mso]></td></tr></table><![endif]--></td> \r\n             </tr> \r\n           </tbody></table></td> \r\n         </tr> \r\n       </tbody></table> \r\n       <table class=\"es-content\" align=\"center\" cellspacing=\"0\" cellpadding=\"0\" ptz0cqwvrkihzjs47j6bag82ckauky=\"OjnfRcnA3LLPNT9lBphCTQFiVutibknHgAxU\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;\"> \r\n        <tbody nybzsw6tedkqd0kgmdwe5ataoenfil4guwqteb9kh1srlx=\"chHEP6WWQFfyUytFBuGjgNCeURPBhGYAuV26adssxA33UWYkvniMwQq59PUXDWjBi44SLq3iibsEFI\"> \r\n         <tr yqiuurmkkgnksmtv0b61rsuu6dprz0upvbucsppdsrlj7xymetqthvfupnhk14extw=\"LkeryhxvfZwida8N0OsX61ISwGvDn4VbcKoyh8NAPmoza0SNcjYM8DKqpHt8EaQloYbICMhvKZwCjvXrNmAPH3Nc7D\" style=\"border-collapse:collapse;\"> \r\n          <td align=\"center\" 4mbuqadntdwp7avjqcvtclkid5yow3ue6otzptexwh4pntu1qpwdti3jambimpavbmdabw5nzcl=\"CcA1Fm1l95KdYhgRE1mVS1f1vl73h42p6pUtM8aPUiU2136yPxPKND0x\" style=\"padding:0;Margin:0;\"> \r\n           <table class=\"es-content-body\" align=\"center\" width=\"600\" cellspacing=\"0\" cellpadding=\"0\" bgcolor=\"#ffffff\" xxz44k4dulmmjt3xqnrp71vkb1xj2i1lvct5qib0fsosyasbsyzo41nyclxv1mt6dxdnxjxaf12xlwvap4ngxretb6qap9lon=\"vIWSwohDhqE9Uih1XHC3m0cBVtgSxk6SKjQ3VKbt\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;\"> \r\n            <tbody ppdw3xgrnjx0etjcjm4jxvjiseti3tewo7aa0mpkknacofsxenzogm9bxoiexehz9w1xgiuhezd=\"K2Qv2pYESvYen92dmz3ObUd2mxFnl2QFjSpcAcNClwU92WW0QkvNuRRqOxCmERGUQS\"> \r\n             <tr shg4zle00bkf4gmdvbupbymdfnjy0xkxiq45scqdgz1dipkqq5phugu00vwhydwshzan5e36vmz6=\"CXGLpYcfRubZfA8nrxBHDTOpKvOW17kMCQ652T9CxnhjFbwReJtanCsvSPuRv\" style=\"border-collapse:collapse;\"> \r\n              <td align=\"left\" ly3jogts3ib0j8l3uszqpe8ut0hrayrdakgrjpq1hdtrgg9bh4nj1srqcxhckrvsiuxmosdk7hdpnbsiicf=\"9GmWmn7siL41xhnwLcden2MgQIxmTB3h9o64MsBR667C\" style=\"padding:0;Margin:0;padding-top:20px;\"> \r\n               <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" hpicpdfwxzsgqwvuvsfzifyiz0dvbge1ykqh=\"nJOcmef0obmaDKSTSCPJw8c49KOPgwZ1aIxpAMKuJqlDqIo3OGb8bUoOIKW1hMH1hHsEVJ77lR1prZhB82uFmEfwQgQ5vfdY\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n                <tbody w2hl45veukduiy7eaqalhszhachomascjrpabksvsqw6oifssyipae9lsn=\"LnwlPrMF15WZKXkJCIsSybxCYI6XHantPI1NMGcYjeErKGes1P1bB0Yi7HbNAMKooT6YovumE\"> \r\n                 <tr 3vzbu0jldkfwdgq0o8rqhx5qqfapkjtxmffydqa3dxcudh4zrcbb4brxn6cr9uozm9dpybvh7llumfr5zb9q8wcs4ypq=\"hP3YZCTUxNOAdezUUNWV9kdc7JbbPt6NyXnU8cvQOs4PmjjxTAbdc3tiCX3vIWhXeu\" style=\"border-collapse:collapse;\"> \r\n                  <td align=\"center\" width=\"600\" valign=\"top\" vuqfwlcq6amj9ysuukn3e4vszav4p2bgbsobug8zks8azxwdosj7dqipcic8hed3hhoeghyjorclfa6jofjuoocbxfpahtf=\"KYNBf439K9In4qChK80DitrenC6XznkbOyduGUTNuklTzX9J\" style=\"padding:0;Margin:0;\"> \r\n                   <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" 3tqzstbk1jndrx0wjf5sxvlrwsykaj32ffjss8rpajcczo7=\"KmscI3Rr4IPXCd2zsoBIzfkKGTaZkh9bMz3Te41nCcSAxfWIfTdJbIwQSk9WhScuxrVsVCMfMJ2k2jEMmIvLLl15zChHn\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n                    <tbody zqxa3wr2kttqq7e00vgpg3gnecyyvcyv=\"tAPcS2L6fzfAiSZpfOz1sBQ2VudAPVxe6FBxfxk9ITGhi8IsiR5MavfopU9MAZVHzd8RpdPyDFK\"> \r\n                     <tr kalqyno6k5y5ywxk2fojz3bh7m5rs14=\"OXyCYoDEzzPM5EKeLHSRdIx5AGEKixFR9SGaZx\" style=\"border-collapse:collapse;\"> \r\n                      <td align=\"center\" ujux5w40zqq51zxibhqosffpdp3sxb7o6hc7rsnldxnr5y9dpugefrtq3w1zo6qkqxt1=\"imSQx3SZtiJB9dzH68YhL0p3xojU\" style=\"padding:0;Margin:0;padding-left:20px;padding-right:20px;\"><img class=\"adapt-img\" src=\"https://ebhbcz.stripocdn.email/content/guids/CABINET_dcfc987b48fba8fc58ecf440b6fb62d1/images/541568447981412.png\" alt=\"Image\" style=\"display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;\" title=\"Image\" width=\"260\" tfljhpo4hljuzbtldnip9dre4ayszh=\"4tkP6AX0VmERUoNAiTzmU4Ee4A58MOGVP3CH25NY31cvxIzWGfDXn8PPwlSl0k8kLRFOh\"></td> \r\n                     </tr> \r\n                   </tbody></table></td> \r\n                 </tr> \r\n               </tbody></table></td> \r\n             </tr> \r\n           </tbody></table></td> \r\n         </tr> \r\n       </tbody></table> \r\n       <table class=\"es-content\" align=\"center\" cellspacing=\"0\" cellpadding=\"0\" pdyrsdfhldgz564ty8apvchu1vos2hplfbhyfc1jwy7woph435qpceieg0vbsuj0wgec0p2rag8=\"M5t0zU7fPKazD3MaxLpv4Ii5gZdCitmlYYlwMzQl2u81NarUzXEYjAm23g2MckpC0dELQ9nbSbqg5NbipzQm3ZjwwyC\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;\"> \r\n        <tbody iinf0cnrmuy0xeyqjeiaoh2me41c3fnwtl7ptqyeujftyqbipvazgncny2k7oziri7l=\"SCQDVyIOJcC7yiBPFjyOf68AZx\"> \r\n         <tr aco97onzbmzjxgqsnxfzmq6gmof8wvj7ari1t6csklhly79w3dygcdqy4rp5aeft38gfmorycmow=\"GjJkz5XzcpCYVHVtbepcrtO6mDrYerVcE3Fds6g2LddgRsaDt5Jrq0IU\" style=\"border-collapse:collapse;\"> \r\n          <td align=\"center\" rqelfz3ahofoxrxqycbjfmgyslcqa1pemh3mzhe0ljmqatvsnpfixyxfdzs=\"AXKZelUwQSlXmEu9MQ2IR4ama4xJL8VLNmuT\" style=\"padding:0;Margin:0;\"> \r\n           <table class=\"es-content-body\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;border-left:1px solid transparent;border-right:1px solid transparent;border-top:1px solid transparent;border-bottom:1px solid transparent;\" align=\"center\" width=\"600\" cellspacing=\"0\" cellpadding=\"0\" bgcolor=\"#ffffff\" bb7qjsxn6i2laqkwk0darpe9j2=\"kSrQ2f05e01oYjwIqE50HWGWFruxsRauvrKcSd6RbCk3WDUbu5NQHUlyc1vonwr0dPizg5NIRKvzqClAKRCCELXcaDaqQc\"> \r\n            <tbody dubliznfnvmuyibkgfwa3e1urm1zgz8teitlfyotee2lo4qk69wrmbpd6dqzupzwyukeidfvz4ulokbct8pf5gt7v=\"yQMnDPKMKkffeCsOpN9AQd6ouuUnQwEva6So55X20DsEx4Cbz5fsk8RB3aLkJQn\"> \r\n             <tr myz1vebl1rpj9z18nlqc=\"ZgEf18vLjIaYKskJRvGivkcu17jvHt2sxq2LKtyc8HAlenuvKNqimayPI5osH\" style=\"border-collapse:collapse;\"> \r\n              <td align=\"left\" r5me2yeklv43knoku7ueswhk07l=\"L2rV7tnTgHJQYsTGoDkofly2IkMxO2aqGXU9Njhj37qh6Qb9C66UzjpMH\" style=\"Margin:0;padding-top:20px;padding-bottom:40px;padding-left:40px;padding-right:40px;\"> \r\n               <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" 5gioldbkctffzdknuro0o4i7abwcfydzsj6lpxglztaozomexjufqjvojale=\"ykPkXz68I6z3JKgUN5mGBnGBoCDHpsDkbfitapcTwb5UZIapmOmBZHYJ7rTpk7285PAYiOIlzq86S9SZ\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n                <tbody qihr4hvuunwtkdjuicpx2v1wa0ru3m=\"2wvyDa6X618lSHpOfQd7But8S21AslJWjJditqnFlLdOHFrkedRPUfWnHh9fvllD\"> \r\n                 <tr sove1zswevh1rfnnuhrzhc6n7qqkzvxwursv9tjdaxawojdkdc0io6m3w4vdlitaq0ulgo5vrsgfp4zeoyyyl2mipaeh=\"0NoeErcukJM0B9LQNlttxsFePSpxENrnwuSxJqZtwbpJAqkEhzbchQBQM2mSPorN6zE4Oh6pKkmmWBXNyctD0X\" style=\"border-collapse:collapse;\"> \r\n                  <td align=\"left\" width=\"518\" o0f749c1epeunagvsdmpa=\"RbvngpYnqSNcZZ8a7rkibmJF1gsB1eLzLTD14p8A2PijrcaJz62AQyw4ibkGsoxe7Z8Hl9VxBx9\" style=\"padding:0;Margin:0;\"> \r\n                   <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" uqliizsdyoyug9tnvafdc1kgeqa2jmr99h3vzxtgy5fmc67qrs8u5iqsba1sdz1f7wl=\"Aj8a2auZOVi2388miSWgUljYofcufqeMI0keJu2v73jCuQHofwJOWEVS6r5jxXKlZBhnBStBTXv59bLOn5ulcyhSeMCw1zT\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n                    <tbody kfsfzcorzukokxn1dpk1lnxym0tr7gxqspxamahokr01spktfpbebinynf1run5id1ts4=\"S6OLaxCMoWMM9Fi0p5EPcDFL1evciz6XjGzies1f1B8xOwlR9FartPORRouZc\"> \r\n                     <tr yaqjnynmcyxknmlzvbxwgowm12v68ppf2=\"M9iqISZ27WDGlqRD52H1JCNw\" style=\"border-collapse:collapse;\"> \r\n                      <td class=\"es-m-txt-c\" align=\"center\" nzpctssib3cjfagowlongt=\"ZPbtKJuReo9gleJrQ47iPmV5ATg7O9QFDeZ\" style=\"padding:0;Margin:0;\"><h2 vl7awmwpsc6mtwegq7cug1rcaokez3b3j0ty66yorzibnbp34uvxe8lgpe=\"0s6AbRVCUfYNSHIDOgiAP9uDmByZ1G1dxKLCKbBA5F2EAU2xTcxgdFReKy9qoDWlhfFSP9UZikLO9ieL1qYys\" style=\"Margin:0;line-height:29px;mso-line-height-rule:exactly;font-family:arial, \'helvetica neue\', helvetica, sans-serif;font-size:24px;font-style:normal;font-weight:normal;color:#333333;\">Hey {{user_name}}!</h2></td> \r\n                     </tr> \r\n                     <tr tjrkkkmvwuhi5oyhvo3i7qh7pc9rqqgnvraaa4zxxuzsbw5mwmsrflaz4sihnebbw6jddslrz=\"UJNedNtAuUx2w6I2SIDmV6bJFhEtpcoAwH98c\" style=\"border-collapse:collapse;\"> \r\n                      <td class=\"es-m-txt-c\" align=\"center\" tkp9h98lcbutvlugvscoanmuvk24ouqy89keszgqrj7kp4ldqvosmea9adqr2vhvj=\"B2cbJDOFRTZhEZ000Q0ErB0rx50rVBp2psPmF0RxyZxnMuvEG7kmI5MWcICfQoO85ZPyijntaTJjW0T4cmukxpzvLxQl2IoNZ\" style=\"padding:0;Margin:0;padding-top:15px;\"><p tiugyuaxnspgmwglxcylfxiobzdilxgav6q52usk1w9jrttoor78lbktoyqz7pglngnfgh=\"I4YQ2a7zDaikHtk4vFBYsCCwwpX\" style=\"Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial, \'helvetica neue\', helvetica, sans-serif;line-height:21px;color:#333333;\">We received a request to set reset your password. You can use the temporary password below to sign in to your account. If you do not know why you got this email, please tell us straight away so we can fix this for you.</p></td> \r\n                     </tr> \r\n                     <tr kehplxn8bpl47tcl00039zvtwjy7qtmna350bdvc1wuhuuj=\"yaLRci4yUaelUUul3FTdnwmnJ8kGhLPUbc72DbM5ROLnz1sbx99zRVV3tBfklUrkaaMylVAqC\" style=\"border-collapse:collapse;\"> \r\n                      <td align=\"center\" 8ihb8rt5yfegbtpguc1kxbuejstjaos17v0jisxb807waxylk4vggr7nctgs1epike=\"pKPAMsAxtvgZz7EusZsQjqu7EvlJU9I8I60rCvdgMDSg7zCrcuZkCakSqYnMLxBXwzd2OzTQ7T093JCv4L0MTcivYzwXSj\" style=\"Margin:0;padding-left:10px;padding-right:10px;padding-bottom:15px;padding-top:20px;\"><span class=\"es-button-border\" tytjvfhufbi0uny8wdq1ahrzb37xrmcx1on5llkoeptfkadqier3yvam5w9jflbzfximwchxfkxqejvd1=\"AN7MlUmDT8ZsgRuSan5rAjmpd6aU\" style=\"border-style:solid;border-color:#474745;background:#474745;border-width:0px;display:inline-block;border-radius:20px;width:auto;\"><span class=\"es-button\" ooo5hr5gpqlkkhxlsq0yu9g5ppl1xa5idysctyhh4xl33qqhe77vvha3w=\"aoIqfL7JP6xaEJzZwJIQQJw2fYfk3cMuNjUvmviQRtAyaqZLqXwarkWCzm0WlzPou4JxN9\" style=\"mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:helvetica, \'helvetica neue\', arial, verdana, sans-serif;font-size:16px;color:#EFEFEF;border-style:solid;border-color:#474745;border-width:6px 25px 6px 25px;display:inline-block;background:#474745;border-radius:20px;font-weight:normal;font-style:normal;line-height:19px;width:auto;text-align:center;\">{{new_password}}</span></span></td> \r\n                     </tr> \r\n                   </tbody></table></td> \r\n                 </tr> \r\n               </tbody></table></td> \r\n             </tr> \r\n           </tbody></table></td> \r\n         </tr> \r\n       </tbody></table> \r\n       <table class=\"es-content\" align=\"center\" cellspacing=\"0\" cellpadding=\"0\" deuf9hehhsd66hf2zdi5yp=\"IzWGRnBQwy7FEAQaHLsOLUmv5qr7vKiwQXaKuhloHL2qlOv\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;\"> \r\n        <tbody sx1pl2vtd3utze5bils7rl4mcmbztaoqajes=\"8Vl5o8S2FvxqSOK6rCxkiS2ZTmQ62GFjmUbHy9I\"> \r\n         <tr hxxqfhvbsizqrkileb9rlrdm8rbp9qd9lcnzja9qh3r4xagzsny5dqzpz=\"93ZmZNZRqdyPJ6ZUNPyuuNgTUdnZkARtFUaMGGQ7o17YvlApGn6pP6\" style=\"border-collapse:collapse;\"></tr> \r\n         <tr w0xm60lupdujvuanilmvtwsdmd8ri8tpnm1txfy8kgn6u1baqvvriz0=\"BKArz4zaT5aCAJp97Xd000HKsSckwFJA2GmtxRUG1U4uXHwINXl9CUtKQUZvnmkH2g3nOfOkI4u8edku4wf0fj920EMnaM695nB\" style=\"border-collapse:collapse;\"> \r\n          <td style=\"padding:0;Margin:0;background-color:#F7F7F7;\" align=\"center\" bgcolor=\"#f7f7f7\" ytwiorqccnu3q2yjfbujn9uwfaqeittynlkcs7w8w=\"OSQgBleb04bKLbS31hTbSs2KyE6TYm4gcJlNb7LqS3tAhB03LqRodGZnmQHage6vV7QEoMdEiwHAOz35hZEyo83kzKHpPDfiY\"> \r\n           <table class=\"es-footer-body\" align=\"center\" width=\"600\" cellspacing=\"0\" cellpadding=\"0\" yqfrlc76pawdbryzndvcyelkhmldlkxedjrb=\"knSMXyqk8tAl0bVmwThrhz4HTKpIQGCkr5KgLNeVNtYtqZY3Ei4DbAyhx3q38\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#F7F7F7;\"> \r\n            <tbody 7tjivmeekyr73s8lqy9dkvwy60yriw8iof9ou2aafqk8fctwxhkoytzqh2vhdkzuw9fctkwbcbx2lztllpbss=\"vksDsuhAxAaKaeaAJQUkT0\"> \r\n             <tr rghdwqoyx2x5geqadnlkztb0n9tkegwmhfeptcx5gcj1qzpo8i0wphmyy3mhwuvwtkm131kwonnewljiyxlvloej=\"f3Afv4xq6ucXHS1f3oE2U6jzYnUiY4jOZ9sK1QChbXip9HBOszAlVeQiu7\" style=\"border-collapse:collapse;\"> \r\n              <td align=\"left\" z3ew7s1odycqdat0mgmovj5bpc8vfhdpt9unoeammobizhpqhm5lgvuyjpkjgqukuaiahjd=\"4MFVKFsalHm02NPhhE2lI0bN2DzgE\" style=\"Margin:0;padding-top:20px;padding-bottom:20px;padding-left:20px;padding-right:20px;\"> \r\n               <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" 4r3qbbh9hwp4jpuyou7q4toa3owcyudkeew4shpkvs4sqgtukir59q8rwa472ovvxis5og7z7zrc0=\"Vqp0bWm5VMK1oIe68yLSKTBiO6eCzWE5Tbxf\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n                <tbody cjecv0z6dkkkoepvqw0keyjp6qdiqtvk19ktz46n8cg6uqd1luivhkeztw5h9ikoy0k3nwgpwlapmqkg9ki40jjcyrfeffb=\"c4NIxTN7stQNpusOvgCKJsIeazwh0Vrp4twmeX353kJS2WYjXHC61eenhle6ZuYnE82Hw1qqpcow9QURZ0wjp6bUkHzJL0VFdx\"> \r\n                 <tr 9qzcwijr1krujavvpr4oqi4201vrcofhaajcx09th=\"WFoJ7oQnfUQHqKMxYdNZrwNJRpaePBJ4rix0gZbDykjJdSd0z9nNeMc2qffI7Q\" style=\"border-collapse:collapse;\"> \r\n                  <td align=\"center\" width=\"560\" valign=\"top\" ojvwvqduwcfbfgfysh4hmpkvg4fzgvwgmebhajxxss3bd8gqhm9tgnbq1=\"cXEjb5MJaPv8k9ffpPldZXycjRTaPpT6KezW9mGqgAWWzIHpFo8Hj3nHHZ0jpkyCovmi\" style=\"padding:0;Margin:0;\"> \r\n                   <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" wf0gpnqfag2vf9sh5iqzs65jqan=\"nj33PS4JUGDuJWSJAzNlX5l8O5KGMKbR6lYaDNjEp7pIP60mRJf7aYSdS\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n                    <tbody ag1gk4ijwcau2ubsxu3ulwfwc5azwj526ldqv2ier=\"6HaEOeM9HGZzP6qTsZMglzE\"> \r\n                     <tr bzml0wzmnwzzjzboiig2vrrz1hiawejqitrjpt13ad5sifinglr1k1qfli4ho8mymgv1rx3qhc40sltwhsl4hppnls0g4=\"Agsi0TTDTzZc7tXLfRHLl6HeTseAU\" style=\"border-collapse:collapse;\"> \r\n						</tr> \r\n                     <tr gzhlddmug6ty8lwnwx3hpw8dkfk84l2eshzajuhvhcfk6seltst20uragg7pjrvwhe1tgybqdnprz091dqkrmapfe=\"SGJ355WEAga71jjLY8mcKFRreOKnj6YBQsAIozmQj8G7CSe8Ke59ykrJ3uwvc4Xin5vPNsgMLNDNj3tvI\" style=\"border-collapse:collapse;\"> \r\n                      <td align=\"center\" 1kwclz5pfptutedww0yyjlo5abnjvtwsdz93dmnku3vblzhm4kvciany1hiacrqtzs1lwehqjzugmsjvedrqe4morjmoc5xqlh=\"29V0hgqmu5zpuChwBmCInDCk3xxQji3dVFo2ww\" style=\"padding:0;Margin:0;padding-top:10px;padding-bottom:10px;\"> \r\n                       <table class=\"es-table-not-adapt es-social\" cellspacing=\"0\" cellpadding=\"0\" 1t19xv83w5iefcgiyiljbvojucnmnrivubj2rmo8y1k5ivdo3o6rwlxkokfylel3ydulu4hlu2zjulqsb2wcw=\"JXXZ40gUrPNlV6bcuILLCvLagrDZuVDgTw8saO99\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n                        <tbody nxekoddmrwbqgspuhnxpbgkfdds=\"mMaiX7WIlOrYG4JxdNrCqUabePUBJ\"> \r\n                         <tr ce84yarmbb1jvual6ylblp3w5xb0k6hgjk7hfitvwxufushzpocsvdiozxtezc1vihdconx52f6ksiofsu5z89fn3httolyokjy=\"l7haDhtooRVY2mZklceJW8fI4F91TW5p0jO1VQ\" style=\"border-collapse:collapse;\"> \r\n						</tr> \r\n                       </tbody></table></td> \r\n                     </tr> \r\n                     <tr pcy2kv8arvgjjljla6k3g55vpecw2ffyc85gegmzatysiweqdzbqxomikjzhyqey4wygyxtjkbmsxerpwueoqxpl6wfk=\"GmRQ4v3xoGKj6RVaEKsXFvCyEzHuqJwdMVccIDqssyUWRBznSU976735RUhnfzqx3iQBXEozZW0djQhonBbaauh2g8yNiHUY\" style=\"border-collapse:collapse;\"> \r\n                      <td align=\"center\" e30i1vf4klmjvpwhmxkj8ncjs5dq1sjt2f=\"tJUJ2pJjTut66PZuPUiQV1xVfasdgp8dWsro83jpLQrPm1BMygOQUfvVJZ24gquwG0tCy\" style=\"padding:0;Margin:0;padding-top:10px;padding-bottom:10px;\"><p 8dy9u1vkftvkjumdxt97ug3uzq=\"5PjNd0oV36nPpRnziR6mBXswSekN1HHoFnNRI5SprJ9Bb\" style=\"Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial, \'helvetica neue\', helvetica, sans-serif;line-height:21px;color:#333333;\">{{year}}&nbsp;{{appName}} All rights reserved</p></td> \r\n                     </tr> \r\n                   </tbody></table></td> \r\n                 </tr> \r\n               </tbody></table></td> \r\n             </tr> \r\n           </tbody></table></td> \r\n         </tr> \r\n       </tbody></table></td> \r\n     </tr> \r\n   </tbody></table> \r\n  </div> \r\n  <div style=\"position:absolute;left:-9999px;top:-9999px;margin:0px;\" lm63kaqzgqcvgg9cmyaogcrdzqtb1vhfaczsrzpbzfjqj=\"Eeoq0OLCfoGjjNXzNhw9k0MZof8QK24rhjNb6oNQXKAWG1z8ra9FFIR9DiepgT5LELH7P3nt4NbNeZ7jQFq\"></div>  \r\n \r\n</body></html>', '2019-11-04 00:00:00', '2019-11-04 00:00:00'),
(6, 'nilai_point', '1000', '2019-11-04 00:00:00', '2019-11-04 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `trashcategory`
--

CREATE TABLE `trashcategory` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `unitId` int(11) NOT NULL,
  `point_value` float NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateModified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `trashcategory`
--

INSERT INTO `trashcategory` (`id`, `name`, `unitId`, `point_value`, `dateCreated`, `dateModified`) VALUES
(1, 'Segala Jenis Plastik \r\n', 1, 3, '2019-10-26 05:38:25', '0000-00-00 00:00:00'),
(3, 'Segala Jenis Kertas ', 1, 1, '2019-11-05 22:07:22', '2019-11-05 22:11:34'),
(4, 'Segala Jenis Botol ', 1, 1, '2019-11-05 22:07:37', '0000-00-00 00:00:00'),
(5, 'Sampah Organic', 1, 1, '2019-11-05 22:07:48', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `trashrequestdet`
--

CREATE TABLE `trashrequestdet` (
  `id` int(11) NOT NULL,
  `trashRequestMstId` int(11) NOT NULL,
  `trashCategoryId` int(11) NOT NULL,
  `weight` float NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateModified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `trashrequestdet`
--

INSERT INTO `trashrequestdet` (`id`, `trashRequestMstId`, `trashCategoryId`, `weight`, `dateCreated`, `dateModified`) VALUES
(1, 1, 1, 50, '2019-11-04 04:01:47', '2019-11-04 04:01:47'),
(2, 1, 1, 50, '2019-11-04 04:01:47', '2019-11-04 04:01:47'),
(3, 2, 1, 3, '2019-11-05 07:11:29', '2019-11-05 07:11:29'),
(4, 2, 1, 3, '2019-11-05 07:11:29', '2019-11-05 07:11:29');

-- --------------------------------------------------------

--
-- Struktur dari tabel `trashrequestmst`
--

CREATE TABLE `trashrequestmst` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `mitraId` int(11) DEFAULT NULL,
  `weightTotal` float NOT NULL,
  `status` enum('pending','completed','canceled') NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateModified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `trashrequestmst`
--

INSERT INTO `trashrequestmst` (`id`, `userId`, `mitraId`, `weightTotal`, `status`, `dateCreated`, `dateModified`) VALUES
(2, 21, 22, 100, 'completed', '2019-11-05 07:11:29', '2019-11-05 20:58:24');

-- --------------------------------------------------------

--
-- Struktur dari tabel `unit`
--

CREATE TABLE `unit` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateModified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `unit`
--

INSERT INTO `unit` (`id`, `name`, `dateCreated`, `dateModified`) VALUES
(1, 'kg', '2019-11-04 00:00:00', '2019-11-04 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `dateOfBirth` date NOT NULL,
  `birthPlace` varchar(100) NOT NULL,
  `phoneNumber` varchar(13) NOT NULL,
  `address` text NOT NULL,
  `gender` enum('male','female') NOT NULL,
  `imageProfile` text NOT NULL,
  `role` enum('member','admin','mitra') NOT NULL,
  `status` enum('active','nonactive') NOT NULL,
  `point` float NOT NULL,
  `verification` text NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateModified` datetime NOT NULL,
  `password` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `name`, `email`, `dateOfBirth`, `birthPlace`, `phoneNumber`, `address`, `gender`, `imageProfile`, `role`, `status`, `point`, `verification`, `dateCreated`, `dateModified`, `password`) VALUES
(2, 'Administrator', 'admin@i-solusi.com', '2019-10-01', 'lampung', '082124356123', 'jakarta', 'male', '', 'admin', 'active', 0, '', '2019-10-02 00:00:00', '2019-10-16 00:00:00', '482c811da5d5b4bc6d497ffa98491e38'),
(21, 'Fikri Akhdi Maulana', 'fikriakhdi22@gmail.com', '0000-00-00', 'Jakarta', '082113137410', 'Jakarta Utara', 'male', '', 'member', 'active', 3.6, '3c9d905fcd4b1488e495dd63c2472f9e', '2019-11-04 01:17:00', '2019-11-05 20:58:25', 'efafce856fcdf65b5632250ef980cb4a'),
(22, 'Mitra', 'invirsoft@gmail.com', '1995-12-22', 'Bekasi', '082113137410', 'Jakarta Utara', 'male', '', 'mitra', 'active', 0.8, 'fc9e55233d29e9650452f205779f700b', '2019-11-05 07:17:21', '2019-11-05 21:21:29', 'efafce856fcdf65b5632250ef980cb4a');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `faq`
--
ALTER TABLE `faq`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `point`
--
ALTER TABLE `point`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `redeem`
--
ALTER TABLE `redeem`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `redeemcategory`
--
ALTER TABLE `redeemcategory`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `session`
--
ALTER TABLE `session`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `trashcategory`
--
ALTER TABLE `trashcategory`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `trashrequestdet`
--
ALTER TABLE `trashrequestdet`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `trashrequestmst`
--
ALTER TABLE `trashrequestmst`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `unit`
--
ALTER TABLE `unit`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `faq`
--
ALTER TABLE `faq`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `point`
--
ALTER TABLE `point`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `redeem`
--
ALTER TABLE `redeem`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `redeemcategory`
--
ALTER TABLE `redeemcategory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `session`
--
ALTER TABLE `session`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT untuk tabel `setting`
--
ALTER TABLE `setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `trashcategory`
--
ALTER TABLE `trashcategory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `trashrequestdet`
--
ALTER TABLE `trashrequestdet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `trashrequestmst`
--
ALTER TABLE `trashrequestmst`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `unit`
--
ALTER TABLE `unit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
