<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Backend extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
    }
    public function index()
    {
        auth_redirect();
        $notifikasi      = array();
        $data['title']   = 'Dashboard';
        $data['content'] = VIEW_BACK . 'dashboard';
        $data['member']  = get_current_member();
        $data['notifikasi'] = $notifikasi;
        $this->load->view(VIEW_BACK . 'template', $data);
    }

    public function pemasok()
    {
        auth_redirect();
        $data['title']   = 'Pemasok';
        $data['content'] = VIEW_BACK . 'pemasok';
        $data['member']  = get_current_member();
        $this->load->view(VIEW_BACK . 'template', $data);
    }

    public function pelanggan()
    {
        auth_redirect();
        $data['title']   = 'Pelanggan';
        $data['content'] = VIEW_BACK . 'pelanggan';
        $data['member']  = get_current_member();
        $this->load->view(VIEW_BACK . 'template', $data);
    }

    public function pekerja()
    {
        auth_redirect();
        $data['title']   = 'Pekerja';
        $data['content'] = VIEW_BACK . 'pekerja';
        $data['member']  = get_current_member();
        $this->load->view(VIEW_BACK . 'template', $data);
    }

    function redeem()
    {
        auth_redirect();
        $data['title']   = 'Redeem';
        $data['content'] = VIEW_BACK . 'redeem';
        $data['member']  = get_current_member();
        $this->load->view(VIEW_BACK . 'template', $data);
    }

    public function setting()
    {
        auth_redirect();
        $data['title']   = 'Setting';
        $data['content'] = VIEW_BACK . 'setting';
        $data['member']  = get_current_member();
        $this->load->view(VIEW_BACK . 'template', $data);
    }
    
    
    function save_setting()
    {
        auth_redirect();
        //check validation
        $pembagian_mitra = $this->input->post('pembagian_mitra');
        $pembagian_customer = $this->input->post('pembagian_customer');
        $pembagian_pengepul = $this->input->post('pembagian_pengepul');
        if($pembagian_mitra+$pembagian_customer+$pembagian_pengepul==100){
        save_settings('pembagian_mitra',$this->input->post('pembagian_mitra'));
        save_settings('pembagian_customer',$this->input->post('pembagian_customer'));
        save_settings('pembagian_pengepul',$this->input->post('pembagian_pengepul'));
        save_settings('nilai_point',$this->input->post('nilai_point'));
        save_settings('max_pickup',$this->input->post('max_pickup'));
        //set flashdata
        $this->session->set_flashdata('message', '<div class="alert alert-success"><strong>Success!</strong> Changes Saved!</div>');
        redirect(base_url('/setting'));
        } else {
            //set flashdata
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error!</strong> Please put the right value!</div>');
            redirect(base_url('/setting'));
        }
    }


    public function penjualan()
    {
        auth_redirect();
        $data['title']   = 'Penjualan';
        $data['content'] = VIEW_BACK . 'penjualan';
        $data['member']  = get_current_member();
        $this->load->view(VIEW_BACK . 'template', $data);
    }

    public function pembelian()
    {
        auth_redirect();
        $data['title']   = 'Pembelian';
        $data['content'] = VIEW_BACK . 'pembelian';
        $data['member']  = get_current_member();
        $this->load->view(VIEW_BACK . 'template', $data);
    }
    
    public function trashrequest_detail($id)
    {
        auth_redirect();
        $data['title']   = 'Trash Request detail';
        $data['content'] = VIEW_BACK . 'trashrequestdet';
        $data['member']  = get_current_member();
        $data['id']      = $id;
        $this->load->view(VIEW_BACK . 'template', $data);
    }

    public function report_point()
    {
        auth_redirect();
        $data['title']   = 'Report Point';
        $data['content'] = VIEW_BACK . 'report_point';
        $data['member']  = get_current_member();
        $this->load->view(VIEW_BACK . 'template', $data);
    }

   
        function create_pemasok()
    {
       $nama_perusahaan     = $this->input->post('nama_perusahaan');
       $nama_pemasok       = $this->input->post('nama_pemasok');
       $alamat_pemasok     = $this->input->post('alamat_pemasok');
       $email_pemasok      = $this->input->post('email_pemasok');
       $telp_pemasok       = $this->input->post('telp_pemasok');
       $status              = $this->input->post('status');
       $member            = get_current_member();
       $datetime          = date("Y-m-d H:i:s");

       $data      = array(
                       'nama_perusahaan' => $nama_perusahaan,
                       'nama_pemasok' => $nama_pemasok,
                       'alamat_pemasok' => $alamat_pemasok,
                       'email_pemasok' => $email_pemasok,
                       'telp_pemasok' => $telp_pemasok,
                       'status' => $status,
                       'datecreated' => $datetime,
                       'datemodified' => $datetime
                   );
                   $flag      = $this->pemasok_model->create_pemasok($data);
                   if ($flag) {
                       //set flashdata
                       $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Sukses!</div>');
                       redirect(base_url("pemasok"));
                   } else {
                       //set flashdata
                       $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Gagal! Terjadi kesalahan pada database.</div>');
                       redirect(base_url("pemasok"));
                   }
    }

        function pemasok_edit_process()
    {
    $id                  = $this->input->post('id');
    $nama_perusahaan     = $this->input->post('nama_perusahaan');
    $nama_pemasok       = $this->input->post('nama_pemasok');
    $alamat_pemasok     = $this->input->post('alamat_pemasok');
    $email_pemasok      = $this->input->post('email_pemasok');
    $telp_pemasok       = $this->input->post('telp_pemasok');
    $status              = $this->input->post('status');
    $datetime            = date("Y-m-d H:i:s");

    $data      = array(
                    'id_pemasok' => $id,
                    'nama_perusahaan' => $nama_perusahaan,
                    'nama_pemasok' => $nama_pemasok,
                    'alamat_pemasok' => $alamat_pemasok,
                    'email_pemasok' => $email_pemasok,
                    'telp_pemasok' => $telp_pemasok,
                    'status' => $status,
                    'datemodified' => $datetime
                );
                $flag      = $this->pemasok_model->update_pemasok($data);
                if ($flag) {
                    //set flashdata
                    $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Data Sukses!</div>');
                    redirect(base_url("pemasok"));
                } else {
                    //set flashdata
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Data Gagal! Terjadi kesalahan pada database.</div>');
                    redirect(base_url("pemasok"));
                }
    }

       function delete_pemasok($id)
   {
      $flag = $this->pemasok_model->delete_pemasok($id);
       if ($flag) {
           //set flashdata
           $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Hapus Data Berhasil!</div>');
           redirect(base_url('pemasok'));
       } else {
           //set flashdata
           $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Hapus Data Gagal!</div>');
           redirect(base_url('pemasok'));
       }
   }


        function pemasok_edit($id = 0)
    {
        auth_redirect();
        $where      = array(
            'id_pemasok' => $id
        );
        $edit_qry = $this->pemasok_model->read_pemasok($where);
        if ($edit_qry->num_rows() != 0) {
            $data['title']       = 'Edit Supplier';
            $data['content']     = VIEW_BACK . 'pemasok_edit';
            $data['member']      = get_current_member();
            $data['data_edit'] = $edit_qry->row();
            $this->load->view(VIEW_BACK . 'template', $data);
        } else
            $this->load->view('404');
    }

        function create_redeem()
    {
       $total_point       = $this->input->post('total_point');
       $id_pelanggan		  = $this->input->post('id_pelanggan');
       $datetime          = date("Y-m-d H:i:s");
       $data_pelanggan    = get_pelanggan($id_pelanggan);
       if($data_pelanggan!=false){
         if($total_point<=$data_pelanggan->total_point){
           $data      = array(
                           'total_point' => $total_point,
                           'id_pelanggan' => $id_pelanggan,
                           'type' => 'out',
                           'datecreated' => $datetime,
                           'datemodified' => $datetime
                       );
                       $flag      = $this->point_model->create_point($data);
                       if ($flag) {
                           //set flashdata
                           $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Sukses!</div>');
                           redirect(base_url("redeem"));
                       } else {
                           //set flashdata
                           $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Gagal! Terjadi kesalahan pada database.</div>');
                           redirect(base_url("redeem"));
                       }
         } else {
         //set flashdata
         $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Gagal! Poin Member tidak cukup.</div>');
         redirect(base_url("redeem"));
         }
       } else {
         //set flashdata
         $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Gagal! Pelanggan tidak dikenal.</div>');
         redirect(base_url("redeem"));
       }
    }

        function redeem_edit_process()
    {
    $id_point          = $this->input->post('id');
    $total_point       = $this->input->post('total_point');
    $id_pelanggan      = $this->input->post('id_pelanggan');
    $datetime          = date("Y-m-d H:i:s");

    $data      = array(
                            'id_point' => $id_point,
                    'total_point' => $total_point,
                    'id_pelanggan' => $id_pelanggan,
                    'datemodified' => $datetime
                );
                $flag      = $this->point_model->update_point($data);
                if ($flag) {
                    //set flashdata
                    $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Data Sukses!</div>');
                    redirect(base_url("redeem"));
                } else {
                    //set flashdata
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Data Gagal! Terjadi kesalahan pada database.</div>');
                    redirect(base_url("redeem"));
                }
    }



        function create_pekerja()
    {
      $id_user           = $this->input->post('id_user');
      $nama_pekerja       = $this->input->post('nama_pekerja');
      $alamat_pekerja      = $this->input->post('alamat_pekerja');
      $telp_pekerja      = $this->input->post('telp_pekerja');
      $gender_pekerja      = $this->input->post('gender_pekerja');
      $datetime          = date("Y-m-d H:i:s");

       $data      = array(
                        'id_user'  => $id_user,
                         'nama_pekerja'  => $nama_pekerja,
                         'alamat_pekerja' => $alamat_pekerja,
                         'telp_pekerja' => $telp_pekerja,
                         'gender_pekerja' => $gender_pekerja,
                         'datecreated' => $datetime,
                         'datemodified' => $datetime
                   );
                   $flag      = $this->pekerja_model->create_pekerja($data);
                   if ($flag) {
                       //set flashdata
                       $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Sukses!</div>');
                       redirect(base_url("pekerja"));
                   } else {
                       //set flashdata
                       $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Gagal! Terjadi kesalahan pada database.</div>');
                       redirect(base_url("pekerja"));
                   }
    }

        function pekerja_edit_process()
    {
        $id_user           = $this->input->post('id_user');
        $nama_pekerja       = $this->input->post('nama_pekerja');
        $alamat_pekerja      = $this->input->post('alamat_pekerja');
        $telp_pekerja      = $this->input->post('telp_pekerja');
        $gender_pekerja      = $this->input->post('gender_pekerja');
        $id_pekerja        = $this->input->post('id');
        $datetime          = date("Y-m-d H:i:s");

     $data      = array(
                        'id_pekerja'  => $id_pekerja,
                        'id_user'  => $id_user,
                        'nama_pekerja'  => $nama_pekerja,
                        'alamat_pekerja' => $alamat_pekerja,
                        'telp_pekerja' => $telp_pekerja,
                        'gender_pekerja' => $gender_pekerja,
                        'datemodified' => $datetime
                );
                $flag      = $this->pekerja_model->update_pekerja($data);
                if ($flag) {
                    //set flashdata
                    $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Data Sukses!</div>');
                    redirect(base_url("pekerja"));
                } else {
                    //set flashdata
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Data Gagal! Terjadi kesalahan pada database.</div>');
                    redirect(base_url("pekerja"));
                }
    }

       function delete_pekerja($id)
   {
      $flag = $this->pekerja_model->delete_pekerja($id);
       if ($flag) {
           //set flashdata
           $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Hapus Data Berhasil!</div>');
           redirect(base_url('pekerja'));
       } else {
           //set flashdata
           $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Hapus Data Gagal!</div>');
           redirect(base_url('pekerja'));
       }
   }

        function pekerja_edit($id = 0)
    {
        auth_redirect();
        $where      = array(
            'pekerja.id_pekerja' => $id
        );
        $edit_qry = $this->pekerja_model->read_pekerja($where);
        if ($edit_qry->num_rows() != 0) {
            $data['title']       = 'Edit Pekerja';
            $data['content']     = VIEW_BACK . 'pekerja_edit';
            $data['member']      = get_current_member();
            $data['data_edit'] = $edit_qry->row();
            $this->load->view(VIEW_BACK . 'template', $data);
        } else
            $this->load->view('404');
    }

  function create_penjualan()
    {
       $id_pelanggan       = $this->input->post('id_pelanggan');
       $id_produk          = $this->input->post('id_produk');
       $tgl_penjualan      = date('Y-m-d', strtotime($this->input->post('tgl_penjualan')));
       $jumlah             = $this->input->post('jumlah_product');
//        $diskon             = $this->input->post('diskon');
//        $status_pembayaran  = $this->input->post('status_pembayaran');
       $list_product       = array();
       $total_penjualan    = 0;
       $datetime           = date("Y-m-d H:i:s");
       $flag_harga_kurang  = 0;
       $member             = get_current_member();
      
       for($i=1;$i<=$jumlah;$i++){
         $qty              = $this->input->post('qty_'.$i);
         $harga_jual       = $this->input->post('harga_jual_'.$i);
         $total_penjualan +=$harga_jual;
         if(!empty($qty) && !empty($harga_jual))
         $list_product[]   = array('id'=>$this->input->post('id_'.$i), 'price'=>$this->input->post('price_'.$i),'qty'=>$qty, 'stock'=>$this->input->post('stok_'.$i), 'harga_jual'=>$harga_jual, 'id_pembelian'=>$this->input->post('id_pembelian_'.$i));
       }
       $this->db->trans_begin();
       //data penjualan master
      if($id_pelanggan!="" && !empty($id_pelanggan) && !empty($list_product)){
       $data      = array(
                       'id_user'     => $member->id_user,
                       'id_pelanggan'  => $id_pelanggan,
                       'tgl_penjualan' => $tgl_penjualan,
//                        'diskon'        => $diskon,
                       'total_penjualan' => $total_penjualan,
//                        'status_pembayaran' => $status_pembayaran,
                       'datecreated'  => $datetime,
                       'datemodified' => $datetime
                   );
                   $insert_id      = $this->penjualan_model->create_penjualan($data);
                   if (!empty($insert_id)) {
                     foreach($list_product as $produk){
                     //create detail penjualan
                     if($produk['qty']<=$produk['stock']){
                         if($produk['harga_jual']>=$produk['price']){
                     $data      = array(
                                     'id_penjualan'  => $insert_id,
                                     'id_produk' => $produk['id'],
                                     'qty' => $produk['qty'],
                                     'harga_jual' => $produk['harga_jual'],
                                     'total_harga' => $produk['qty']*$produk['harga_jual'],
                                     'datecreated'  => $datetime,
                                     'datemodified' => $datetime
                                 );
                                 $total_penjualan+=$data['total_harga'];
                                 $flag      = $this->penjualan_detail_model->create_penjualan_detail($data);
                                //create transaction
                                $pembelian_produk = get_pembelian_produk($produk['id_pembelian']);
                                if($pembelian_produk!=false){
                                $data_create = array(
                                  'id_pembelian_produk'=>$pembelian_produk->id_pembelian_produk,
                                  'qty'=>$produk['qty'],
                                  'type'=>'Penjualan',
                                  'date'=>date('Y-m-d'),
                                  'datecreated'  => $datetime,
                                  'datemodified' => $datetime
                                );
                                $this->transaksi_model->create_transaksi($data_create);
                                }
                         } else $flag_harga_kurang = 1;
                     }
                               }
                       if($flag_harga_kurang==1){
                           //rollback 
                       $this->db->trans_rollback();
                       //set flashdata
                       $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Gagal! Harga Jual tidak boleh kurang dari harga modal.</div>');
                       redirect(base_url("penjualan"));
                       }
                       if(!empty($flag)){
                         //update total_penjualan
                         $data_update = array(
                           'id_penjualan'=>$insert_id,
                           'total_penjualan'=>$total_penjualan,
                           'total_keseluruhan'=>$total_penjualan
                         );
                         $this->penjualan_model->update_penjualan($data_update);
                         //create point
//                          $data_poin = array(
//                            'id_pelanggan'=>$id_pelanggan,
//                            'total_point'=>round($total_penjualan/100000,0,PHP_ROUND_HALF_DOWN),
//                            'type'=>'in',
//                            'datecreated'  => $datetime,
//                            'datemodified' => $datetime
//                          );
//                          $this->point_model->create_point($data_poin);
                         //create nota
                         $data_nota = array(
                           'id_penjualan'=>$insert_id,
                           'datecreated'=>$datetime,
                           'datemodified'=>$datetime
                         );
                         $this->nota_model->create_nota($data_nota);
                         //create surat jalan
                         $data_surat_jalan = array(
                           'id_penjualan'=>$insert_id,
                           'datecreated'=>$datetime,
                           'datemodified'=>$datetime
                         );
                         $this->surat_jalan_model->create_surat_jalan($data_surat_jalan);
//                         update stock
                          foreach($list_product as $produk){
                            $where = array('id_produk'=>$produk['id']);
                            $id_pembelian = $this->penerimaan_produk_model->read_produk($where)->row()->id_pembelian;
                            $where = array('id_pembelian'=>$id_pembelian);
                            $id_pembelian_produk = $this->pembelian_model->read_pembelian($where)->row()->id_pembelian_produk;
                            $where = array('id_pembelian_produk'=>$id_pembelian_produk);
                            $data_pembelian_produk = $this->pembelian_produk_model->read_pembelian_produk($where)->row();
                            $data_update = array(
                              'id_pembelian_produk'=>$id_pembelian_produk,
                              'stok_produk'=>$data_pembelian_produk->stok_produk-$produk['qty']
                            );
                            $this->pembelian_produk_model->update_pembelian_produk($data_update);
                          }
                       //commit 
                       $this->db->trans_commit();
                       //set flashdata
                       $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Sukses!</div>');
                       redirect(base_url("penjualan"));
                     } else {
                       //rollback 
                       $this->db->trans_rollback();
                       //set flashdata
                       $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Gagal! Stok Produk Kurang.</div>');
                       redirect(base_url("penjualan"));
                     }
                     } else {
                       //rollback 
                       $this->db->trans_rollback();
                       //set flashdata
                       $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Gagal! Terjadi kesalahan pada database.</div>');
                       redirect(base_url("penjualan"));
                     }
      } else {
               //rollback 
               $this->db->trans_rollback();
               //set flashdata
               $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Gagal! Data harus dilengkapi.</div>');
               redirect(base_url("penjualan"));
      }
    }

       function nonactive_penjualan($id)
   {
         $data_update = array(
          'id_penjualan'=>$id,
           'status_active'=>'nonactive'
         );
      $flag = $this->penjualan_model->update_penjualan($data_update);
       if ($flag) {
           $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Nonaktif Data Berhasil!</div>');
           redirect(base_url('penjualan'));
       } else {
           //set flashdata
           $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Nonaktif Data Gagal!</div>');
           redirect(base_url('penjualan'));
       }
   }
  
  function active_penjualan($id)
   {
         $data_update = array(
          'id_penjualan'=>$id,
           'status_active'=>'active'
         );
      $flag = $this->penjualan_model->update_penjualan($data_update);
       if ($flag) {
           $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Aktif Data Berhasil!</div>');
           redirect(base_url('penjualan'));
       } else {
           //set flashdata
           $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Aktif Data Gagal!</div>');
           redirect(base_url('penjualan'));
       }
   }
  
  function nonactive_penerimaan_produk($id)
   {
         $data_update = array(
          'id_produk'=>$id,
           'status_active'=>'nonactive'
         );
      $flag = $this->penerimaan_produk_model->update_produk($data_update);
       if ($flag) {
           $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Nonaktif Data Berhasil!</div>');
           redirect(base_url('penerimaan_produk'));
       } else {
           //set flashdata
           $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Nonaktif Data Gagal!</div>');
           redirect(base_url('penerimaan_produk'));
       }
   }
  
  function active_penerimaan_produk($id)
   {
         $data_update = array(
          'id_produk'=>$id,
           'status_active'=>'active'
         );
      $flag = $this->penerimaan_produk_model->update_produk($data_update);
       if ($flag) {
           $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Aktif Data Berhasil!</div>');
           redirect(base_url('penerimaan_produk'));
       } else {
           //set flashdata
           $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Aktif Data Gagal!</div>');
           redirect(base_url('penerimaan_produk'));
       }
   }

  function nonactive_pembelian($id)
   {
         $data_update = array(
          'id_pembelian'=>$id,
           'status_active'=>'nonactive'
         );
      $flag = $this->pembelian_model->update_pembelian($data_update);
       if ($flag) {
           $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Nonaktif Data Berhasil!</div>');
           redirect(base_url('pembelian'));
       } else {
           //set flashdata
           $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Nonaktif Data Gagal!</div>');
           redirect(base_url('pembelian'));
       }
   }
  
  function active_pembelian($id)
   {
         $data_update = array(
          'id_pembelian'=>$id,
           'status_active'=>'active'
         );
      $flag = $this->pembelian_model->update_pembelian($data_update);
       if ($flag) {
           $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Aktif Data Berhasil!</div>');
           redirect(base_url('pembelian'));
       } else {
           //set flashdata
           $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Aktif Data Gagal!</div>');
           redirect(base_url('pembelian'));
       }
   }
  
  
  
        function nonactive_pembelian_produk($id)
    {
            $data_update = array(
            'id_pembelian_produk'=>$id,
            'status_active'=>'nonactive'
            );
        $flag = $this->pembelian_produk_model->update_pembelian_produk($data_update);
        if ($flag) {
            $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Nonaktif Data Berhasil!</div>');
            redirect(base_url('pembelian_produk'));
        } else {
            //set flashdata
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Nonaktif Data Gagal!</div>');
            redirect(base_url('pembelian_produk'));
        }
    }
  
        function active_pembelian_produk($id)
    {
            $data_update = array(
            'id_pembelian_produk'=>$id,
            'status_active'=>'active'
            );
        $flag = $this->pembelian_produk_model->update_pembelian_produk($data_update);
        if ($flag) {
            $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Aktif Data Berhasil!</div>');
            redirect(base_url('pembelian_produk'));
        } else {
            //set flashdata
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Aktif Data Gagal!</div>');
            redirect(base_url('pembelian_produk'));
        }
    }


        function penjualan_edit($id = 0)
    {
        auth_redirect();
        $where      = array(
            'id_penjualan' => $id
        );
        $edit_qry = $this->penjualan_model->read_penjualan($where);
        if ($edit_qry->num_rows() != 0) {
            $data['title']       = 'Edit penjualan';
            $data['content']     = VIEW_BACK . 'penjualan_edit';
            $data['member']      = get_current_member();
            $data['data_edit'] = $edit_qry->row();
            $this->load->view(VIEW_BACK . 'template', $data);
        } else $this->load->view('404');
    }

        function penjualan_detail($id_penjualan='')
    {
        if($id_penjualan!=''){
        auth_redirect();
        $where      = array(
            'id_penjualan' => $id_penjualan
        );
        $qry = $this->penjualan_detail_model->read_penjualan_detail($where);
        if ($qry->num_rows() != 0){
            $data['title']       = 'Detail penjualan';
            $data['content']     = VIEW_BACK . 'penjualan_detail';
            $data['member']      = get_current_member();
            $data['penjualan_detail'] = $qry;
            $this->load->view(VIEW_BACK . 'template', $data);
        } else $this->load->view('404');
        } else $this->load->view('404');
    }

        function create_pelanggan()
    {
        $nama_pelanggan               = $this->input->post('nama_pelanggan');
        $alamat_pelanggan             = $this->input->post('alamat_pelanggan');
        $telp_pelanggan               = $this->input->post('telp_pelanggan');
        $datetime           = date("Y-m-d H:i:s");

        $data      = array(
                        'nama_pelanggan'  => $nama_pelanggan,
                        'alamat_pelanggan' => $alamat_pelanggan,
                        'telp_pelanggan' => $telp_pelanggan,
                        'datecreated'  => $datetime,
                        'datemodified' => $datetime
                    );
                    $flag      = $this->pelanggan_model->create_pelanggan($data);
                    if ($flag) {
                        //set flashdata
                        $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Sukses!</div>');
                        redirect(base_url("pelanggan"));
                    } else {
                        //set flashdata
                        $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Gagal! Terjadi kesalahan pada database.</div>');
                        redirect(base_url("pelanggan"));
                    }
    }

        function pelanggan_edit_process()
    {
        $id_pelanggan                 = $this->input->post('id');
        $nama_pelanggan               = $this->input->post('nama_pelanggan');
        $alamat_pelanggan             = $this->input->post('alamat_pelanggan');
        $telp_pelanggan               = $this->input->post('telp_pelanggan');
        $datetime           = date("Y-m-d H:i:s");

                $data      = array(
                    'id_pelanggan'=>$id_pelanggan,
                    'nama_pelanggan'  => $nama_pelanggan,
                    'alamat_pelanggan' => $alamat_pelanggan,
                    'telp_pelanggan' => $telp_pelanggan,
                    'datemodified' => $datetime
                );
                    $flag      = $this->pelanggan_model->update_pelanggan($data);
                    if ($flag) {
                        //set flashdata
                        $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Data Sukses!</div>');
                        redirect(base_url("pelanggan"));
                    } else {
                        //set flashdata
                        $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Data Gagal! Terjadi kesalahan pada database.</div>');
                        redirect(base_url("pelanggan"));
                    }
    }

        function delete_pelanggan($id)
    {
        $flag = $this->pelanggan_model->delete_pelanggan($id);
        if ($flag) {
            //set flashdata
            $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Hapus Data Berhasil!</div>');
            redirect(base_url('pelanggan'));
        } else {
            //set flashdata
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Hapus Data Gagal!</div>');
            redirect(base_url('pelanggan'));
        }
    }


        function pelanggan_edit($id = 0)
    {
       auth_redirect();
       $where      = array(
           'id_pelanggan' => $id
       );
       $edit_qry = $this->pelanggan_model->read_pelanggan($where);
       if ($edit_qry->num_rows() != 0) {
           $data['title']       = 'Edit pelanggan';
           $data['content']     = VIEW_BACK . 'pelanggan_edit';
           $data['member']      = get_current_member();
           $data['data_edit'] = $edit_qry->row();
           $this->load->view(VIEW_BACK . 'template', $data);
       } else
           $this->load->view('404');
    }

        function create_pembelian()
    {
        $id_pembelian_produk         = $this->input->post('id_pembelian_produk');
        $kuantitas                   = $this->input->post('kuantitas');
        $tanggal_pembelian           = date('Y-m-d', strtotime($this->input->post('tanggal_pembelian')));
        $datetime                    = date("Y-m-d H:i:s");
        if($kuantitas>0){
        //penghitungan total
        $where                       = array('id_pembelian_produk'=>$id_pembelian_produk);
        $data_pembelian_produk       = $this->pembelian_produk_model->read_pembelian_produk($where)->row();
    
        $data      = array(
                        'id_pembelian_produk' => $id_pembelian_produk,
                        'kuantitas' => $kuantitas,
                        'total_pembelian' => $data_pembelian_produk->harga_produk*$kuantitas,
                        'tanggal_pembelian' => $tanggal_pembelian,
                        'status'       => '0',
                        'datecreated'  => $datetime,
                        'datemodified' => $datetime
                    );
                    $flag      = $this->pembelian_model->create_pembelian($data);
                    if ($flag) {
                        //update stok produk
                        $data_update = array(
                            'id_pembelian_produk'=>$id_pembelian_produk,
                            'stok_produk'=>$data_pembelian_produk->stok_produk+$kuantitas
                        );
                        $this->pembelian_produk_model->update_pembelian_produk($data_update);
                        //create transaksi
                        $data_create = array(
                            'id_pembelian_produk'=>$id_pembelian_produk,
                            'qty'=>$kuantitas,
                            'type'=>'Pembelian',
                            'date'=>$tanggal_pembelian,
                            'datecreated'=>date('Y-m-d H:i:s'),
                            'datemodified'=>date('Y-m-d H:i:s'),
                        );
                        $this->transaksi_model->create_transaksi($data_create);
                        //set flashdata
                        $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Sukses!</div>');
                        redirect(base_url("pembelian"));
                    } else {
                        //set flashdata
                        $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Gagal! Terjadi kesalahan pada database.</div>');
                        redirect(base_url("pembelian"));
                    }
        } else {
            //set flashdata
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Gagal! Kuantitas tidak boleh kurang dari 1.</div>');
            redirect(base_url("pembelian"));
        }
    }

        function pembelian_edit_process()
    {
      $id                          = $this->input->post('id');
      $nama_produk                 = $this->input->post('nama_produk');
      $jenis_produk                = $this->input->post('jenis_produk');
      $harga_produk                = $this->input->post('harga_produk');
      $stok_produk                 = $this->input->post('stok_produk');
      $tanggal_pembelian           = $this->input->post('tanggal_pembelian');
      $datetime                    = date("Y-m-d H:i:s");

      $data      = array(
                      'id_pembelian'  => $id,
                      'nama_produk' => $nama_produk,
                      'jenis_produk' => $jenis_produk,
                      'harga_produk'  => $harga_produk,
                      'stok_produk'  => $stok_produk,
                      'tanggal_pembelian' => $tanggal_pembelian,
                      'datemodified' => $datetime
                  );
                   $flag      = $this->pembelian_model->update_pembelian($data);
                   if ($flag) {
                       //set flashdata
                       $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Data Sukses!</div>');
                       redirect(base_url("pembelian"));
                   } else {
                       //set flashdata
                       $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Data Gagal! Terjadi kesalahan pada database.</div>');
                       redirect(base_url("pembelian"));
                   }
    }

        function delete_pembelian($id)
    {
        $flag = $this->pembelian_model->delete_pembelian($id);
        if ($flag) {
            //set flashdata
            $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Hapus Data Berhasil!</div>');
            redirect(base_url('pembelian'));
        } else {
            //set flashdata
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Hapus Data Gagal!</div>');
            redirect(base_url('pembelian'));
        }
    }


      function pembelian_edit($id = 0)
    {
        auth_redirect();
        $where      = array(
            'id_pembelian' => $id
        );
        $edit_qry = $this->pembelian_model->read_pembelian($where);
        if ($edit_qry->num_rows() != 0) {
            $data['title']       = 'Edit pembelian';
            $data['content']     = VIEW_BACK . 'pembelian_edit';
            $data['member']      = get_current_member();
            $data['data_edit'] = $edit_qry->row();
            $this->load->view(VIEW_BACK . 'template', $data);
        } else
            $this->load->view('404');
    }

        function create_pembelian_produk()
    {
        $id_pemasok                  = $this->input->post('pemasok');
        $nama_produk                 = $this->input->post('nama_produk');
        $jenis_produk                = $this->input->post('jenis_produk');
        $harga_produk                = $this->input->post('harga_produk');
        $harga_eceran                = $this->input->post('harga_eceran');
        $harga_borongan              = $this->input->post('harga_borongan');
        $stok_produk                 = 0;
        $satuan_produk               = $this->input->post('satuan_produk');
        $min_stok                    = $this->input->post('min_stok');
        $datetime                    = date("Y-m-d H:i:s");
        if($min_stok>0){
        if($harga_produk>0){
            if($harga_borongan>=$harga_produk){
            if($harga_eceran>=$harga_borongan){
                    $data      = array(
                        'id_pemasok' => $id_pemasok,
                        'nama_produk' => $nama_produk,
                        'jenis_produk' => $jenis_produk,
                        'satuan_produk' => $satuan_produk,
                        'harga_produk'  => $harga_produk,
                        'harga_eceran'  => $harga_eceran,
                        'harga_borongan'  => $harga_borongan,
                        'stok_produk'  => $stok_produk,
                        'min_stok' => $min_stok,
                        'datecreated'  => $datetime,
                        'datemodified' => $datetime
                    );
                    $flag      = $this->pembelian_produk_model->create_pembelian_produk($data);
                    if ($flag) {
                        //set flashdata
                        $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Sukses!</div>');
                        redirect(base_url("pembelian_produk"));
                    } else {
                        //set flashdata
                        $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Gagal! Terjadi kesalahan pada database.</div>');
                        redirect(base_url("pembelian_produk"));
                    }
            } else {
                //set flashdata
                        $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Gagal! Harga eceran tidak boleh kurang dari harga borongan.</div>');
                        redirect(base_url("pembelian_produk")); 
        }
            }else { 
                //set flashdata 
                        $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Gagal! Harga borongan tidak boleh kurang dari harga produk.</div>');
                        redirect(base_url("pembelian_produk"));
        }
        } else {
                //set flashdata
                        $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Gagal! Harga produk tidak boleh kurang dari 1.</div>');
                        redirect(base_url("pembelian_produk"));
        }
        } else {
            //set flashdata
                        $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Gagal! Min Stok tidak boleh kurang dari 1.</div>');
                        redirect(base_url("pembelian_produk"));
        }
    }

        function pembelian_produk_edit_process()
    {
      $id                          = $this->input->post('id');
      $id_pemasok                  = $this->input->post('pemasok');
      $nama_produk                 = $this->input->post('nama_produk');
      $jenis_produk                = $this->input->post('jenis_produk');
      $harga_produk                = $this->input->post('harga_produk');
      $harga_eceran                = $this->input->post('harga_eceran');
      $harga_borongan              = $this->input->post('harga_borongan');
      $satuan_produk               = $this->input->post('satuan_produk');
      $min_stok                    = $this->input->post('min_stok');
      $datetime                    = date("Y-m-d H:i:s");
      if($harga_eceran>$harga_produk){
          if($harga_borongan>$harga_produk){
      $data      = array(
                      'id_pembelian_produk'  => $id,
                      'id_pemasok' => $id_pemasok,
                      'nama_produk' => $nama_produk,
                      'jenis_produk' => $jenis_produk,
                      'harga_produk'  => $harga_produk,
                      'harga_eceran'  => $harga_eceran,
                      'harga_borongan'  => $harga_borongan,
                      'satuan_produk' => $satuan_produk,
                      'min_stok' => $min_stok, 
                      'datemodified' => $datetime
                  );
                   $flag      = $this->pembelian_produk_model->update_pembelian_produk($data); 
                   if ($flag) {
                       //set flashdata
                       $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Data Sukses!</div>');
                       redirect(base_url("pembelian_produk"));
                   } else {
                       //set flashdata
                       $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Data Gagal! Terjadi kesalahan pada database.</div>');
                       redirect(base_url("pembelian_produk"));
                   }
          } else {
                 //set flashdata
                           $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Gagal! Harga borongan tidak boleh kurang dari harga produk.</div>');
                           redirect(base_url("pembelian_produk"));
      } 
      } else {
                 //set flashdata
                           $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Gagal! Harga eceran tidak boleh kurang dari harga produk.</div>');
                           redirect(base_url("pembelian_produk"));
        }
    }

        function delete_pembelian_produk($id)
    {
        $flag = $this->pembelian_produk_model->delete_pembelian_produk($id);
        if ($flag) {
            //set flashdata
            $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Hapus Data Berhasil!</div>');
            redirect(base_url('pembelian_produk'));
        } else {
            //set flashdata
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Hapus Data Gagal!</div>');
            redirect(base_url('pembelian_produk'));
        }
    }


        function pembelian_produk_edit($id = 0)
    {
        auth_redirect(); 
        $where      = array(
            'id_pembelian_produk' => $id
        );
        $edit_qry = $this->pembelian_produk_model->read_pembelian_produk($where);
        if ($edit_qry->num_rows() != 0) {
            $data['title']       = 'Edit Pembelian Produk';
            $data['content']     = VIEW_BACK . 'pembelian_produk_edit';
            $data['member']      = get_current_member();
            $data['data_edit'] = $edit_qry->row();
            $this->load->view(VIEW_BACK . 'template', $data);
        } else
            $this->load->view('404');
    }
    
      function create_produk()
        {
           $id_pembelian                 = $this->input->post('id_pembelian');
           $tanggal_penerimaan           = $this->input->post('tanggal_penerimaan');
           $datetime                     = date("Y-m-d H:i:s");

           $data      = array(
                           'id_pembelian'  => $id_pembelian,
                           'tanggal_penerimaan' => $tanggal_penerimaan,
                           'datecreated'  => $datetime,
                           'datemodified' => $datetime
                       );
                       $flag      = $this->penerimaan_produk_model->create_produk($data);
                       if ($flag) {
                         //ubah status pembelian
                         $data_update = array(
                           'id_pembelian'=>$id_pembelian,
                           'status'=>'1'
                         );
                         $this->pembelian_model->update_pembelian($data_update);
                         $pembelian_produk = get_pembelian_produk($id_pembelian);
                         if($pembelian_produk!=false){
                         //create transaksi
                         $data_create = array(
                           'id_pembelian_produk'=>$pembelian_produk->id_pembelian_produk,
                           'qty'=>$pembelian_produk->kuantitas,
                           'type'=>'Penerimaan',
                           'date'=>date('Y-m-d'),
                           'datecreated'=>date('Y-m-d H:i:s'),
                           'datemodified'=>date('Y-m-d H:i:s'),
                         );
                          $this->transaksi_model->create_transaksi($data_create);
                         }
                           //set flashdata
                           $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Sukses!</div>');
                           redirect(base_url("penerimaan_produk"));
                       } else {
                           //set flashdata
                           $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Gagal! Terjadi kesalahan pada database.</div>');
                           redirect(base_url("penerimaan_produk"));
                       }
        }

        function penerimaan_produk_edit_process()
    {
      $id                           = $this->input->post('id');
      $id_pembelian_before          = $this->input->post('id_pembelian_before');
      $id_pembelian                 = $this->input->post('id_pembelian');
      $datetime                     = date("Y-m-d H:i:s");

      $data      = array(
                      'id_produk'  => $id,
                      'id_pembelian'  => $id_pembelian,
                      'datemodified' => $datetime
                  );
                   $flag      = $this->penerimaan_produk_model->update_produk($data);
                   if ($flag) {
                     //ubah status pembelian
                     $data_update = array(
                       'id_pembelian'=>$id_pembelian,
                       'status'=>'1'
                     );
                     $this->pembelian_model->update_pembelian($data_update);
                     //ubah status pembelian before
                     $data_update = array(
                       'id_pembelian'=>$id_pembelian_before,
                       'status'=>'0'
                     );
                     $this->pembelian_model->update_pembelian($data_update);
                       //set flashdata
                       $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Data Sukses!</div>');
                       redirect(base_url("penerimaan_produk"));
                   } else {
                       //set flashdata
                       $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Data Gagal! Terjadi kesalahan pada database.</div>');
                       redirect(base_url("penerimaan_produk"));
                   }
    }


// USER 


        public function user_manager()
    {
        auth_redirect();
        $data['title']   = 'User Manager';
        $data['content'] = VIEW_BACK . 'user_manager';
        $data['member']  = get_current_member();
        $this->load->view(VIEW_BACK . 'template', $data);
    }

        function create_user()
    {
        $name                         = $this->input->post('name');
        $email                        = $this->input->post('email');
        $address                       = $this->input->post('alamat');
        $phoneNumber                  = $this->input->post('phoneNumber');
        $role                         = $this->input->post('role');
        $status                       = $this->input->post('status');
        $provinceId                   = $this->input->post("provinceId");
        $regencyId                    = $this->input->post("regencyId");
        $districtId                   = $this->input->post("districtId");
        $password                     = $this->input->post('password');
        $datetime                     = date("Y-m-d H:i:s");

        $data      = array(
            'name'          => $name,
            'email'         => $email,
            'address'       => $address,
            'phoneNumber'   => $phoneNumber,
            'role'          => $role,
            'status'        => $status,
            'provinceId'    => $provinceId,
            'regencyId'     => $regencyId,
            'districtId'    => $districtId,
            'password'      => $password,
            'verification' => hash('md5', $email),
            'dateCreated'   => $datetime
        );
    

        $flag      = $this->user_model->create_user($data);
        if ($flag) {
            //set flashdata
            $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Sukses!</div>');
            redirect(base_url("user_manager"));
        } else {
            //set flashdata
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Gagal! Terjadi kesalahan pada database.</div>');
            redirect(base_url("user_manager"));
        }
    }

        function user_edit($id = 0)
    {
        auth_redirect();
        $where      = array(
            'user.id' => $id
        );
        $edit_qry = $this->user_model->read_user($where);
        if ($edit_qry->num_rows() != 0) {
            $data['title']       = 'Edit User';
            $data['content']     = VIEW_BACK . 'user_manager_edit';
            $data['member']      = get_current_member();
            $data['data_edit'] = $edit_qry->row();
            $this->load->view(VIEW_BACK . 'template', $data);
        } else
            $this->load->view('404');
    }

        function user_edit_process()
    {
      $id                           = $this->input->post('id');
      $name                         = $this->input->post('name');
      $email                        = $this->input->post('email');
      $address                      = $this->input->post('alamat');
      $provinceId                   = $this->input->post('provinceId');
      $regencyId                    = $this->input->post('regencyId');
      $phoneNumber                  = $this->input->post('phoneNumber');
      $role                         = $this->input->post('role');
      $status                       = $this->input->post('status');
      $datetime                     = date("Y-m-d H:i:s");

        $data      = array(
            'id'            => $id,
            'name'          => $name,
            'email'         => $email,
            'address'       => $address,
            'provinceId'    => $provinceId,
            'regencyId'     => $regencyId,
            'phoneNumber'   => $phoneNumber,
            'role'          => $role,
            'status'        => $status,
            'datemodified'  => $datetime
        );
        $data['ktp'] = $this->input->post("ktp")?$this->input->post("ktp"):"";
        $data['sim'] = $this->input->post("sim")?$this->input->post("sim"):"";
        $data['masa_berlaku_sim'] = $this->input->post("masa_berlaku_sim")?$this->input->post("masa_berlaku_sim"):"";
        $data['jenis_kendaraan'] = $this->input->post("jenis_kendaraan")?$this->input->post("jenis_kendaraan"):"";
        $data['no_plat_kendaraan'] = $this->input->post("no_plat_kendaraan")?$this->input->post("no_plat_kendaraan"):"";
        $flag      = $this->user_model->update_user($data); 
        if ($flag) {
            //set flashdata
            $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Data Sukses!</div>');
            redirect(base_url("user_manager"));
        } else {
            //set flashdata
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Data Gagal! Terjadi kesalahan pada database.</div>');
            redirect(base_url("user_manager"));
        }
    }


    function delete_user($id)
    {
        $flag = $this->user_model->delete_user($id);
        if ($flag) {
            //set flashdata
            $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Hapus Data Berhasil!</div>');
            redirect(base_url('user_manager'));
        } else {
            //set flashdata
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Hapus Data Gagal!</div>');
            redirect(base_url('user_manager'));
        }
    }

// NEWS

        public function news()
    {
        auth_redirect();
        $data['title']   = 'News';
        $data['content'] = VIEW_BACK . 'news';
        $data['member']  = get_current_member();
        $this->load->view(VIEW_BACK . 'template', $data);
    }


        function create_news()
    {
       $title                 = $this->input->post('title');
       $content               = $this->input->post('content');
       $datetime                     = date("Y-m-d H:i:s");
        $ok_ext                = array(
            'jpg',
            'png',
            'jpeg',
            'bmp'
        ); // allow only these types of files
        $destination    = 'assets/img/'; // where our files will be stored
        $save_dest      = 'img/';
        if (!empty($_FILES['file']['name'])) {
            $file           = $_FILES['file'];
            $filename       = explode(".", $file["name"]);
            $file_name      = $file['name']; // file original name
            $file_extension = $filename[count($filename) - 1];
            $file_weight    = $file['size'];
            $file_type      = $file['type'];
            // If there is no error
            if ($file['error'] == 0) {
                // check if the extension is accepted
                if (in_array(strtolower($file_extension), $ok_ext)) {
                    // check if the size is not beyond expected size
                    // rename the file
                    $fileNewName = str_replace(" ", "_", strtolower(uniqid())) . '.' . $file_extension;
                    // and move it to the destination folder
                    if (move_uploaded_file($file['tmp_name'], $destination . $fileNewName)) {
                        $foto_path = $destination . $fileNewName;
                        $save_path = $destination. $fileNewName;
        $data      = array(
                        'title'      => $title,
                        'content'    => $content,
                        'images'            => $save_path,
                        'dateCreated'       => $datetime,
                    );
                    $flag      = $this->news_model->create_news($data);
                    if ($flag) {
                        //set flashdata
                        $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Sukses!</div>');
                        redirect(base_url("news"));
                    } else {
                        //set flashdata
                        $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Gagal! Terjadi kesalahan pada database.</div>');
                        redirect(base_url("news"));
                    }
                    } else {
                        //upload gagal
                        //set flashdata
                        $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Gagal! Terjadi kesalahan pada database.</div>');
                        redirect(base_url("news"));
                    }
                } else {
                    //yg diupload bukan gambar
                    //set flashdata
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Gagal! Terjadi kesalahan pada database.</div>');
                    redirect(base_url("news"));
                }
            } else {
                //jika error
                //set flashdata
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Gagal! Terjadi kesalahan pada database.</div>');
                redirect(base_url("news"));
            }
        } else {
            //gak diupload
            //set flashdata
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Gagal! Terjadi kesalahan pada database.</div>');
            redirect(base_url("news"));
        }
    }


        function news_edit_process()
    {
        $id                 = $this->input->post('id');
        $title                 = $this->input->post('title');
        $content               = $this->input->post('content');
        $datetime                     = date("Y-m-d H:i:s");
            $ok_ext                = array(
                'jpg',
                'png',
                'jpeg',
                'bmp'
            ); // allow only these types of files
            $destination    = 'assets/img/'; // where our files will be stored
            $save_dest      = 'img/';
            if (!empty($_FILES['file']['name'])) {
                $file           = $_FILES['file'];
                $filename       = explode(".", $file["name"]);
                $file_name      = $file['name']; // file original name
                $file_extension = $filename[count($filename) - 1];
                $file_weight    = $file['size'];
                $file_type      = $file['type'];
                // If there is no error
                if ($file['error'] == 0) {
                    // check if the extension is accepted
                    if (in_array(strtolower($file_extension), $ok_ext)) {
                        // check if the size is not beyond expected size
                        // rename the file
                        $fileNewName = str_replace(" ", "_", strtolower(uniqid())) . '.' . $file_extension;
                        // and move it to the destination folder
                        if (move_uploaded_file($file['tmp_name'], $destination . $fileNewName)) {
                            $foto_path = $destination . $fileNewName;
                            $save_path = $destination. $fileNewName;
                            if(!empty($_FILES['file']['name'])){
                                
                                $data      = array(
                                    'id'         => $id,
                                    'title'      => $title,
                                    'content'    => $content,
                                    'images'            => $save_path,
                                    'dateModified'       => $datetime,
                                ); 
                                $flag      = $this->news_model->update_news($data);
      
                                    $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Sukses!</div>');
                                    redirect(base_url("news"));

                            }else {
                                
                            //upload gagal
                            //set flashdata
                            $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Gagal! Terjadi kesalahan pada database 2.</div>');
                            redirect(base_url("news"));
                            }
                        } else {
                            //upload gagal
                            //set flashdata
                            $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Gagal! Terjadi kesalahan pada database 2.</div>');
                            redirect(base_url("news"));
                        }
                    } else {
                        //yg diupload bukan gambar
                        //set flashdata
                        $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Gagal! Terjadi kesalahan pada database 3.</div>');
                        redirect(base_url("news"));
                    }
                } else {
                    //jika error
                    //set flashdata
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Gagal! Terjadi kesalahan pada database 4.</div>');
                    redirect(base_url("news"));
                }
            } else {
                                
                $data      = array(
                    'id'         => $id,
                    'title'      => $title,
                    'content'    => $content,
                    'dateModified'       => $datetime,
                ); 
                $flag      = $this->news_model->update_news($data);
                

                    $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Sukses!</div>');
                    redirect(base_url("news"));

            }
    }

        function delete_news($id)
    {
        $flag = $this->news_model->delete_news($id);
        if ($flag) {
            //set flashdata
            $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Hapus Data Berhasil!</div>');
            redirect(base_url('news'));
        } else {
            //set flashdata
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Hapus Data Gagal!</div>');
            redirect(base_url('news'));
        }
    }


        function news_edit($id = 0)
    {
       auth_redirect();
       $where      = array(
           'news.id' => $id
       );
       $edit_qry = $this->news_model->read_news($where);
       if ($edit_qry->num_rows() != 0) {
           $data['title']       = 'Edit News';
           $data['content']     = VIEW_BACK . 'news_edit';
           $data['member']      = get_current_member();
           $data['data_edit'] = $edit_qry->row();
           $this->load->view(VIEW_BACK . 'template', $data);
       } else
           $this->load->view('404');
    }



    // FAQ 


        public function faq()
    {
        auth_redirect();
        $data['title']   = 'FAQ';
        $data['content'] = VIEW_BACK . 'faq';
        $data['member']  = get_current_member();
        $this->load->view(VIEW_BACK . 'template', $data);
    }

        function create_faq()
    {
        $question                     = $this->input->post('question');
        $answer                       = $this->input->post('answer');

        $data      = array(
            'question'       => $question,
            'answer'         => $answer,
        );
        $flag      = $this->faq_model->create_faq($data);
        if ($flag) {
            //set flashdata
            $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Sukses!</div>');
            redirect(base_url("faq"));
        } else {
            //set flashdata
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Gagal! Terjadi kesalahan pada database.</div>');
            redirect(base_url("faq"));
        }
    }

        function faq_edit($id = 0)
    {
        auth_redirect();
        $where      = array(
            'faq.id' => $id
        );
        $edit_qry = $this->faq_model->read_faq($where);
        if ($edit_qry->num_rows() != 0) {
            $data['title']       = 'FAQ';
            $data['content']     = VIEW_BACK . 'faq_edit';
            $data['member']      = get_current_member();
            $data['data_edit'] = $edit_qry->row();
            $this->load->view(VIEW_BACK . 'template', $data);
        } else
            $this->load->view('404');
    }

        function faq_edit_process()
    {
        $id                           = $this->input->post('id');
        $question                     = $this->input->post('question');
        $answer                       = $this->input->post('answer');

        $data      = array(
            'id'            => $id,
            'question'      => $question,
            'answer'        => $answer,
        );
        $flag      = $this->faq_model->update_faq($data); 
        if ($flag) {
            //set flashdata
            $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Data Sukses!</div>');
            redirect(base_url("faq"));
        } else {
            //set flashdata
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Data Gagal! Terjadi kesalahan pada database.</div>');
            redirect(base_url("faq"));
        }
    }


        function delete_faq($id)
    {
        $flag = $this->faq_model->delete_faq($id);
        if ($flag) {
            //set flashdata
            $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Hapus Data Berhasil!</div>');
            redirect(base_url('faq'));
        } else {
            //set flashdata
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Hapus Data Gagal!</div>');
            redirect(base_url('faq'));
        }
    }



// Redeem Category 


    public function redeemcategory()
{
    auth_redirect();
    $data['title']   = 'Redeem Category';
    $data['content'] = VIEW_BACK . 'redeemcategory';
    $data['member']  = get_current_member();
    $this->load->view(VIEW_BACK . 'template', $data);
}

    function create_redeemcategory()
{
    $title                     = $this->input->post('title');
    $description               = $this->input->post('description');
    $price                     = $this->input->post('price');
    $pointRequired             = $this->input->post('pointRequired');
    $datetime                  = date("Y-m-d H:i:s");
    $ok_ext                = array(
        'jpg',
        'png',
        'jpeg',
        'bmp'
    ); // allow only these types of files
    $destination    = 'assets/img/'; // where our files will be stored
    $save_dest      = 'img/';
    if (!empty($_FILES['file']['name'])) {
        $file           = $_FILES['file'];
        $filename       = explode(".", $file["name"]);
        $file_name      = $file['name']; // file original name
        $file_extension = $filename[count($filename) - 1];
        $file_weight    = $file['size'];
        $file_type      = $file['type'];
        // If there is no error
        if ($file['error'] == 0) {
            // check if the extension is accepted
            if (in_array(strtolower($file_extension), $ok_ext)) {
                // check if the size is not beyond expected size
                // rename the file
                $fileNewName = str_replace(" ", "_", strtolower(uniqid())) . '.' . $file_extension;
                // and move it to the destination folder
                if (move_uploaded_file($file['tmp_name'], $destination . $fileNewName)) {
                    $foto_path = $destination . $fileNewName;
                    $save_path = $destination. $fileNewName;
                $data      = array(
                    'title'             => $title,
                    'description'       => $description,
                    'price'             => $price,
                    'pointRequired'     => $pointRequired,
                    'images'            => $save_path,
                    'dateCreated'       => $datetime,
                );
                $flag      = $this->redeemcategory_model->create_redeemcategory($data);
                if ($flag) {
                    //set flashdata
                    $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Sukses!</div>');
                    redirect(base_url("redeemcategory"));
                } else {
                    //set flashdata
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Gagal! Terjadi kesalahan pada database.</div>');
                    redirect(base_url("redeemcategory"));
                }
                } else {
                    //upload gagal
                    //set flashdata
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Gagal! Terjadi kesalahan pada database.</div>');
                    redirect(base_url("redeemcategory"));
                }
            } else {
                //yg diupload bukan gambar
                //set flashdata
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Gagal! Terjadi kesalahan pada database.</div>');
                redirect(base_url("redeemcategory"));
            }
        } else {
            //jika error
            //set flashdata
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Gagal! Terjadi kesalahan pada database.</div>');
            redirect(base_url("redeemcategory"));
        }
    } else {
        //gak diupload
        //set flashdata
        $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Gagal! Terjadi kesalahan pada database.</div>');
        redirect(base_url("redeemcategory"));
    }
}

    function redeemcategory_edit($id = 0)
{
    auth_redirect();
    $where      = array(
        'redeemcategory.id' => $id
    );
    $edit_qry = $this->redeemcategory_model->read_redeemcategory($where);
    if ($edit_qry->num_rows() != 0) {
        $data['title']       = 'Trash Category';
        $data['content']     = VIEW_BACK . 'redeemcategory_edit';
        $data['member']      = get_current_member();
        $data['data_edit'] = $edit_qry->row();
        $this->load->view(VIEW_BACK . 'template', $data);
    } else
        $this->load->view('404');
}

    function redeemcategory_edit_process()
{
    $id                        = $this->input->post('id');
    $title                     = $this->input->post('title');
    $description               = $this->input->post('description');
    $price                     = $this->input->post('price');
    $pointRequired             = $this->input->post('pointRequired');
    $datetime                     = date("Y-m-d H:i:s");
        $ok_ext                = array(
            'jpg',
            'png',
            'jpeg',
            'bmp'
        ); // allow only these types of files
        $destination    = 'assets/img/'; // where our files will be stored
        $save_dest      = 'img/';
        if (!empty($_FILES['file']['name'])) {
            $file           = $_FILES['file'];
            $filename       = explode(".", $file["name"]);
            $file_name      = $file['name']; // file original name
            $file_extension = $filename[count($filename) - 1];
            $file_weight    = $file['size'];
            $file_type      = $file['type'];
            // If there is no error
            if ($file['error'] == 0) {
                // check if the extension is accepted
                if (in_array(strtolower($file_extension), $ok_ext)) {
                    // check if the size is not beyond expected size
                    // rename the file
                    $fileNewName = str_replace(" ", "_", strtolower(uniqid())) . '.' . $file_extension;
                    // and move it to the destination folder
                    if (move_uploaded_file($file['tmp_name'], $destination . $fileNewName)) {
                        $foto_path = $destination . $fileNewName;
                        $save_path = $destination. $fileNewName;
                        if(!empty($_FILES['file']['name'])){
                            
                            $data      = array(
                                'id'         => $id,
                                'title'             => $title,
                                'description'       => $description,
                                'price'             => $price,
                                'pointRequired'     => $pointRequired,
                                'images'            => $save_path,
                                'dateModified'       => $datetime,
                            ); 
                            $flag      = $this->redeemcategory_model->update_redeemcategory($data);
    
                                $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Sukses!</div>');
                                redirect(base_url("redeemcategory"));

                        }else {
                            
                        //upload gagal
                        //set flashdata
                        $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Gagal! Terjadi kesalahan pada database 2.</div>');
                        redirect(base_url("redeemcategory"));
                        }
                    } else {
                        //upload gagal
                        //set flashdata
                        $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Gagal! Terjadi kesalahan pada database 2.</div>');
                        redirect(base_url("redeemcategory"));
                    }
                } else {
                    //yg diupload bukan gambar
                    //set flashdata
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Gagal! Terjadi kesalahan pada database 3.</div>');
                    redirect(base_url("redeemcategory"));
                }
            } else {
                //jika error
                //set flashdata
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Gagal! Terjadi kesalahan pada database 4.</div>');
                redirect(base_url("redeemcategory"));
            }
        } else {
                            
            $data      = array(
                'id'         => $id,
                'title'             => $title,
                'description'       => $description,
                'price'             => $price,
                'pointRequired'     => $pointRequired,
                'dateModified'       => $datetime,
            ); 
            $flag      = $this->redeemcategory_model->update_redeemcategory($data);
            

                $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Sukses!</div>');
                redirect(base_url("redeemcategory"));

        }
}


    function delete_redeemcategory($id)
{
    $flag = $this->redeemcategory_model->delete_redeemcategory($id);
    if ($flag) {
        //set flashdata
        $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Hapus Data Berhasil!</div>');
        redirect(base_url('redeemcategory'));
    } else {
        //set flashdata
        $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Hapus Data Gagal!</div>');
        redirect(base_url('redeemcategory'));
    }
}



// // Redeem


    function delete_redeem($id="")
{
    auth_redirect();
    if($id){
        $dataUpdate = array("id"=>$id,"status"=>"canceled", "dateModified"=>date("Y-m-d H:i:s"));
        $flag = $this->redeem_model->update_redeem($dataUpdate);
        if ($flag) {
            //set flashdata
            $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Delete Data Success!</div>');
            redirect(base_url('redeem'));
        } else {
            //set flashdata
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Delete Data Failed!</div>');
            redirect(base_url('redeem'));
        }
} else {
    //set flashdata
    $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>ID Is Required!</div>');
    redirect(base_url('redeem'));
}
}

function confirm_redeem($id="")
{
    auth_redirect();
    if($id){
        $dataUpdate = array("id"=>$id,"status"=>"accepted", "dateModified"=>date("Y-m-d H:i:s"));
        $flag = $this->redeem_model->update_redeem($dataUpdate);
        if ($flag) {
            //set flashdata
            $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Confirm Data Success!</div>');
            redirect(base_url('redeem'));
        } else {
            //set flashdata
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Confirm Data Failed!</div>');
            redirect(base_url('redeem'));
        }
} else {
    //set flashdata
    $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>ID Is Required!</div>');
    redirect(base_url('redeem'));
}
}



// Trash Category


    public function trashcategory()
{
    auth_redirect();
    $data['title']   = 'Trash Category';
    $data['content'] = VIEW_BACK . 'trashcategory';
    $data['member']  = get_current_member();
    $this->load->view(VIEW_BACK . 'template', $data);
}

    function create_trashcategory()
{
    $name                     = $this->input->post('name');
    $unitId                     = $this->input->post('unit');
    $min_weight                     = $this->input->post('min_weight');
    $datetime                  = date("Y-m-d H:i:s");

    $data      = array(
        'name'              => $name,
        'unitId'            => $unitId,
        'point_value'            => $point_value,
        'min_weight'        => $min_weight,
        'dateCreated'       => $datetime,
    );
    $flag      = $this->trashcategory_model->create_trashcategory($data);
    if ($flag) {
        //set flashdata
        $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Sukses!</div>');
        redirect(base_url("trashcategory"));
    } else {
        //set flashdata
        $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Gagal! Terjadi kesalahan pada database.</div>');
        redirect(base_url("trashcategory"));
    }
}

    function trashcategory_edit($id = 0)
{
    auth_redirect();
    $where      = array(
        'trashcategory.id' => $id
    );
    $edit_qry = $this->trashcategory_model->read_trashcategory($where);
    if ($edit_qry->num_rows() != 0) {
        $data['title']       = 'Trash Category';
        $data['content']     = VIEW_BACK . 'trashcategory_edit';
        $data['member']      = get_current_member();
        $data['data_edit'] = $edit_qry->row();
        $this->load->view(VIEW_BACK . 'template', $data);
    } else
        $this->load->view('404');
}

    function trashcategory_edit_process()
{
    $id                           = $this->input->post('id');
    $name                         = $this->input->post('name');
    $unitId                     = $this->input->post('unit');
    $point_value                     = $this->input->post('point_value');
    $min_weight                     = $this->input->post('min_weight');
    $datetime                     = date("Y-m-d H:i:s");

    $data      = array(
        'id'            => $id,
        'name'              => $name,
        'unitId'            => $unitId,
        'point_value'       => $point_value,
        'min_weight'        => $min_weight,
        'dateModified'       => $datetime,
    );
    $flag      = $this->trashcategory_model->update_trashcategory($data); 
    if ($flag) {
        //set flashdata
        $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Data Sukses!</div>');
        redirect(base_url("trashcategory"));
    } else {
        //set flashdata
        $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Data Gagal! Terjadi kesalahan pada database.</div>');
        redirect(base_url("trashcategory"));
    }
}


    function delete_trashcategory($id)
{
    $flag = $this->trashcategory_model->delete_trashcategory($id);
    if ($flag) {
        //set flashdata
        $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Hapus Data Berhasil!</div>');
        redirect(base_url('trashcategory'));
    } else {
        //set flashdata
        $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Hapus Data Gagal!</div>');
        redirect(base_url('trashcategory'));
    }
}




    function penerimaan_produk_edit($id = 0)
{
    auth_redirect();
    $where      = array(
        'id_produk' => $id
    );
    $edit_qry = $this->penerimaan_produk_model->read_produk($where);
    if ($edit_qry->num_rows() != 0) {
        $data['title']       = 'Edit penerimaan produk';
        $data['content']     = VIEW_BACK . 'penerimaan_produk_edit';
        $data['member']      = get_current_member();
        $data['data_edit'] = $edit_qry->row();
        $this->load->view(VIEW_BACK . 'template', $data);
    } else
        $this->load->view('404');
}

    function login()
    {
      $user_login = $this->session->userdata('user_login');
        if (empty($user_login))
            $this->load->view(VIEW_BACK . 'login');
        else
            redirect(base_url('/'));
    }

    function cetak_nota($id_penjualan=''){
      if($id_penjualan!=''){
        //get nota id
        $nota_qry = $this->nota_model->read_single_nota($id_penjualan);
        if($nota_qry->num_rows()!=0){
          $nota_id = $nota_qry->row()->id_nota;
          auth_redirect();
          $where      = array(
              'id_penjualan' => $id_penjualan
          );
          $qry = $this->penjualan_detail_model->read_penjualan_detail($where);
          if ($qry->num_rows() != 0){
            $where = array('id_penjualan'=>$id_penjualan);
            $data_penjualan = $this->penjualan_model->read_penjualan($where);
            if($data_penjualan->num_rows()!=0){
              $where = array('id_pelanggan'=>$data_penjualan->row()->id_pelanggan);
              $data_pelanggan        = $this->pelanggan_model->read_pelanggan($where);
              if($data_pelanggan->num_rows()!=0){
              $data['title']       = 'Cetak Nota';
              $data['member']      = get_current_member();
              $data['nota']        = $qry;
              $data['tanggal_penjualan'] = $data_penjualan->row()->tgl_penjualan;
              $data['nama_pelanggan'] = $data_pelanggan->row()->nama_pelanggan;
              $data['alamat_pelanggan'] = $data_pelanggan->row()->alamat_pelanggan;
              $data['total_keseluruhan'] = $data_penjualan->row()->total_keseluruhan;
//               $data['diskon'] = $data_penjualan->row()->diskon;
              $data['id_nota'] = $nota_id;
              $this->load->view(VIEW_BACK . 'cetak_nota', $data);
            } else redirect(base_url('404'));
            } else redirect(base_url('404'));
          } else redirect(base_url('404'));
        } else redirect(base_url('404'));
      } else redirect(base_url('404'));
    }
    
    function cetak_sj($id_penjualan=''){
      if($id_penjualan!=''){
        //get nota id
        $sj_query = $this->surat_jalan_model->read_single_sj($id_penjualan);
        if($sj_query->num_rows()!=0){
          $id_surat_jalan = $sj_query->row()->id_surat_jalan;
          auth_redirect();
          $where      = array(
              'id_penjualan' => $id_penjualan
          );
          $qry = $this->penjualan_detail_model->read_penjualan_detail($where);
          if ($qry->num_rows() != 0){
            $where = array('id_penjualan'=>$id_penjualan);
            $data_penjualan = $this->penjualan_model->read_penjualan($where);
            if($data_penjualan->num_rows()!=0){
              $where = array('id_pelanggan'=>$data_penjualan->row()->id_pelanggan);
              $data_pelanggan        = $this->pelanggan_model->read_pelanggan($where);
              if($data_pelanggan->num_rows()!=0){
              $data['title']       = 'Cetak Surat Jalan';
              $data['member']      = get_current_member();
              $data['nota']        = $qry;
              $data['tanggal_penjualan'] = $data_penjualan->row()->tgl_penjualan;
              $data['nama_pelanggan'] = $data_pelanggan->row()->nama_pelanggan;
              $data['alamat_pelanggan'] = $data_pelanggan->row()->alamat_pelanggan;
              $data['telp_pelanggan'] = $data_pelanggan->row()->telp_pelanggan;
              $data['id_sj'] = $id_surat_jalan;
              $this->load->view(VIEW_BACK . 'cetak_sj', $data);
            } else redirect(base_url('404'));
            } else redirect(base_url('404'));
          } else redirect(base_url('404'));
        } else redirect(base_url('404'));
      } else redirect(base_url('404'));
    }

    

    function login_process()
    {
        $email    = $this->input->post('email');
        $password = hash('md5', $this->input->post('password'));
        $where    = array(
            'email'=>$email,
            'password'=>$password
        );
        $flag     = $this->user_model->read_user($where)->num_rows();
        if ($flag) {
            $this->session->set_userdata('user_login', $email);
            $this->session->set_userdata('lang_admin', 'en');
            $data = array(
                'info' => 'success',
                'message' => '<div class="alert alert-success"><strong>Sukses!</strong> Sesaat lagi anda akan masuk..</div>'
            );
            die(json_encode($data));
            redirect(base_url(''));
        } else {
            $data = array(
                'info' => 'failed',
                'message' => '<div class="alert alert-danger"><strong>Error!</strong> Login Gagal! Silahkan Cek Email/Password Anda!</div>'
            );
            die(json_encode($data));
        }
    }

    function logout()
    {
        $this->session->set_userdata('user_login', "");
        redirect(base_url('login'));
    }

    function verification($code){
        $where = array("verification"=>$code);
        $check = $this->user_model->read_user($where);
        if($check->num_rows()>0){
            //update data
            $dataUpdate = array("id"=>$check->row()->id,"status"=>"active", "dateModified"=>date('Y-m-d H:i:s'));
            $user = $this->user_model->update_user($dataUpdate);
            if($user){
                //set flashdata
                $this->session->set_flashdata('message', '<div class="alert alert-success"><strong>Success!</strong> Verification Success! You can use your account now!</div>');
                redirect(base_url('verification_result'));
            } else {
                //set flashdata
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error!</strong> Verification Failed! Invalid URL!</div>');
                redirect(base_url('verification_result'));
            }
        } else {
            //set flashdata
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error!</strong> Verification Failed! Invalid URL!</div>');
            redirect(base_url('verification_result'));
        }
    }

    function trash_request(){
        auth_redirect();
        $data['title']       = 'Trash Request';
        $data['content']     = VIEW_BACK . 'trash_request';
        $data['member']      = get_current_member();
        $this->load->view(VIEW_BACK . 'template', $data);
    }

    function confirm_trash_request($id=""){
        if($id){
            $sql = "SELECT A.*, 
            B.name customer_name, 
            B.phoneNumber customer_phone, 
            C.name mitra_name, 
            C.jenis_kendaraan mitra_jenis_kendaraan, 
            C.no_plat_kendaraan mitra_no_plat_kendaraan ,
            D.name category_name,
            D.point_value point_value
            FROM trashrequest AS A 
            JOIN user AS B on A.userId = B.id 
            LEFT JOIN user AS C on A.mitraId = C.id 
            LEFT JOIN trashcategory AS D on A.trashCategoryId = D.id
            WHERE A.id='".$id."'";
            $trashrequest = $this->trashrequest_model->custom_sql($sql);
            if($trashrequest->num_rows()>0){
                if($trashrequest->row()->mitraId!=NULL && $trashrequest->row()->status=="pending"){
                //load data pembagian
                $pembagian_mitra = get_settings('pembagian_mitra')/100;
                $pembagian_customer = get_settings('pembagian_customer')/100;
                $pembagian_pengepul = get_settings('pembagian_pengepul')/100;
                $nilai_point = get_settings('nilai_point');
                $total_point = 0;
                $total_point+=$trashrequest->row()->point_value*$trashrequest->row()->weight;
                //penghitungan point
                $point_mitra = $total_point*$pembagian_mitra;
                $point_customer = $total_point*$pembagian_customer;
                $point_pengepul = $total_point*$pembagian_pengepul;
                //update request
                $dataUpdate = array("id"=>$trashrequest->row()->id,"status"=>"completed", "dateModified"=>date("Y-m-d H:i:s"));
                $flag = $this->trashrequest_model->update_trashrequest($dataUpdate);
                if($flag>0){
                    $mitra = get_single_user($trashrequest->row()->mitraId);
                    //set point mitra
                    $dataUpdate = array("id"=>$trashrequest->row()->mitraId,"point"=>(FLOAT)$point_mitra+(FLOAT)$mitra->point, "dateModified"=>date("Y-m-d H:i:s"));
                    $flag = $this->user_model->update_user($dataUpdate);
                    if($flag>0){
                        $token = $mitra->firebase_token;
                        $data = array(
                            "type"=>"point",
                            "message"=>"Anda baru saja mendapatkan point sebesar ".$point_mitra,
                            "token"=>$token
                        );
                        send_firebase_notification($data);
                        //create point history
                        $dataCreate = array("userId"=>$trashrequest->row()->mitraId, 
                        "pointValue"=>$point_mitra, 
                        "type"=>"in",
                        "description"=>"Get Point From Trash Request",
                        "dateCreated"=>date("Y-m-d H:i:s"),
                        "dateModified"=>date("Y-m-d H:i:s"));
                        $flag = $this->point_model->create_point($dataCreate);
                        if($flag>0){
                            $customer = get_single_user($trashrequest->row()->userId);
                            //set point customer
                            $dataUpdate = array("id"=>$trashrequest->row()->userId,"point"=>(FLOAT)$point_customer+(FLOAT)$customer->point, "dateModified"=>date("Y-m-d H:i:s"));
                            $flag = $this->user_model->update_user($dataUpdate);
                            if($flag>0){
                                $token = $customer->firebase_token;
                                $data = array(
                                    "type"=>"point",
                                    "message"=>"Anda baru saja mendapatkan point sebesar ".$point_customer,
                                    "token"=>$token
                                );
                                send_firebase_notification($data);
                                //create point history
                                $dataCreate = array("userId"=>$trashrequest->row()->userId, 
                                "pointValue"=>$point_customer, 
                                "type"=>"in",
                                "description"=>"Get Point From Trash Request",
                                "dateCreated"=>date("Y-m-d H:i:s"),
                                "dateModified"=>date("Y-m-d H:i:s"));
                                $flag = $this->point_model->create_point($dataCreate);
                                if($flag>0){
                                    //Create point history for pengepul
                                    $dataCreate = array("userId"=>0, 
                                    "pointValue"=>$point_pengepul, 
                                    "type"=>"in",
                                    "description"=>"Get Point From Trash Request",
                                    "dateCreated"=>date("Y-m-d H:i:s"),
                                    "dateModified"=>date("Y-m-d H:i:s"));
                                    $flag = $this->point_model->create_point($dataCreate);
                                    if($flag>0){
                                        //set flashdata
                                        $this->session->set_flashdata('message', '<div class="alert alert-success"><strong>Success!</strong> Confirm Request Success!</div>');
                                        redirect(base_url('/trash_request'));
                                    } else {
                                        //set flashdata
                                        $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error!</strong> Confirm Request Failed!</div>');
                                        redirect(base_url('/trash_request'));
                                    }
                                } else {
                                    //set flashdata
                                    $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error!</strong> Confirm Request Failed!</div>');
                                    redirect(base_url('/trash_request'));
                                }
                                } else {
                                    //set flashdata
                                    $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error!</strong> Confirm Request Failed!</div>');
                                    redirect(base_url('/trash_request'));
                                }
                    } else {
                        //set flashdata
                        $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error!</strong> Confirm Request Failed!</div>');
                        redirect(base_url('/trash_request'));
                    }
                } else {
                    //set flashdata
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error!</strong> Confirm Request Failed!</div>');
                    redirect(base_url('/trash_request'));
                }
            }
        } else {
            //set flashdata
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error!</strong> Request is alread confirmed!</div>');
            redirect(base_url('/trash_request'));
        }
        } else {
            //set flashdata
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error!</strong> Invalid ID Request!</div>');
            redirect(base_url('/trash_request'));
        }
    } else {
        //set flashdata
        $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error!</strong> ID is required!</div>');
        redirect(base_url('/trash_request'));
    }
    }

    function delete_trash_request($id=""){
        if($id){
        $dataUpdate = array("id"=>$id, "status"=>"canceled", "dateModified"=>date("Y-m-d H:i:s"));
        $flag = $this->trashrequest_model->update_trashrequest($dataUpdate);
        if($flag){
            //set flashdata
            $this->session->set_flashdata('message', '<div class="alert alert-success"><strong>Success!</strong> Delete Request Success!</div>');
            redirect(base_url('/trash_request'));
        } else {
            //set flashdata
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error!</strong> Invalid Request ID!</div>');
            redirect(base_url('/trash_request'));
        }
        } else {
        //set flashdata
        $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error!</strong> ID is required!</div>');
        redirect(base_url('/trash_request'));
        }
    }


    // function delete_redeem($id=""){
    //     if($id){
    //     $dataUpdate = array("id"=>$id, "status"=>"canceled", "dateModified"=>date("Y-m-d H:i:s"));
    //     $flag = $this->trashrequest_model->update_trashrequest($dataUpdate);
    //     if($flag){
    //         //set flashdata
    //         $this->session->set_flashdata('message', '<div class="alert alert-success"><strong>Success!</strong> Delete Request Success!</div>');
    //         redirect(base_url('/trash_request'));
    //     } else {
    //         //set flashdata
    //         $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error!</strong> Invalid Request ID!</div>');
    //         redirect(base_url('/trash_request'));
    //     }
    //     } else {
    //     //set flashdata
    //     $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error!</strong> ID is required!</div>');
    //     redirect(base_url('/trash_request'));
    //     }
    // }

// USER 


public function unit()
{
    auth_redirect();
    $data['title']   = 'Unit Manager';
    $data['content'] = VIEW_BACK . 'unit';
    $data['member']  = get_current_member();
    $this->load->view(VIEW_BACK . 'template', $data);
}

    function create_unit()
{
    $name                         = $this->input->post('name');
    $datetime                     = date("Y-m-d H:i:s");

    $data      = array(
        'name'          => $name,
        'dateCreated'   => $datetime
    );


    $flag      = $this->unit_model->create_unit($data);
    if ($flag) {
        //set flashdata
        $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Sukses!</div>');
        redirect(base_url("unit"));
    } else {
        //set flashdata
        $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Gagal! Terjadi kesalahan pada database.</div>');
        redirect(base_url("unit"));
    }
}

    function unit_edit($id = 0)
{
    auth_redirect();
    $where      = array(
        'unit.id' => $id
    );
    $edit_qry = $this->unit_model->read_unit($where);
    if ($edit_qry->num_rows() != 0) {
        $data['title']       = 'Edit Unit';
        $data['content']     = VIEW_BACK . 'unit_edit';
        $data['member']      = get_current_member();
        $data['data_edit'] = $edit_qry->row();
        $this->load->view(VIEW_BACK . 'template', $data);
    } else
        $this->load->view('404');
}

    function unit_edit_process()
{
  $id                           = $this->input->post('id');
  $name                         = $this->input->post('name');
  $datetime                     = date("Y-m-d H:i:s");

    $data      = array(
        'id'            => $id,
        'name'          => $name,
        'datemodified'  => $datetime
    );
    $flag      = $this->unit_model->update_unit($data); 
    if ($flag) {
        //set flashdata
        $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Data Sukses!</div>');
        redirect(base_url("unit"));
    } else {
        //set flashdata
        $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Data Gagal! Terjadi kesalahan pada database.</div>');
        redirect(base_url("unit"));
    }
}


function delete_unit($id)
{
    $flag = $this->unit_model->delete_unit($id);
    if ($flag) {
        //set flashdata
        $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Hapus Data Berhasil!</div>');
        redirect(base_url('unit'));
    } else {
        //set flashdata
        $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Hapus Data Gagal!</div>');
        redirect(base_url('unit'));
    }
}


    function verification_result(){
        $this->load->view(VIEW_BACK . 'verification_result');
    }

}