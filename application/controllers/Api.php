<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

  function __construct(){
    parent::__construct();
    date_default_timezone_set("Asia/Jakarta");
  }
  
  function signin(){
    $email = $this->input->post("email");
    $password = hash('md5', $this->input->post("password"));
    $where = array("email"=>$email, "password"=>$password, "role!="=>"admin");
    $user = $this->user_model->read_user($where);
    if($user->num_rows()>0){
      if($user->row()->status=="active"){
      //create session
      $now = date("Y-m-d H:i:s");
      $dataCreate = array("userId"=>$user->row()->id,"key"=>uniqid(rand(0,100)), "dateCreated"=>$now, "dateModified"=>$now);
      $session = $this->session_model->create_session($dataCreate);
      if($session>0){
        $dataCreate["user"]=$user->row();
        //update firebase token
        $dataUpdate = array("id"=>$user->row()->id, "firebase_token"=>$this->input->post("firebase_token"), "dateModified"=>$now);  
        $flag = $this->user_model->update_user($dataUpdate);
        if($flag){
          //read session
          $ret = array("status"=>200, "message"=>"Sign-in Sukses", "data"=>$dataCreate);
          die(json_encode($ret));
        } else {
          //read session
          $ret = array("status"=>200, "message"=>"Sign-in Sukses", "data"=>$dataCreate);
          die(json_encode($ret));
        }
      } else {
        $ret = array("status"=>400, "message"=>"Sign-in Gagal");
        die(json_encode($ret));
      }
    } else {
      $ret = array("status"=>400, "message"=>"Your account hasn't active yet, please check your email to activate your account.");
      die(json_encode($ret));
    }
    } else {
      $ret = array("status"=>400, "message"=>"Invalid email/password");
      die(json_encode($ret));
    }
  }

  function signout(){
    $headers = $this->input->request_headers();
    $key = $headers["key"];
    if($key){
      $where = array("key"=>$key);
      $session = $this->session_model->read_session($where);
    if($session->num_rows()>0){
      $where = array("id"=>$session->row()->userId, "firebase_token"=>"");
      $user = $this->user_model->update_user($where);
      if($user){
        $session = $this->session_model->delete_session($key);
        if($session>0){

          $ret = array("status"=>200, "message"=>"Sign Out Sukses");
          die(json_encode($ret));
        } else {
          $ret = array("status"=>400, "message"=>"Sign Out Gagal");
          die(json_encode($ret));
        }
      }
  } else {
    $ret = array("status"=>400, "message"=>"Key Session Diperlukan");
    die(json_encode($ret));
  }
  }
}

  function my_profile(){
    $headers = $this->input->request_headers();
    $key = $headers["key"];
    if($key){
      $where = array("key"=>$key);
    $session = $this->session_model->read_session($where);
    if($session->num_rows()>0){
      $where = array("id"=>$session->row()->userId);
      $sql = "SELECT A.*, 
      B.name province_name, 
      C.name regency_name, 
      D.name district_name

      FROM user AS A
      LEFT JOIN provinces as B ON B.id = A.provinceId
      LEFT JOIN regencies as C ON C.id = A.regencyId
      LEFT JOIN districts as D ON D.id = A.districtId
      WHERE A.id='".$session->row()->userId."'
      ";
      $user = $this->user_model->custom_sql($sql);
      if($user->num_rows()>0){
        $ret = array("status"=>200, "message"=>"Get Data Sukses", "data"=>$user->row());
        die(json_encode($ret));
      } else {
        $ret = array("status"=>400, "message"=>"Get Data Gagal");
        die(json_encode($ret));
      }
    } else {
      $ret = array("status"=>400, "message"=>"Get Data Gagal");
      die(json_encode($ret));
    }
  } else {
    $ret = array("status"=>400, "message"=>"Key Session Diperlukan");
    die(json_encode($ret));
  }
  }

  function sign_up(){
    $data['name'] = $this->input->post("name");
    $data['password'] = hash('md5',$this->input->post("password"));
    $data['email'] = $this->input->post("email");
    $data['verification'] = hash('md5',$this->input->post("email"));
    $data['phoneNumber'] = $this->input->post("phoneNumber");
    $data['address'] = $this->input->post("address");
    $data['role'] = $this->input->post("role");
    $data['provinceId'] = $this->input->post("provinceId");
    $data['regencyId'] = $this->input->post("regencyId");
    $data['ktp'] = $this->input->post("ktp")?$this->input->post("ktp"):"";
    $data['sim'] = $this->input->post("sim")?$this->input->post("sim"):"";
    $data['masa_berlaku_sim'] = $this->input->post("masa_berlaku_sim")?$this->input->post("masa_berlaku_sim"):"";
    $data['jenis_kendaraan'] = $this->input->post("jenis_kendaraan")?$this->input->post("jenis_kendaraan"):"";
    $data['no_plat_kendaraan'] = $this->input->post("no_plat_kendaraan")?$this->input->post("no_plat_kendaraan"):"";
    $data['status'] = "nonactive";
    $data['point'] = 0;
    $data['dateCreated'] = date("Y-m-d H:i:s");
    $data['dateModified'] = date("Y-m-d H:i:s");
    $ktp         = upload_file("ktp", "images", 'assets/img/upload/');
    if($ktp!="empty" && $ktp!="error" && $ktp!="error_extension" && $ktp!="error_upload") $data['foto_ktp'] = $ktp;
    $sim         = upload_file("sim", "images", 'assets/img/upload/');
    if($sim!="empty" && $sim!="error" && $sim!="error_extension" && $sim!="error_upload") $data['foto_sim'] = $sim;
    if($data['name'] && $data['password'] && $data['email'] && $data['phoneNumber'] && $data['address'] && $data['provinceId'] && $data['regencyId']){
      //check if email is already registered
      $where = array("email"=>$data['email']);
      $user = $this->user_model->read_user($where);
      if($user->num_rows()==0){
      $member = $this->user_model->create_user($data);
      if($member){
        //send verification email
        $verification_url = base_url('verification/'.hash('md5',$data['email']));
        $this->mailer->send_email_verification($data, $verification_url);
        $ret = array("status"=>200, "message"=>"Sign Up Sukses, Please Check your email to activate your account.");
        die(json_encode($ret));
      } else {
        $ret = array("status"=>400, "message"=>"Gagal to Sign Up");
        die(json_encode($ret));
      }
    } else {
      $ret = array("status"=>400, "message"=>"Email is already registered");
      die(json_encode($ret));
    }
    } else {
    $ret = array("status"=>400, "message"=>"All fields is required", "data"=>$data);
    die(json_encode($ret));
    }
  }

  function news(){
    $headers = $this->input->request_headers();
    $key = $headers["key"];
    if($key){
      $where = array("key"=>$key);
    $session = $this->session_model->read_session($where);
    if($session->num_rows()>0){
      $where = array("id"=>$session->row()->userId);
      $user = $this->user_model->read_user($where);
      if($user->num_rows()>0){
        $news = $this->news_model->read_news();
        if($news->num_rows()>0) $news_list = $news->result();
        else $news_list = "";
        $ret = array("status"=>200, "message"=>"Get Data Sukses", "data"=>$news_list);
        die(json_encode($ret));
      } else {
        $ret = array("status"=>400, "message"=>"Get Data Gagal");
        die(json_encode($ret));
      }
    } else {
      $ret = array("status"=>400, "message"=>"Get Data Gagal");
      die(json_encode($ret));
    }
  } else {
    $ret = array("status"=>400, "message"=>"Key Session Diperlukan");
    die(json_encode($ret));
  }
  }

  function provinces(){
    $data = $this->provinces_model->read_provinces();
    if($data->num_rows()>0) $data_list = $data->result();
    else $data_list = "";
    $ret = array("status"=>200, "message"=>"Get Data Sukses", "data"=>$data_list);
    die(json_encode($ret));
  }
  

  function regencies(){
    $province_id = $this->input->get('province_id');
    $where = array("province_id"=>$province_id);
    $data = $this->regencies_model->read_regencies($where);
    if($data->num_rows()>0) $data_list = $data->result();
    else $data_list = "";
    $ret = array("status"=>200, "message"=>"Get Data Sukses", "data"=>$data_list);
    die(json_encode($ret));
  }

  function setting(){
    $name = $this->input->get('name');
    $data = $this->setting_model->read_setting($name);
    if($data->num_rows()>0) $data_list = $data->result();
    else $data_list = "";
    $ret = array("status"=>200, "message"=>"Get Data Sukses", "data"=>$data_list);
    die(json_encode($ret));
  }

  function districts(){
    $regency_id = $this->input->get('regency_id');
    $where = array("regency_id"=>$regency_id);
    $data = $this->districts_model->read_districts($where);
    if($data->num_rows()>0) $data_list = $data->result();
    else $data_list = "";
    $ret = array("status"=>200, "message"=>"Get Data Sukses", "data"=>$data_list);
    die(json_encode($ret));
  }

  function villages(){
    $disctrict_id = $this->input->get('disctrict_id');
    $where = array("disctrict_id"=>$disctrict_id);
    $data = $this->villages_model->read_villages($where);
    if($data->num_rows()>0) $data_list = $data->result();
    else $data_list = "";
    $ret = array("status"=>200, "message"=>"Get Data Sukses", "data"=>$data_list);
    die(json_encode($ret));
  }

  function faq(){
    $headers = $this->input->request_headers();
    $faqcategoryid = $this->input->get('faqcategoryid');
    $key = $headers["key"];
    if($key){
      $where = array("key"=>$key);
    $session = $this->session_model->read_session($where);
    if($session->num_rows()>0){
      $where = array("id"=>$session->row()->userId);
      $user = $this->user_model->read_user($where);
      if($user->num_rows()>0){
        $sql = "SELECT A.*, B.name faq_category_name FROM faq AS A
        JOIN faq_category AS B ON A.faqCategoryId = B.id WHERE A.faqCategoryId = '".$faqcategoryid."'";
        $data_list = $this->faq_model->custom_sql($sql);
        if($data_list->num_rows()>0) $data_in = $data_list->result();
        else $data_in = "";
        $ret = array("status"=>200, "message"=>"Get Data Sukses", "data"=>$data_in);
        die(json_encode($ret));
      } else {
        $ret = array("status"=>400, "message"=>"Get Data Gagal");
        die(json_encode($ret));
      }
    } else {
      $ret = array("status"=>400, "message"=>"Get Data Gagal");
      die(json_encode($ret));
    }
  } else {
    $ret = array("status"=>400, "message"=>"Key Session Diperlukan");
    die(json_encode($ret));
  }
  }

  function point_summary(){
    $headers = $this->input->request_headers();
    $key = $headers["key"];
    if($key){
      $where = array("key"=>$key);
    $session = $this->session_model->read_session($where);
    if($session->num_rows()>0){
      $where = array("id"=>$session->row()->userId);
      $user = $this->user_model->read_user($where);
      if($user->num_rows()>0){
        $sql = "SELECT IFNULL(sum(CASE WHEN type='out' THEN pointValue END),0) as total_out,IFNULL(sum(CASE WHEN type='in' THEN pointValue END),0) as total_in  FROM `point` WHERE userId='".$user->row()->id."'";
        $data_list = $this->user_model->custom_sql($sql);
        $data_point = array("total_in"=>$data_list->row()->total_in, "total_out"=>$data_list->row()->total_out, "total_point"=>$user->row()->point);
        $ret = array("status"=>200, "message"=>"Get Data Sukses", "data"=>$data_point);
        die(json_encode($ret));
      } else {
        $ret = array("status"=>400, "message"=>"Get Data Gagal");
        die(json_encode($ret));
      }
    } else {
      $ret = array("status"=>400, "message"=>"Get Data Gagal");
      die(json_encode($ret));
    }
  } else {
    $ret = array("status"=>400, "message"=>"Key Session Diperlukan");
    die(json_encode($ret));
  }
  }

  function redeem_category(){
    $headers = $this->input->request_headers();
    $key = $headers["key"];
    if($key){
      $where = array("key"=>$key);
    $session = $this->session_model->read_session($where);
    if($session->num_rows()>0){
      $where = array("id"=>$session->row()->userId);
      $user = $this->user_model->read_user($where);
      if($user->num_rows()>0){
        $data_list = $this->redeemcategory_model->read_redeemcategory();
        if($data_list->num_rows()>0) $data_in = $data_list->result();
        else $data_in = "";
        $ret = array("status"=>200, "message"=>"Get Data Sukses", "data"=>$data_in);
        die(json_encode($ret));
      } else {
        $ret = array("status"=>400, "message"=>"Get Data Gagal");
        die(json_encode($ret));
      }
    } else {
      $ret = array("status"=>400, "message"=>"Get Data Gagal");
      die(json_encode($ret));
    }
  } else {
    $ret = array("status"=>400, "message"=>"Key Session Diperlukan");
    die(json_encode($ret));
  }
  }

  function faq_category(){
    $headers = $this->input->request_headers();
    $key = $headers["key"];
    if($key){
      $where = array("key"=>$key);
    $session = $this->session_model->read_session($where);
    if($session->num_rows()>0){
      $where = array("id"=>$session->row()->userId);
      $user = $this->user_model->read_user($where);
      if($user->num_rows()>0){
        $data_list = $this->faq_category_model->read_faq_category();
        if($data_list->num_rows()>0) $data_in = $data_list->result();
        else $data_in = "";
        $ret = array("status"=>200, "message"=>"Get Data Sukses", "data"=>$data_in);
        die(json_encode($ret));
      } else {
        $ret = array("status"=>400, "message"=>"Get Data Gagal");
        die(json_encode($ret));
      }
    } else {
      $ret = array("status"=>400, "message"=>"Get Data Gagal");
      die(json_encode($ret));
    }
  } else {
    $ret = array("status"=>400, "message"=>"Key Session Diperlukan");
    die(json_encode($ret));
  }
  }

  function unit(){
    $headers = $this->input->request_headers();
    $key = $headers["key"];
    if($key){
      $where = array("key"=>$key);
    $session = $this->session_model->read_session($where);
    if($session->num_rows()>0){
      $where = array("id"=>$session->row()->userId);
      $user = $this->user_model->read_user($where);
      if($user->num_rows()>0){
        $data_list = $this->unit_model->read_unit();
        if($data_list->num_rows()>0) $data_in = $data_list->result();
        else $data_in = "";
        $ret = array("status"=>200, "message"=>"Get Data Sukses", "data"=>$data_in);
        die(json_encode($ret));
      } else {
        $ret = array("status"=>400, "message"=>"Get Data Gagal");
        die(json_encode($ret));
      }
    } else {
      $ret = array("status"=>400, "message"=>"Get Data Gagal");
      die(json_encode($ret));
    }
  } else {
    $ret = array("status"=>400, "message"=>"Key Session Diperlukan");
    die(json_encode($ret));
  }
  }

  function trash_category(){
    $headers = $this->input->request_headers();
    $key = $headers["key"];
    if($key){
      $where = array("key"=>$key);
    $session = $this->session_model->read_session($where);
    if($session->num_rows()>0){
      $where = array("id"=>$session->row()->userId);
      $user = $this->user_model->read_user($where);
      if($user->num_rows()>0){
        $sql = "SELECT A.*, B.name nama_satuan FROM trashcategory AS A
        JOIN unit AS B ON A.unitId";
        $data_list = $this->trashcategory_model->custom_sql($sql);
        if($data_list->num_rows()>0) $data_in = $data_list->result();
        else $data_in = "";
        $ret = array("status"=>200, "message"=>"Get Data Sukses", "data"=>$data_in);
        die(json_encode($ret));
      } else {
        $ret = array("status"=>400, "message"=>"Get Data Gagal");
        die(json_encode($ret));
      }
    } else {
      $ret = array("status"=>400, "message"=>"Get Data Gagal");
      die(json_encode($ret));
    }
  } else {
    $ret = array("status"=>400, "message"=>"Key Session Diperlukan");
    die(json_encode($ret));
  }
  }

  function trash_request(){
    $headers = $this->input->request_headers();
    $key = $headers["key"];
    if($key){
      $where = array("key"=>$key);
    $session = $this->session_model->read_session($where);
    if($session->num_rows()>0){
      $where = array("id"=>$session->row()->userId);
      $user = $this->user_model->read_user($where);
      if($user->num_rows()>0){
        $dataCreate = array(
          "userId"=>$user->row()->id,
          "status"=>"pending", 
          "lat"=>$this->input->post("lat"),
          "lon"=>$this->input->post("lon"), 
          "trashCategoryId"=>$this->input->post('trashCategoryId'),
          "weight"=>$this->input->post('weight'),
          "dateCreated"=>date("Y-m-d H:i:s"), 
          "dateModified"=>date("Y-m-d H:i:s"));
        $trashrequest = $this->trashrequest_model->create_trashrequest($dataCreate);
        if($trashrequest){
          //send notif to all user
          $where = array("role"=>"mitra");
          $users = $this->user_model->read_user($where);
          $firebase = array();
          if($users->num_rows()>0){
            foreach($users->result() as $row){
              $data = array(
                "type"=>"trash_request",
                "message"=>"Ada sampah baru, silahkan buka aplikasi Anda",
                "token"=>$row->firebase_token
              );
              send_firebase_notification($data);
            }
          }
          $ret = array("status"=>200, "message"=>"Submit Request Sukses");
          die(json_encode($ret));
          } else {
            $ret = array("status"=>400, "message"=>"Submit Request Gagal");
            die(json_encode($ret));
          }
      } else {
        $ret = array("status"=>400, "message"=>"User tidak dikenali");
        die(json_encode($ret));
      }
    } else {
      $ret = array("status"=>400, "message"=>"Key Session Tidak Sah");
      die(json_encode($ret));
    }
  } else {
    $ret = array("status"=>400, "message"=>"Key Session Diperlukan");
    die(json_encode($ret));
  }
  }

  function redeem(){
    $redeemCategoryId = $this->input->post('redeemcategoryid');
    $headers = $this->input->request_headers();
    $key = $headers["key"];
    if($key){
      $where = array("key"=>$key);
    $session = $this->session_model->read_session($where);
    if($session->num_rows()>0){
      $where = array("id"=>$session->row()->userId);
      $user = $this->user_model->read_user($where);
      if($user->num_rows()>0){
        if($this->input->post('type')=="product"){
        //check redeem category
        $where = array("id"=>$redeemCategoryId);
        $redeemCategory = $this->redeemcategory_model->read_redeemcategory($where);
        if($redeemCategory->num_rows()>0){
          //check point 
          if($user->row()->point>=$redeemCategory->row()->pointRequired){
        $dataCreate = array("userId"=>$user->row()->id, "type"=>$this->input->post('type'),"status"=>"pending","redeemCategoryId"=>$redeemCategory->row()->id,"dateCreated"=>date("Y-m-d H:i:s"), "dateModified"=>date("Y-m-d H:i:s"));
        $flag = $this->redeem_model->create_redeem($dataCreate);
        if($flag){
          //minus point
          $dataUpdate = array("id"=>$user->row()->id,"point"=>$user->row()->point-$redeemCategory->row()->pointRequired, "dateModified"=>date("Y-m-d H:i:s"));
          $user_update = $this->user_model->update_user($dataUpdate);
          if($user_update){
            //create point log
            $dataCreate = array("userId"=>$user->row()->id, "type"=>"out","description"=>"Redeem","pointValue"=>$redeemCategory->row()->pointRequired,"dateCreated"=>date("Y-m-d H:i:s"),"dateModified"=>date("Y-m-d H:i:s"));
            $point_history = $this->point_model->create_point($dataCreate);
            if($point_history){
              
              $firebase = "";
              //send notif to member
              $data = array(
                "type"=>"redeem",
                "message"=>"Anda baru saja melakukan redeem sebesar ".$redeemCategory->row()->pointRequired,
                "token"=>$user->row()->firebase_token
              );
              $firebase = send_firebase_notification($data);
            $ret = array("status"=>200, "message"=>"Redeem Sukses", $data=>$firebase);
            die(json_encode($ret));
            } else {
              $ret = array("status"=>400, "message"=>"Redeem Gagal");
              die(json_encode($ret));
            }
          } else {
            $ret = array("status"=>400, "message"=>"Redeem Gagal");
            die(json_encode($ret));
          }
          } else {
            $ret = array("status"=>400, "message"=>"Redeem Gagal");
            die(json_encode($ret));
          }
        }  else {
          $ret = array("status"=>400, "message"=>"Insufficient Points");
          die(json_encode($ret));
        }
      } else {
        $ret = array("status"=>400, "message"=>"Invalid Redeem Category".$redeemCategoryId);
        die(json_encode($ret));
      }
    } else {
        //check point 
        if($user->row()->point>=$this->input->post("pointValue")) {
          $dataCreate = array("userId"=>$user->row()->id, "type"=>"cash","status"=>"pending","pointValue"=>$this->input->post("pointValue"),"redeemCategoryId"=>0,"dateCreated"=>date("Y-m-d H:i:s"), "dateModified"=>date("Y-m-d H:i:s"));
          $flag = $this->redeem_model->create_redeem($dataCreate);
          if($flag){
            //minus point
            $dataUpdate = array("id"=>$user->row()->id,"point"=>$user->row()->point-$this->input->post("pointValue"), "dateModified"=>date("Y-m-d H:i:s"));
            $user_update = $this->user_model->update_user($dataUpdate);
            if($user_update){
              //create point log
              $dataCreate = array("userId"=>$user->row()->id, "type"=>"out","description"=>"Redeem","pointValue"=>$this->input->post("pointValue"),"dateCreated"=>date("Y-m-d H:i:s"),"dateModified"=>date("Y-m-d H:i:s"));
              $point_history = $this->point_model->create_point($dataCreate);
              if($point_history){
                
                $firebase = "";
                //send notif to member
                $data = array(
                  "type"=>"redeem",
                  "message"=>"Anda baru saja melakukan redeem sebesar ".$this->input->post("pointValue"),
                  "token"=>$user->row()->firebase_token
                );
                $firebase = send_firebase_notification($data);
              $ret = array("status"=>200, "message"=>"Redeem Sukses");
              die(json_encode($ret));
              } else {
                $ret = array("status"=>400, "message"=>"Redeem Gagal");
                die(json_encode($ret));
              }
            } else {
              $ret = array("status"=>400, "message"=>"Redeem Gagal");
              die(json_encode($ret));
            }
            } else {
              $ret = array("status"=>400, "message"=>"Redeem Gagal");
              die(json_encode($ret));
            }
      }  else {
        $ret = array("status"=>400, "message"=>"Point tidak mencukupi");
        die(json_encode($ret));
      }
    }
    } else {
      $ret = array("status"=>400, "message"=>"User tidak dikenali");
      die(json_encode($ret));
    }
  } else {
    $ret = array("status"=>400, "message"=>"Key Session Tidak Sah");
    die(json_encode($ret));
  }
} else {
  $ret = array("status"=>400, "message"=>"Key Session Diperlukan");
  die(json_encode($ret));
}
  }

  function point_history(){
    $headers = $this->input->request_headers();
    $key = $headers["key"];
    if($key){
      $where = array("key"=>$key);
    $session = $this->session_model->read_session($where);
    if($session->num_rows()>0){
      $where = array("id"=>$session->row()->userId);
      $user = $this->user_model->read_user($where);
      if($user->num_rows()>0){
        $where = array("userId"=>$user->row()->id);
        $data_list = $this->point_model->read_point($where);
        if($data_list->num_rows()>0) $data_in = $data_list->result();
        else $data_in = "";
        $ret = array("status"=>200, "message"=>"Get Data Sukses", "data"=>$data_in);
        die(json_encode($ret));
      } else {
        $ret = array("status"=>400, "message"=>"Get Data Gagal");
        die(json_encode($ret));
      }
    } else {
      $ret = array("status"=>400, "message"=>"Get Data Gagal");
      die(json_encode($ret));
    }
  } else {
    $ret = array("status"=>400, "message"=>"Key Session Diperlukan");
    die(json_encode($ret));
  }
  }

  function trash_request_history($status=""){
    $headers = $this->input->request_headers();
    $key = $headers["key"];
    if($key){
      $where = array("key"=>$key);
    $session = $this->session_model->read_session($where);
    if($session->num_rows()>0){
      $where = array("id"=>$session->row()->userId);
      $user = $this->user_model->read_user($where);
      if($user->num_rows()>0){
        $where = ($user->row()->role=="mitra"?" AND A.mitraId ='".$user->row()->id."'":" AND A.userId='".$user->row()->id."'");
        $sql = "SELECT A.*, 
        B.name customer_name, 
        B.phoneNumber customer_phone, 
        C.name mitra_name, 
        C.jenis_kendaraan mitra_jenis_kendaraan, 
        C.no_plat_kendaraan mitra_no_plat_kendaraan ,
        D.name category_name,
        E.name unit_name
        FROM trashrequest AS A 
        JOIN user AS B on A.userId = B.id 
        LEFT JOIN user AS C on A.mitraId = C.id 
        LEFT JOIN trashcategory AS D on A.trashCategoryId = D.id
        LEFT JOIN unit AS E on D.unitId = E.id
        WHERE  A.status='".$status."' ".$where."
        GROUP BY A.id
        ";
        $trashrequest = $this->trashcategory_model->custom_sql($sql);
        if($trashrequest->num_rows()>0){
        $ret = array("status"=>200, "message"=>"Get Data Sukses", "data"=>$trashrequest->result());
        die(json_encode($ret));
      } else {
        $ret = array("status"=>200, "message"=>"Get Data Sukses", "data"=>"");
        die(json_encode($ret));
      }
      } else {
        $ret = array("status"=>400, "message"=>"User Not Found");
        die(json_encode($ret));
      }
    } else {
      $ret = array("status"=>400, "message"=>"Key Session Tidak Sah");
      die(json_encode($ret));
    }
  } else {
    $ret = array("status"=>400, "message"=>"Key Session Diperlukan");
    die(json_encode($ret));
  }
  }

  function get_trash_request(){
    $headers = $this->input->request_headers();
    $key = $headers["key"];
    if($key){
      $where = array("key"=>$key);
    $session = $this->session_model->read_session($where);
    if($session->num_rows()>0){
      $where = array("id"=>$session->row()->userId);
      $user = $this->user_model->read_user($where);
      if($user->num_rows()>0){
        $sql = "SELECT A.*, 
        B.name customer_name, 
        B.phoneNumber customer_phone, 
        C.name category_name,
        D.name unit
        FROM trashrequest AS A 
        JOIN user AS B on A.userId = B.id 
        JOIN trashcategory AS C ON A.trashCategoryId = C.id
        JOIN unit AS D On C.unitId = D.id
        WHERE A.mitraId IS NULL AND A.status='pending'
        GROUP BY A.id
        ORDER BY A.dateCreated
        ";
        $trashrequest = $this->trashrequest_model->custom_sql($sql);
        if($trashrequest->num_rows()>0){
        $ret = array("status"=>200, "message"=>"Get Data Sukses", "data"=>$trashrequest->result());
        die(json_encode($ret));
      } else {
        $ret = array("status"=>200, "message"=>"Get Data Sukses", "data"=>"");
        die(json_encode($ret));
      }
      } else {
        $ret = array("status"=>400, "message"=>"Get Data Gagal");
        die(json_encode($ret));
      }
    } else {
      $ret = array("status"=>400, "message"=>"Get Data Gagal");
      die(json_encode($ret));
    }
  } else {
    $ret = array("status"=>400, "message"=>"Key Session Diperlukan");
    die(json_encode($ret));
  }
  }

  function receive_trash_request(){
    $trashrequestid = $this->input->get('trashrequestid');
    $headers = $this->input->request_headers();
    $key = $headers["key"];
    if($key){
      $where = array("key"=>$key);
    $session = $this->session_model->read_session($where);
    if($session->num_rows()>0){
      $where = array("id"=>$session->row()->userId);
      $user = $this->user_model->read_user($where);
      if($user->num_rows()>0){
        if($user->row()->role=="mitra"){
          //pengecekan jika ada pickup yang belum selesai, tidak boleh ambil lagi (semasih ada yang belum selesai)
          $where = array("mitraId"=>$session->row()->userId,"status"=>"pending");
        $trashrequest = $this->trashrequest_model->read_trashrequest($where);
        $max_pickup = get_settings('max_pickup');
        if($trashrequest->num_rows()<=$max_pickup){
      $where = array("id"=>$trashrequestid);
      $trashrequest = $this->trashrequest_model->read_trashrequest($where);
      if($trashrequest->num_rows()>0){
        if($trashrequest->row()->mitraId==""){
      //create session
      $now = date("Y-m-d H:i:s");
      $userId = $trashrequest->row()->userId;
      $dataUpdate = array("id"=>$trashrequest->row()->id,"mitraId"=>$user->row()->id,"dateModified"=>$now);
      $trashrequest = $this->trashrequest_model->update_trashrequest($dataUpdate);
      if($trashrequest>0){
        
      $where = array("id"=>$userId);
      $user = $this->user_model->read_user($where);
      $firebase = "";
      if($user->num_rows()>0){
        
        $data = array(
          "type"=>"trash_request",
          "message"=>"Sampah Anda telah dipickup",
          "token"=>$user->row()->firebase_token
        );
        $firebase = send_firebase_notification($data);
      }
        //read session
        $ret = array("status"=>200, "message"=>"Receive Trash Request Sukses");
        die(json_encode($ret));
      } else {
        $ret = array("status"=>400, "message"=>"Receive Trash Request Gagal");
        die(json_encode($ret));
      }
    } else {
      $ret = array("status"=>400, "message"=>"Trash Request is already picked up");
      die(json_encode($ret));
    }
    } else {
      $ret = array("status"=>400, "message"=>"Invalid Trash Request ID");
      die(json_encode($ret));
    }
  } else {
    $ret = array("status"=>400, "message"=>"There's still ".$max_pickup."uncompleted request, please completed it first!");
    die(json_encode($ret));
  }
  } else {
    $ret = array("status"=>400, "message"=>"You are not allowed to receive the request");
    die(json_encode($ret));
  }
  } else {
    $ret = array("status"=>400, "message"=>"User tidak dikenali");
    die(json_encode($ret));
  }
} else {
  $ret = array("status"=>400, "message"=>"Key Session Tidak Sah");
  die(json_encode($ret));
}
} else {
  $ret = array("status"=>400, "message"=>"Key Session Diperlukan");
  die(json_encode($ret));
}
  }

  function signature_trash_request(){
    $trashrequestid = $this->input->get('trashrequestid');
    $headers = $this->input->request_headers();
    $key = $headers["key"];
    if($key){
      $where = array("key"=>$key);
    $session = $this->session_model->read_session($where);
    if($session->num_rows()>0){
      $where = array("id"=>$session->row()->userId);
      $user = $this->user_model->read_user($where);
      if($user->num_rows()>0){
        if($user->row()->role=="mitra"){
      $where = array("id"=>$trashrequestid, "status"=>"pending");
      $trashrequest = $this->trashrequest_model->read_trashrequest($where);
      if($trashrequest->num_rows()>0){
        if($trashrequest->row()->signature==""){
      //create session
      $now = date("Y-m-d H:i:s");
      $picture         = upload_file("signature", "images", 'assets/img/upload/');
      $dataUpdate = array("id"=>$trashrequest->row()->id,"dateModified"=>$now);
      if($picture!="empty" && $picture!="error" && $picture!="error_extension" && $picture!="error_upload") $dataUpdate += array('signature'=>$picture);
      $trashrequest = $this->trashrequest_model->update_trashrequest($dataUpdate);
      if($trashrequest>0){
        
        //send notif to customer
        $data = array(
          "type"=>"trash_request",
          "message"=>"Anda baru saja menandatangani request Anda",
          "token"=>$user->row()->firebase_token
        );
        send_firebase_notification($data);
        //read session
        $ret = array("status"=>200, "message"=>"Receive Trash Request Sukses");
        die(json_encode($ret));
      } else {
        $ret = array("status"=>400, "message"=>"Receive Trash Request Gagal");
        die(json_encode($ret));
      }
    } else {
      $ret = array("status"=>400, "message"=>"Trash Request is already picked up");
      die(json_encode($ret));
    }
    } else {
      $ret = array("status"=>400, "message"=>"Receive Trash Request Gagal");
      die(json_encode($ret));
    }
  } else {
    $ret = array("status"=>400, "message"=>"User tidak dikenali");
    die(json_encode($ret));
  }
} else {
  $ret = array("status"=>400, "message"=>"Key Session Tidak Sah");
  die(json_encode($ret));
}
} else {
  $ret = array("status"=>400, "message"=>"Key Session Diperlukan");
  die(json_encode($ret));
}
  }
}

  function update_profile(){
    $headers = $this->input->request_headers();
    $key = $headers["key"];
    if($key){
      $where = array("key"=>$key);
    $session = $this->session_model->read_session($where);
    if($session->num_rows()>0){
      $where = array("id"=>$session->row()->userId);
      $user = $this->user_model->read_user($where);
      if($user->num_rows()>0){
        $dataUpdate = array(
          "id"=>$user->row()->id,
          "name"=>$this->input->post("name"),
         "dateOfBirth"=>date('Y-m-d', strtotime($this->input->post('dateOfBirth'))),
         "gender"=>$this->input->post("gender"),
         "birthPlace"=>$this->input->post("birthPlace"),
         "phoneNumber"=>$this->input->post("phoneNumber"),
         "provinceId"=>$this->input->post("provinceId"),
         "regencyId"=>$this->input->post("regencyId"),
         "districtId"=>$this->input->post("districtId"),
         "address"=>$this->input->post("address"),
         "dateModified"=>date('Y-m-d H:i:s')
        );
        $userUpdate = $this->user_model->update_user($dataUpdate);
        $where = array("id"=>$session->row()->userId);
        $user = $this->user_model->read_user($where);
        if($user->num_rows()>0){
        $ret = array("status"=>200, "message"=>"Update Data Sukses", "data"=>$user->row());
        die(json_encode($ret));
        } else {
          $ret = array("status"=>400, "message"=>"Update Data Gagal");
          die(json_encode($ret));
        }
      } else {
        $ret = array("status"=>400, "message"=>"Update Data Gagal");
        die(json_encode($ret));
      }
    } else {
      $ret = array("status"=>400, "message"=>"Key Session Tidak Sah");
      die(json_encode($ret));
    }
  } else {
    $ret = array("status"=>400, "message"=>"Key Session Diperlukan");
    die(json_encode($ret));
  }
  }

  function change_password(){
    $headers = $this->input->request_headers();
    $key = $headers["key"];
    if($key){
      $where = array("key"=>$key);
    $session = $this->session_model->read_session($where);
    if($session->num_rows()>0){
      $where = array("id"=>$session->row()->userId);
      $user = $this->user_model->read_user($where);
      if($user->num_rows()>0){
        $where = array("id"=>$session->row()->userId, "password"=>hash('md5', $this->input->post("old_password")));
        $user = $this->user_model->read_user($where);
        if($user->num_rows()>0){
        $dataUpdate = array(
          "id"=>$user->row()->id,
          "password"=>hash('md5', $this->input->post("new_password")),
         "dateModified"=>date('Y-m-d H:i:s')
        );
        $userUpdate = $this->user_model->update_user($dataUpdate);
        if($userUpdate>0){
        $ret = array("status"=>200, "message"=>"Update Data Sukses", "data"=>$user->row());
        die(json_encode($ret));
        } else {
          $ret = array("status"=>400, "message"=>"Update Data Gagal");
          die(json_encode($ret));
        }
      } else {
        $ret = array("status"=>400, "message"=>"Old Password Doesn't match");
        die(json_encode($ret));
      }
      } else {
        $ret = array("status"=>400, "message"=>"Update Data Gagal");
        die(json_encode($ret));
      }
    } else {
      $ret = array("status"=>400, "message"=>"Key Session Tidak Sah");
      die(json_encode($ret));
    }
  } else {
    $ret = array("status"=>400, "message"=>"Key Session Diperlukan");
    die(json_encode($ret));
  }
  }

  function cancel_request(){
    $trashrequestid = $this->input->get('trashrequestid');
    $headers = $this->input->request_headers();
    $key = $headers["key"];
    if($key){
      $where = array("key"=>$key);
      $session = $this->session_model->read_session($where);
      if($session->num_rows()>0){
        $where = array("id"=>$session->row()->userId);
        $user = $this->user_model->read_user($where);
        if($user->num_rows()>0){
          $where = array("id"=>$trashrequestid);
          $trashrequest = $this->trashrequest_model->read_trashrequest($where);
          if($trashrequest->num_rows()>0){
            if($trashrequest->row()->status=="pending" && $trashrequest->row()->signature==""){
              $token = get_user_firebase_token($trashrequest->row()->mitraId);
              if($token!=false){
                $firebase = array();
                //send notif to mitra
                $data = array(
                  "type"=>"trash_request",
                  "message"=>"Mohon maaf, ".$user->row()->name." baru saja membatalkan requestnya",
                  "token"=>$token
                );
                send_firebase_notification($data);
              }
              //send notif to customer
              $data = array(
                "type"=>"trash_request",
                "message"=>"Anda baru saja membatalkan request",
                "token"=>$user->row()->firebase_token
              );
              send_firebase_notification($data);
              $dataUpdate = array("id"=>$trashrequest->row()->id, "status"=>"canceled");
              $flag = $this->trashrequest_model->update_trashrequest($dataUpdate);
              if($flag){
                $ret = array("status"=>200, "message"=>"Pembatalan Sukses");
                die(json_encode($ret));
              } else {
                $ret = array("status"=>400, "message"=>"Pembatalan Gagal");
                die(json_encode($ret));
              }
            } else {
              $ret = array("status"=>400, "message"=>"Kamu tidak dapat membatalkan");
              die(json_encode($ret));
            }
          } else {
            $ret = array("status"=>400, "message"=>"ID Tidak Sah");
            die(json_encode($ret));
          }
        }
      } else {
        $ret = array("status"=>400, "message"=>"Key Session Tidak Sah");
        die(json_encode($ret));}
    } else {
      $ret = array("status"=>400, "message"=>"Key Session Diperlukan");
      die(json_encode($ret));}
  }

  function forgot_password(){
      $where = array("email"=>$this->input->post("email"));
      $user = $this->user_model->read_user($where);
      if($user->num_rows()>0){
        $new_password = $this->random_alphanumeric();
        $dataUpdate = array(
          "id"=>$user->row()->id,
          "password"=>hash('md5', $new_password),
         "dateModified"=>date('Y-m-d H:i:s')
        );
        $userUpdate = $this->user_model->update_user($dataUpdate);
        if($userUpdate>0){
          $data_email = array("name"=>$user->row()->name, "email"=>$user->row()->email);
          $flag = $this->mailer->send_email_forgot_password($data_email, $new_password);
        $ret = array("status"=>200, "message"=>"Ubah Kata Sandi Sukses", "data"=>$user->row());
        die(json_encode($ret));
        } else {
          $ret = array("status"=>400, "message"=>"Ubah Kata Sandi Gagal");
          die(json_encode($ret));
        }
      } else {
        $ret = array("status"=>400, "message"=>"Invalid Email");
        die(json_encode($ret));
      }
  }

  function random_alphanumeric(){
    return substr(md5(uniqid(rand(), true)), 0,5);
  }

function firebase_notification(){
  $json_data = array(
    "to" => $this->input->post("token"),
    "notification" => array(
        "body" => $this->input->post("body"),
        "title" => $this->input->post("title")
        // "icon" => "ic_launcher"
  )
    // "data" => [
    //     "ANYTHING EXTRA HERE"
    // ]
    );
  $data = json_encode($json_data);
  //FCM API end-point
  $url = 'https://fcm.googleapis.com/fcm/send';
  //api_key in Firebase Console -> Project Settings -> CLOUD MESSAGING -> Server key
  $server_key = 'AAAAmnGYHeY:APA91bFAlhq1CbzKfhI_H8h4oc1Pux1BRRbGLEo_u7Wfmnj5auPerFQ_V8Mwuh4sTrdC2KoqXXLp3DnSX1586O3dipWnI6HMdk5mdFk9C0sgEOYD9REpcPqhAotLBuEUJgegwEoUCw1s';
  //header with content_type api key
  $headers = array(
      'Content-Type:application/json',
      'Authorization:key='.$server_key
  );
  //CURL request to route notification to FCM connection server (provided by Google)
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_POST, true);
  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
  $result = curl_exec($ch);
  if ($result === FALSE) {
      die('Oops! FCM Send Error: ' . curl_error($ch));
  }
  curl_close($ch);
}

}
?>