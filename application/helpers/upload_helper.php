<?php
if ( !function_exists('upload_file') ){
    function upload_file($input_name, $file_type, $destination){
        $file = $_FILES[$input_name];
        $filename = explode(".", $file["name"]);
		$file_name = $file['name'];
        $file_extension = $filename[count($filename)-1];
        $pic_ext = array('jpg','png','jpeg', 'bmp', 'ico');
        $doc_ext = array('doc', 'docx', 'pdf');
        $spd_ext = array('xls', 'xslx', 'csv');
        $ok_ext  = "";
        if($file_type=="images") $ok_ext = $pic_ext;
        if($file_type=="document") $ok_ext = $doc_ext;
        if($file_type=="spreadsheet") $ok_ext = $spd_ext;
        if ($file['tmp_name']!='') {
		// If there is no error
			if( $file['error'] == 0 ){
				// check if the extension is accepted
				if( in_array(strtolower($file_extension), $ok_ext)){
					// check if the size is not beyond expected size
					// rename the file
					$fileNewName = str_replace(" ","_",strtolower(uniqid())).'.'.$file_extension ;
					// and move it to the destination folder
					if( move_uploaded_file($file['tmp_name'], $destination.$fileNewName) ){
						$foto_path = $destination.$fileNewName;
                        return $foto_path;
					} else return 'error_upload';
					
				} else return 'error_extension';
					
			} else return 'error';
		} else return 'empty';		
    }
}
?>