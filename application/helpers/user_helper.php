<?php
if ( !function_exists('upload_file') ){
  function upload_file($input_name, $file_type, $destination){
      if(!empty($_FILES[$input_name])){
      $file = $_FILES[$input_name];
      $filename = explode(".", $file["name"]);
  $file_name = $file['name'];
      $file_extension = $filename[count($filename)-1];
      $pic_ext = array('jpg','png','jpeg', 'bmp');
      $doc_ext = array('doc', 'docx', 'pdf');
      $spd_ext = array('xls', 'xslx', 'csv');
      $ok_ext  = "";
      if($file_type=="images") $ok_ext = $pic_ext;
      if($file_type=="document") $ok_ext = $doc_ext;
      if($file_type=="spreadsheet") $ok_ext = $spd_ext;
      if ($file['tmp_name']!='') {
  // If there is no error
    if( $file['error'] == 0 ){
      // check if the extension is accepted
      if( in_array(strtolower($file_extension), $ok_ext)){
        // check if the size is not beyond expected size
        // rename the file
        $fileNewName = str_replace(" ","_",strtolower(uniqid())).'.'.$file_extension ;
        // and move it to the destination folder
        if( move_uploaded_file($file['tmp_name'], $destination.$fileNewName) ){
          $foto_path = $destination.$fileNewName;
                      return $foto_path;
        } else return 'error_upload';
        
      } else return 'error_extension';
        
    } else return 'error';
  } else return 'empty';		
  } else return 'empty';
  }
}

if ( !function_exists('auth_redirect') ){
  function auth_redirect($redirect=""){
    $CI =& get_instance();
      $user_login = $CI->session->userdata('user_login');
    if($redirect!="") $CI->session->set_userdata('redirect', $redirect);
    if(empty($user_login)) redirect('login');
    else return true;
  }
}

if ( !function_exists('auth_redirect_admin') ){
  function auth_redirect_admin(){
    $CI =& get_instance();
    if(empty($CI->session->userdata('admin_login'))) redirect(base_url('login'));
    else return true;
  }
}

if ( !function_exists('get_settings') ){
  function get_settings($name){
    $CI =& get_instance();
    $setting = $CI->setting_model->read_setting($name);
    if($setting->num_rows()>0)
    return $setting->row()->value;
    else return false;
  }
}

if ( !function_exists('save_settings') ){
  function save_settings($name, $value){
    $CI =& get_instance();
    $setting = $CI->setting_model->update_setting($name, $value);
    return $setting;
  }
}



if ( !function_exists('get_all_trash_request_det') ){
  function get_all_trash_request_det($where=""){
    $CI =& get_instance();
    $sql = "SELECT A.*, B.name category_name, B.point_value category_point_value , C.name unit_name
    FROM trashrequestdet AS A
    JOIN trashcategory AS B ON B.id = A.trashCategoryId
    JOIN unit AS C ON C.id = B.unitId
    ";
    if($where) $sql.=$where;
    $result = $CI->trashrequestdet_model->custom_sql($sql);
    if($result->num_rows()>0)
    return $result;
    else return false;
  }
}

if ( !function_exists('get_all_point') ){
  function get_all_point($where=""){
    $CI =& get_instance();
    $sql = "SELECT A.*, B.name user_name
    FROM point AS A
    LEFT JOIN user AS B ON B.id = A.userId
    ";
    if($where) $sql.=$where;
    $result = $CI->point_model->custom_sql($sql);
    if($result->num_rows()>0)
    return $result;
    else return false;
  }
}



if ( !function_exists('get_all_trash_request') ){
  function get_all_trash_request(){
    $CI =& get_instance();
    $sql = "SELECT A.*, B.name customer_name, B.phoneNumber customer_phone, B.address customer_address, B.email customer_email, C.name mitra_name, C.email mitra_email, C.phoneNumber mitra_phone, C.address mitra_address,
    D.name category_name
    FROM trashrequest AS A
    JOIN user AS B ON B.id = A.userId
    LEFT JOIN user AS C ON C.id = A.mitraId
    LEFT JOIN trashcategory as D ON A.trashCategoryId = D.id
    ";
    $result = $CI->trashrequest_model->custom_sql($sql);
    if($result->num_rows()>0)
    return $result;
    else return false;
  }
}




if ( !function_exists('is_admin') ){
  function is_admin(){
    $CI =& get_instance();
    $user_login = $CI->session->userdata('user_login');
    if(!empty($user_login)){
      $where = array(
        'email'=> $CI->session->userdata('user_login')
      );
      $data_user = $CI->user_model->read_user($where);
      if($data_user->num_rows()!=0){
        if($data_user->row()->type=='admin')
        return 1;
        else return 0;
      } else return false;
    } else return false;
  }
}

if ( !function_exists('get_current_member') ){
  function get_current_member(){
    $CI =& get_instance();
    if(!empty($CI->session->userdata('user_login'))){
      $where = array(
        'email'=> $CI->session->userdata('user_login')
      );
      $data_user = $CI->user_model->read_user($where);
      if($data_user->num_rows()!=0){
        return $data_user->row();
      } else return false;
    } else return false;
  }
}

if ( !function_exists('get_province_list') ){
  function get_province_list(){
    $CI =& get_instance();
    $data = $CI->provinces_model->read_provinces();
    if($data->num_rows()>0) return $data;
    else return false;
  }
}
if ( !function_exists('get_regency_list') ){
  function get_regency_list(){
    $CI =& get_instance();
    $data = $CI->regencies_model->read_regencies();
    if($data->num_rows()>0) return $data;
    else return false;
  }
}
if ( !function_exists('get_district_list') ){
  function get_district_list(){
    $CI =& get_instance();
    $data = $CI->districts_model->read_districts();
    if($data->num_rows()>0) return $data;
    else return false;
  }
}


if ( !function_exists('get_all_unit_list') ){
  function get_all_unit_list(){
    $CI =& get_instance();
    $data = $CI->unit_model->read_unit();
    if($data->num_rows()>0) return $data;
    else return false;
  }
}



if ( !function_exists('get_all_pemasok_list') ){
  function get_all_pemasok_list(){
    $CI =& get_instance();
      $data = $CI->pemasok_model->read_pemasok();
      if($data->num_rows()!=0){
        return $data;
      } else return false;
  }
}

if ( !function_exists('get_all_pembelian_produk_list') ){
  function get_all_pembelian_produk_list(){
    $CI =& get_instance();
      $data = $CI->pembelian_produk_model->read_pembelian_produk_detail();
      if($data->num_rows()!=0){
        return $data;
      } else return false;
  }
}

if ( !function_exists('get_all_produk_list') ){
  function get_all_produk_list(){
    $CI =& get_instance();
      $data = $CI->penerimaan_produk_model->read_produk();
      if($data->num_rows()!=0){
        return $data;
      } else return false;
  }
}

if ( !function_exists('get_pelanggan') ){
  function get_pelanggan($id){
    $CI =& get_instance();
    $where = array('pelanggan.id_pelanggan'=>$id);
    $data = $CI->pelanggan_model->read_pelanggan($where);
      if($data->num_rows()!=0){
        return $data->row();
      } else return false;
  }
}

if ( !function_exists('get_all_nota_list') ){
  function get_all_nota_list(){
    $CI =& get_instance();
      $data = $CI->nota_model->read_nota();
      if($data->num_rows()!=0){
        return $data;
      } else return false;
  }
}

if ( !function_exists('get_user_firebase_token') ){
  function get_user_firebase_token($id){
    $CI =& get_instance();
    $where = array("id"=>$id);
      $data = $CI->user_model->read_user($where);
      if($data->num_rows()!=0){
        return $data->row()->firebase_token;
      } else return false;
  }
}

if ( !function_exists('get_single_user') ){
  function get_single_user($id){
    $CI =& get_instance();
    $where = array("id"=>$id);
      $data = $CI->user_model->read_user($where);
      if($data->num_rows()!=0){
        return $data->row();
      } else return false;
  }
}


if ( !function_exists('send_email') ){
  function send_email($to, $subject, $body){
    $CI =& get_instance();
    $CI->load->library('email');
    $result = $CI->email
    ->from('admin@i-solusi.com')
    ->reply_to('admin@i-solusi.com')    // Optional, an account where a human being reads.
    ->to($to)
    ->subject($subject)
    ->message($body)
    ->send();
    return $result;
  }
}

if ( !function_exists('send_email_verification') ){
  function send_email_verification($data, $verification_url){
    $CI =& get_instance();
    $html_down         = get_settings('verification');
    $html_down         = str_replace("{{appName}}",             COMPANY_NAME, $html_down);
    $html_down         = str_replace("{{year}}",     			date('Y'), $html_down);
    $html_down         = str_replace("{{user_name}}",             $data['name'], $html_down);
    $html_down         = str_replace("{{user_email}}",          $data['email'], $html_down);
    $html_down         = str_replace("{{confirmation_url}}",            $verification_url, $html_down);

    $send               = send_email($data['email'], 'Account Verification', $html_down);

    return $send;
  }
}

if ( !function_exists('get_iso_wallet') ){
  function get_iso_wallet(){
    $CI =& get_instance();
    $sql = "SELECT SUM(pointValue) total_iso_wallet FROM point WHERE userId=0 AND type='in'";
      $data = $CI->point_model->custom_sql($sql);
      if($data->num_rows()!=0){
        return $data->row()->total_iso_wallet;
      } else return false;
  }
}

if ( !function_exists('send_firebase_notification') ){
  function send_firebase_notification($data){
    $CI =& get_instance();
    $url = "https://fcm.googleapis.com/fcm/send";
    $message = array("type"=>$data["type"],"message"=>$data["message"]);
    $fields = array(
      "to"  => $data["token"],
      "data"=> $message,
      "time_to_live"=> 30,
      "delay_while_idle"=>true
    );
    $auth_key = "AAAAmnGYHeY:APA91bFAlhq1CbzKfhI_H8h4oc1Pux1BRRbGLEo_u7Wfmnj5auPerFQ_V8Mwuh4sTrdC2KoqXXLp3DnSX1586O3dipWnI6HMdk5mdFk9C0sgEOYD9REpcPqhAotLBuEUJgegwEoUCw1s";
    $headers = array(
      "Authorization: key=".$auth_key,
      "Content-Type: application/json"
    );
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
    $result = curl_exec($ch);
    // return $message;
  }
}


if ( !function_exists('get_all_redeem_list') ){
  function get_all_redeem_list(){
    $CI =& get_instance();
    $where = "SELECT A.*, B.name user_name, B.point user_point, C.title, C.pointRequired
    FROM redeem AS A
    JOIN user AS B ON B.id= A.userId
    LEFT JOIN redeemcategory AS C ON C.id = A.redeemCategoryId

    ";
      $data = $CI->redeem_model->custom_sql($where);
      if($data->num_rows()!=0){
        return $data;
      } else return false;
  }
}

if ( !function_exists('get_all_pekerja_list') ){
  function get_all_pekerja_list(){
    $CI =& get_instance();
      $data = $CI->pekerja_model->read_pekerja();
      if($data->num_rows()!=0){
        return $data;
      } else return false;
  }
}

if ( !function_exists('get_all_penjualan_list') ){
  function get_all_penjualan_list(){
    $CI =& get_instance();
      $data = $CI->penjualan_model->read_penjualan();
      if($data->num_rows()!=0){
        return $data;
      } else return false;
  }
}

if ( !function_exists('get_all_pembelian_list') ){
  function get_all_pembelian_list(){
    $CI =& get_instance();
      $data = $CI->pembelian_model->read_pembelian();
      if($data->num_rows()!=0){
        return $data;
      } else return false;
  }
}

if ( !function_exists('get_all_pelanggan_list') ){
  function get_all_pelanggan_list(){
    $CI =& get_instance();
      $data = $CI->pelanggan_model->read_pelanggan();
      if($data->num_rows()!=0){
        return $data;
      } else return false;
  }
}


if ( !function_exists('get_all_user_list') ){
  function get_all_user_list(){
    $CI =& get_instance();
    $sql = "SELECT A.*, 
    B.name province_name, 
    C.name regency_name, 
    D.name district_name

    FROM user AS A
    LEFT JOIN provinces as B ON B.id = A.provinceId
    LEFT JOIN regencies as C ON C.id = A.regencyId
    LEFT JOIN districts as D ON D.id = A.districtId";
      $data_user = $CI->user_model->custom_sql($sql);
      if($data_user->num_rows()!=0){
        return $data_user;
      } else return false;
  }
}

if ( !function_exists('get_all_nota_list') ){
  function get_all_nota_list(){
    $CI =& get_instance();
      $data = $CI->nota_model->read_nota();
      if($data->num_rows()!=0){
        return $data;
      } else return false;
  }
}

if ( !function_exists('get_all_pekerja_list') ){
  function get_all_pekerja_list($site_code){
    $CI =& get_instance();
    $data_customer = $CI->pekerja_model->read_pekerja($where);
    if($data_customer->num_rows()!=0){
        return $data_customer;
      } else return false;
  }
}

if ( !function_exists('get_all_produksi') ){
  function get_all_produksi(){
    $CI =& get_instance();
      $data_produksi = $CI->produksi_model->read_produksi();
      if($data_produksi->num_rows()!=0){
        return $data_produksi;
      } else return false;
  }
}

if ( !function_exists('money') ){
  function money($number){
    return "Rp.".number_format($number,0,",",".");
  }
}

if ( !function_exists('generate_unique') ){
  function generate_unique(){
    return rand(1,9).rand(1,9).rand(1,9);
  }
}

if( !function_exists('hari')){
  function hari($hari){
    $harinya = "";
    switch ($hari) {
        case 'Sun':
        $harinya = "Minggu";
        break;
        case 'Mon':
        $harinya = "Senin";
        break;
        case 'Tue':
        $harinya = "Selasa";
        break;
        case 'Wed':
        $harinya = "Rabu";
        break;
        case 'Thu':
        $harinya = "Kamis";
        break;
        case 'Fri':
        $harinya = "Jumat";
        break;
        case 'Sat':
        $harinya = "Sabtu";
        break;
    }
    return $harinya;
  }
}

if( !function_exists('bulan')){
  function bulan($bulan){
    $bulannya = "";
    switch ($bulan) {
        case 'Jan':
        $bulannya = "Januari";
        break;
        case 'Feb':
        $bulannya = "Februari";
        break;
        case 'Mar':
        $bulannya = "Maret";
        break;
        case 'Apr':
        $bulannya = "April";
        break;
        case 'May':
        $bulannya = "Mei";
        break;
        case 'Jun':
        $bulannya = "Juni";
        break;
        case 'Jul':
        $bulannya = "Juli";
        break;
        case 'Aug':
        $bulannya = "Agustus";
        break;
        case 'Sep':
        $bulannya = "September";
        break;
        case 'Oct':
        $bulannya = "Oktober";
        break;
        case 'Nov':
        $bulannya = "November";
        break;
        case 'Dec':
        $bulannya = "Desember";
        break;
    }
    return $bulannya;
  }
}
if ( !function_exists('get_firstname') ){
  function get_firstname($name){
      $name_explode  = explode(' ', $name);
    return $name_explode[0];
  }
}

if ( !function_exists('clean_content') ){
  function clean_content($content){
      return strip_tags($content, '<a><b><br><div><em><i><li><table><td><tr><span><sub><sup><strong><u><ul>');
  }
}

if ( !function_exists('view_short') ){
  function view_short($title){
      return substr($title,0,50);
  }
}

if ( !function_exists('phone_number') ){
  function phone_number($number){
      $first = substr($number, 0,1);
      if($first=="0") return "62".substr($number, 1);
      if($first=="+") return substr($number, 1);
      else return $number;
  }
}
if ( !function_exists('get_statistic') ){
  function get_statistic(){
    $CI =& get_instance();
      $sql = "SELECT SUM(penjualan_detail.qty) jumlah, pembelian_produk.nama_produk nama_produk FROM penjualan
      JOIN penjualan_detail ON penjualan_detail.id_penjualan = penjualan.id_penjualan
      JOIN penerimaan_produk ON penerimaan_produk.id_produk=penjualan_detail.id_produk
      JOIN pembelian ON pembelian.id_pembelian = penerimaan_produk.id_pembelian
      JOIN pembelian_produk ON pembelian_produk.id_pembelian_produk=pembelian.id_pembelian_produk
      GROUP BY penjualan_detail.id_produk desc
      ";
      $data = $CI->penjualan_detail_model->custom_sql($sql);
      if($data->num_rows()!=0){
        return $data;
      } else return false;
  }
}

if ( !function_exists('get_transaksi') ){
  function get_transaksi(){
    $CI =& get_instance();
      $sql = "SELECT transaksi.*, pembelian_produk.nama_produk FROM transaksi
      JOIN pembelian_produk ON pembelian_produk.id_pembelian_produk = transaksi.id_pembelian_produk
      ";
      $data = $CI->transaksi_model->custom_sql($sql);
      if($data->num_rows()!=0){
        return $data;
      } else return false;
  }
}

if ( !function_exists('get_statistic_daily') ){
  function get_statistic_daily(){
    $CI =& get_instance();
      $sql = "SELECT SUM(total_keseluruhan) jumlah, date(datecreated) date FROM penjualan
      GROUP BY DATE(datecreated)
      ";
      $data = $CI->penjualan_detail_model->custom_sql($sql);
      if($data->num_rows()!=0){
        return $data;
      } else return false;
  }
}

if ( !function_exists('get_profit_by_date') ){
  function get_profit_by_date($date){
    $CI =& get_instance();
      $sql = "SELECT IFNULL(SUM(total_keseluruhan),0) jumlah, date(datecreated) date FROM penjualan
      WHERE DATE(datecreated)= '".$date."'
      ";
      $data = $CI->penjualan_detail_model->custom_sql($sql);
      if($data->num_rows()!=0){
        return $data->row()->jumlah;
      } else return 0;
  }
}

if ( !function_exists('get_all_news_list') ){
  function get_all_news_list(){
    $CI =& get_instance();
      $data = $CI->news_model->read_news();
      if($data->num_rows()!=0){
        return $data;
      } else return false;
  }
}

if ( !function_exists('get_all_faq_list') ){
  function get_all_faq_list(){
    $CI =& get_instance();
      $data = $CI->faq_model->read_faq();
      if($data->num_rows()!=0){
        return $data;
      } else return false;
  }
}

if ( !function_exists('get_all_unit_list') ){
  function get_all_unit_list(){
    $CI =& get_instance();
      $data = $CI->unit_model->read_unit();
      if($data->num_rows()!=0){
        return $data;
      } else return false;
  }
}

if ( !function_exists('get_all_trashcategory_list') ){
  function get_all_trashcategory_list(){
    $CI =& get_instance();
    $where = "SELECT A.*, B.name unit_name
    FROM trashcategory AS A
    JOIN unit AS B ON B.id= A.unitId
    ";
      $data = $CI->trashcategory_model->custom_sql($where);
      if($data->num_rows()!=0){
        return $data;
      } else return false;
  }
}

if ( !function_exists('get_all_redeemcategory_list') ){
  function get_all_redeemcategory_list(){
    $CI =& get_instance();
      $data = $CI->redeemcategory_model->read_redeemcategory();
      if($data->num_rows()!=0){
        return $data;
      } else return false;
  }
}

?>
