<section class="content list-content">
    <div class="row">
  <div class="col-md-12 pos-con">
    <div class="head-title">
      <h2><span class="fa fa-list"style="padding-right:10px"></span> Pembelian produk</h2>
      <hr>
    </div>
      <?php if(!empty($this->session->userdata('message'))) echo $this->session->userdata('message');?>
    <div class="col-md-12 datatble-content">
      <div class="clearfix">
      <div class="tabbable-panel margin-tops4  datatble-content">
        <div class="tabbable-line">
          <ul class="nav nav-tabs tabtop  tabsetting" class="align=center">
         <li> <a id="barang_list_menu" href="#barang_list" class="active" data-toggle="tab">Pembelian Barang List</a> </li>
         <li> <a id="add_barang_menu" href="#add_barang" data-toggle="tab"> Add Pembelian Barang</a> </li>
          </ul>
          <div class="tab-content margin-tops">
          <!--Tab1-->
            <div class="tab-pane active fade in" id="barang_list">
      <div class="content-datatable table-responsive">
        <table id="example" class="table table-striped table-bordered" style="width:100%">
          <thead>
            <tr class="title-datable">
              <th>NO</th>
              <th>Pemasok</th>
              <th>Nama</th>
              <th>Jenis</th>
              <th>Satuan</th>
              <th>Harga</th>
              <th>Harga Eceran</th>
              <th>Harga Borongan</th>
              <th>Stock</th>
              <th>Min Stock</th>
<!--               <th>Edit</th> -->
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
              <?php
              $data_list = get_all_pembelian_produk_list();
              if($data_list!=false){
                  $num=0;
                  foreach($data_list->result() as $data){
                      $num++;
                      ?>
            <tr>
              <td><?php echo $num;?></td>
              <td><?php echo $data->nama_pemasok." (".$data->nama_perusahaan.')';?></td>
              <td><?php echo $data->nama_produk;?></td>
              <td><?php echo $data->jenis_produk;?></td>
              <td><?php echo $data->satuan_produk;?></td>
              <td><?php echo money($data->harga_produk);?></td>
              <td><?php echo money($data->harga_eceran);?></td>
              <td><?php echo money($data->harga_borongan);?></td>
              <td><?php echo $data->stok_produk;?></td>
              <td><?php echo $data->min_stok;?></td>
              <td>
                <?php if($data->status_active_pembelian_produk=="active") { ?>
               <p data-placement="top" data-toggle="tooltip" title="Non Aktif">
                 <a class="btn btn-danger btn-xs"  href="<?php echo base_url('backend/nonactive_pembelian_produk/'.$data->id_pembelian_produk);?>"><span class="fa fa-ban"></span></a>
               </p>
               <?php } else { ?>
               <p data-placement="top" data-toggle="tooltip" title="Aktif">
                 <a class="btn btn-success btn-xs" href="<?php echo base_url('backend/active_pembelian_produk/'.$data->id_pembelian_produk);?>"><span class="fa fa-check"></span></a>
               </p>
               <?php } ?>
              </td>
            </tr>
              <?php }} ?>
          </tbody>
        </table>
      </div>
    </div>
    <div class="tab-pane fade in" id="add_barang">
      <form class="login100-form validate-form" id="pembelian_produk_form" method="post" action="<?php echo base_url('backend/create_pembelian_produk');?>" enctype="multipart/form-data">
          <input name="id" type="hidden" value="">
          <div class="form-group">
            <label>Pemasok<span style="color:#f00">*</span></label>
            <select class="form-control" name="pemasok" required>
              <option value="">Pilih Pemasok</option>
              <?php
              $list = get_all_pemasok_list();
              if($list!=false){
                foreach($list->result() as $row){
                  echo '
                  <option value="'.$row->id_pemasok.'">'.$row->nama_pemasok." (".$row->nama_perusahaan.')</option>
                  ';
                }
              }
              ?>
            </select>
          </div>
          <div class="form-group">
            <label for="nama_barang">Name Barang<span style="color:#f00">*</span></label>
            <input type="text" class="form-control" id="nama_produk" name="nama_produk" placeholder="Ketik Nama Barang" maxlength="100"  value="" required>
          </div>
          <div class="form-group">
            <label for="jenis_barang">Jenis Barang<span style="color:#f00">*</span></label>
            <input type="text" class="form-control" id="jenis_produk" name="jenis_produk" placeholder="Ketik Jenis Barang" maxlength="100"  value="" required>
          </div>
          <div class="form-group">
            <label for="satuan_produk">Satuan Barang<span style="color:#f00">*</span></label>
            <input type="text" class="form-control" id="satuan_produk" name="satuan_produk" placeholder="Ketik Satuan Barang" maxlength="100"  value="" required>
          </div>
          <div class="form-group">
            <label for="harga_barang">Harga Barang (Modal)<span style="color:#f00">*</span></label>
            <input type="number" class="form-control" id="harga_produk" name="harga_produk" placeholder="Ketik Harga Barang" value="" min="1" required>
          </div>
        <div class="form-group">
            <label for="harga_barang">Harga Borongan<span style="color:#f00">*</span></label>
            <input type="number" class="form-control" id="harga_borongan" name="harga_borongan" placeholder="Ketik Harga Borongan" value="" min="1" required>
          <span class="label label-danger" id="harga_borongan_warn">Harga tidak boleh kurang dari harga modal</span>
          </div>
          <div class="form-group">
            <label for="harga_barang">Harga Eceran<span style="color:#f00">*</span></label>
            <input type="number" class="form-control" id="harga_eceran" name="harga_eceran" placeholder="Ketik Harga Eceran" value="" min="1" required>
            <span class="label label-danger" id="harga_eceran_warn">Harga tidak boleh kurang dari harga borongan</span>
          </div>
          <div class="form-group">
            <label for="min_stock">Min Stok<span style="color:#f00">*</span></label>
            <input type="number" class="form-control" id="min_stok" name="min_stok" placeholder="Ketik Minimal Stock" value="" min="1" required>
          </div>
          <div class="footer-form">
              <button type="submit" class="btn btn-success">Simpan</button>
          </div>
      </form>
    </div>
  </div>
</div>
</div>
</div>
    </div>
  </div>
    </div>
</section>
<div id="delete_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Delete Data</h4>  
        </div> 
        <div class="modal-body">
          Aoakah anda yakin untuk menghapus data ini
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
          <a  class="btn btn-danger" id="delete_footer" href="#">Ya</a>
        </div>
      </div>
    </div>
  </div>
<style>
  .table-striped>tbody>tr:nth-of-type(odd) {
    background:#d2d2d2;
  }
</style>
<script>
$(document).ready(function(){
  $("#harga_eceran_warn").hide();
  $("#harga_borongan_warn").hide();
});
  $("#pembelian_produk_form").submit(function(e){
    var harga_modal = parseInt($("#harga_produk").val());
  var harga_eceran = parseInt($("#harga_eceran").val());
  var harga_borongan = parseInt($("#harga_borongan").val());
  if(harga_eceran<harga_borongan){
    $("#harga_eceran").focus();
    $("#harga_eceran_warn").show();
    e.preventDefault();
  }
  else if(harga_eceran>=harga_borongan){
    $("#harga_eceran_warn").hide();
  }
    var harga_modal = parseInt($("#harga_produk").val());
  var harga_borongan = parseInt($("#harga_borongan").val());
  if(harga_borongan<harga_modal){
    $("#harga_borongan").focus();
    $("#harga_borongan_warn").show();
    e.preventDefault();
  } else if(harga_borongan>=harga_modal){
    $("#harga_borongan_warn").hide();
    return true;
  }
  });
  
</script>
