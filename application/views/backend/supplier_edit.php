<section class="content list-content">
    <div class="row">
    <div class="col-md-12 pos-con">
        <div class="head-title">
            <h2><span class="fa fa-pencil" style="padding-right:10px"></span> Edit Pemasok</h2>
            <hr>
        </div>
        <a href="<?php echo base_url('supplier');?>" class="btn btn-primary"><span class="fa fa-arrow-left"></span> Kembali</a>
        <div class="col-md-12 datatble-content">
            <form class="login100-form validate-form" method="post" id="edit_supplier_form" action="<?php echo base_url('backend/supplier_edit_process');?>" enctype="multipart/form-data">
                            <input name="id" type="hidden" value="<?php echo $data_edit->id_supplier;?>">
                            <div class="form-group">
                              <label>Nama Perusahaan<span style="color:#f00">*</span></label>
                              <input type="text" class="form-control" id="title" name="nama_perusahaan" aria-describedby="emailHelp" placeholder="Ketik nama perusahaan" maxlength="150"  value="<?php echo $data_edit->nama_perusahaan;?>" required>
                            </div>
                            <div class="form-group">
                              <label>Nama<span style="color:#f00">*</span></label>
                              <input type="text" class="form-control" id="title" name="nama_supplier" aria-describedby="emailHelp" placeholder="Ketik name" maxlength="150"  value="<?php echo $data_edit->nama_supplier;?>" required>
                            </div>
                            <div class="form-group">
                              <label for="no_telepon">Alamat<span style="color:#f00">*</span></label>
                              <input type="text" class="form-control" id="alamat" name="alamat_supplier" aria-describedby="emailHelp" placeholder="Ketik Alamat" maxlength="150" value="<?php echo $data_edit->alamat_supplier;?>" required>
                            </div>
                            <div>
                            <div class="form-group">
                              <label for="no_telepon">Email<span style="color:#f00">*</span></label>
                              <input type="email" class="form-control" id="email" name="email_supplier" aria-describedby="emailHelp" placeholder="Ketik Email" maxlength="150" value="<?php echo $data_edit->email_supplier;?>" required>
                              <span class="label label-danger" id="email_warn">Mohon hanya mengisi dengan email .com atau .co.id</span>
                            </div>
                              <label for="telp">No. Telepon<span style="color:#f00">*</span></label>
                              <input type="text" class="form-control" id="telp" name="telp_supplier" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" aria-describedby="emailHelp" placeholder="Ketik No Telepon" maxlength="13"  value="<?php echo $data_edit->telp_supplier;?>" required>
                            </div>
                            <div class="form-group">
                                <label>Status<span style="color:#f00">*</span></label>
                                <select class="form-control" name="status" id="status"  required>
                                    <option value="">Pilih Status</option>
                                    <option value="active">Active</option>
                                    <option value="nonactive">Nonactive</option>
                                  </select>
                              </div>
                            <script>
                                $(document).ready(function(){
                                    $("#status").val('<?php echo $data_edit->status;?>').change();
                                });
                            </script>
                            <div class="footer-form">
                              <br>
                                <button type="submit" class="btn btn-success">Simpan</button>
                            </div>
            </form>

        </div>
    </div>
    </div>
</section>
<script>
    $(document).ready(function(){
    $("#email_warn").hide();
  })
  $("#email").keyup(function(){
  var domain_co_id = $(this).val().substr($(this).length - 6); 
  var domain_com = $(this).val().substr($(this).length - 4); 
//   console.log(domain_com)
  if(domain_co_id=="co.id"){
    $("#email_warn").hide();
  } else if(domain_com=="com"){
    $("#email_warn").hide();
  } else {
    $("#email_warn").show();
  }
})
  
  $("#edit_supplier_form").submit(function(e){
    var domain_co_id = $("#email").val().substr($("#email").length - 6); 
    var domain_com = $("#email").val().substr($("#email").length - 4); 
//     console.log(domain_com)
    if(domain_co_id=="co.id"){
      $("#email_warn").hide();
    } else if(domain_com=="com"){
      $("#email_warn").hide();
    } else {
      e.preventDefault();
      $("#email").focus();
      $("#email_warn").show();
    }
  })
</script>