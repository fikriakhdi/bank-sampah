
<?php 
$nilai_point = get_settings('nilai_point');
?>
<!-- Main content -->
<section class="content">
    <div class="row">
  <div class="col-md-12">
    <?php
  if($this->session->flashdata('message')=='success_checkout') {
    echo '
    <div class="alert alert-success">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <strong>Sukses!</strong> Pemesanan produk berhasil!.
    </div>
    ';
  }
  ?>
  </div>
  <div class="header-title">
    <h1>Selamat Datang Di
      <?php echo COMPANY_NAME;?>
    </h1>
  </div>

  <div class="col-md-12 top-con">
    <div class="col-sm-12">
      <h4>
        Hari Ini :
        <?php echo hari(date('D')).', '.date('d').' '.bulan(date('M')).' '.date('Y');?>
      </h4>
    </div>

    <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-gift"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Total Redeem</span>
              <span class="info-box-number"><?php echo (get_all_redeem_list()!=false?get_all_redeem_list()->num_rows():0);?><small></small></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-trash"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Total Request</span>
              <span class="info-box-number"><?php echo (get_all_trash_request()!=false?get_all_trash_request()->num_rows():0);?><small></small></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow">Rp</span>

            <div class="info-box-content">
              <span class="info-box-text">ISO Wallet</span>
              <span class="info-box-number">Rp<?php echo (get_iso_wallet()?number_format(get_iso_wallet()*$nilai_point,2):0);?><small></small></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>

  </div>

  <div class="col-sm-12">
    <div id="chartContainer" style="height: 300px; width: 100%;"></div>
  </div>


<header>
</header>
</div>
</section>
