<section class="content list-content">
    <div class="row">
    <div class="col-md-12 pos-con">
        <div class="head-title">
            <h2><span class="fa fa-pencil" style="padding-right:10px"></span> Edit Pelanggan</h2>
            <hr>
        </div>
        <a href="<?php echo base_url('pekerja');?>" class="btn btn-primary"><span class="fa fa-arrow-left"></span> Kembali</a>
        <div class="col-md-12 datatble-content">
            <form class="login100-form validate-form" method="post" action="<?php echo base_url('backend/pelanggan_edit_process');?>" enctype="multipart/form-data">
                            <input name="id" type="hidden" value="<?php echo $data_edit->id_pelanggan;?>">
                            <div class="form-group">
                              <label for="name">Nama<span style="color:#f00">*</span></label>
                              <input type="text" class="form-control" id="title" name="nama_pelanggan" aria-describedby="emailHelp" placeholder="Type name" maxlength="150"   value="<?php echo $data_edit->nama_pelanggan;?>" required>
                            </div>
                            <div class="form-group">
                              <label for="telp">No. Telepon<span style="color:#f00">*</span></label>
                              <input type="text" class="form-control" id="telp" name="telp_pelanggan" aria-describedby="emailHelp" placeholder="Type No Telepon" maxlength="150"   value="<?php echo $data_edit->telp_pelanggan;?>" required>
                            </div>
                            <div class="form-group">
                              <label for="no_telepon">Alamat<span style="color:#f00">*</span></label>
                              <input type="text" class="form-control" id="alamat" name="alamat_pelanggan" aria-describedby="emailHelp" placeholder="Type Alamat" maxlength="150"   value="<?php echo $data_edit->alamat_pelanggan;?>" required>
                            </div>
                            <div class="footer-form">
                                <button type="submit" class="btn btn-success">Simpan</button>
                            </div>
            </form>

        </div>
    </div>
    </div>
</section>
