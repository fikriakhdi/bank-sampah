<section class="content list-content">
    <div class="row">
    <div class="col-md-12 pos-con">
        <div class="head-title">
            <h2><span class="fa fa-pencil" style="padding-right:10px"></span> Edit Redeem Category</h2>
            <hr>
        </div>
        <a href="<?php echo base_url('redeemcategory');?>" class="btn btn-primary"><span class="fa fa-arrow-left"></span> Kembali</a>
        <div class="col-md-12 datatble-content">
            <form class="login100-form validate-form" method="post" action="<?php echo base_url('backend/redeemcategory_edit_process');?>" enctype="multipart/form-data">
                    <input name="id" type="hidden" value="<?php echo $data_edit->id;?>">
                      <div class="form-group">
                        <label>Title<span style="color:#f00">*</span></label>
                        <input type="text" class="form-control" name="title" id="title" maxlength="150"  value="<?php echo $data_edit->title;?>" required>
                      </div>
                      <div class="form-group">
                        <label>Description<span style="color:#f00">*</span></label>
                        <textarea class="form-control" name="description" id="description" rows="3" value=""><?php echo $data_edit->description;?></textarea>
                      </div>
                      <div class="form-group">
                        <label>Price<span style="color:#f00">*</span></label>
                        <input type="number" class="form-control" name="price" id="price" maxlength="150"  value="<?php echo $data_edit->price;?>" required>
                      </div>
                      <div class="form-group">
                        <label>Point Required<span style="color:#f00">*</span></label>
                        <input type="text" class="form-control" name="pointRequired" id="pointRequired" step="0.01"  value="<?php echo $data_edit->pointRequired;?>" required>
                      </div>
                      <div class="form-group">
                        <label>Image<span style="color:#f00">*</span></label>
                        <input type="file" class="form-control" name="file" id="images" >
                      </div>
                      <div class="footer-form">
                          <button type="submit" class="btn btn-success">Simpan</button>
                      </div>
            </form>

        </div>
    </div>
    </div>
</section>
