<section class="content list-content">
<div class="row">
  <div class="col-md-12 pos-con">
    <div class="head-title">
      <h2><span class="fa fa-user"style="padding-right:10px"></span> User Manager</h2>
      <hr>
    </div>
      <?php if(!empty($this->session->userdata('message'))) echo $this->session->userdata('message');?>
    <div class="col-md-12">
      <div class="clearfix">
      <div class="tabbable-panel margin-tops4  datatble-content">
        <div class="tabbable-line">
          <ul class="nav nav-tabs tabtop  tabsetting" class="align=center">
         <li> <a id="member_list_menu" href="#member_list" class="active" data-toggle="tab">User List</a> </li>
         <li> <a id="add_member_menu" href="#add_member" data-toggle="tab"> Add User</a> </li>
          </ul>
          <div class="tab-content margin-tops">
          <!--Tab1-->
            <div class="tab-pane active fade in" id="member_list">
      <div class="content-datatable table-responsive">
        <table id="example" class="table table-striped table-bordered" style="width:100%">
          <thead>
            <tr class="title-datable">
              <th>NO</th>
              <th>Nama</th>
              <th>Email</th>
              <th>Alamat</th>
              <th>Privinsi</th>
              <th>Kota</th>
              <th>No.Hp</th>
              <th>Role</th>
              <th>Status</th>
              <th>Point</th>
              <th>Tanggal Daftar</th>
              <th>Edit</th>
              <th>Delete</th>
            </tr>
          </thead>
          <tbody>
              <?php
              $user_list = get_all_user_list();
              if($user_list!=false){
                  $num=0;
                  foreach($user_list->result() as $user_data){
                    if($user_data->id!=$member->id){
                      $num++;
                      ?>
            <tr>
              <td><?php echo $num;?></td>
              <td><?php echo $user_data->name;?></td>
              <td><?php echo $user_data->email;?></td>
              <td><?php echo $user_data->address;?></td>
              <td><?php echo $user_data->province_name;?></td>
              <td><?php echo $user_data->regency_name;?></td>
              <td><?php echo $user_data->phoneNumber;?></td>
              <td><?php echo strtoupper($user_data->role);?></td>
              <td><?php echo strtoupper($user_data->status);?></td>
              <td><?php echo $user_data->point;?></td>
              <td><?php echo date('d-m-Y', strtotime($user_data->dateCreated));?></td>
                <td><p data-placement="top" data-toggle="tooltip" title="Edit"><a href="<?php echo base_url('user_edit/'.$user_data->id);?>" class="btn btn-primary btn-xs" data-title="Edit"  ><span class="glyphicon glyphicon-pencil"></span></a></p></td>
                <td><p data-placement="top" data-toggle="tooltip" title="Hapus"><button class="btn btn-danger btn-xs delete_btn" data-toggle="modal" data-target="#delete_modal" id_url="<?php echo base_url('backend/delete_user/'.$user_data->id);?>" id_data="<?php echo $user_data->id;?>"><span class="glyphicon glyphicon-trash"></span></button></p></td>
            </tr>
              <?php }}} ?>
          </tbody>
        </table>
      </div>
    </div>
    <div class="tab-pane fade in" id="add_member">
      <form class="login100-form validate-form" method="post" action="<?php echo base_url('backend/create_user');?>" id="create_user_form" enctype="multipart/form-data" novalidate>
          <input name="id" type="hidden" value="">
          <div  class="form-group">
            <label>Role<span style="color:#f00">*</span></label>
            <select class="form-control" name="role" id="role" required>
              <option>Pilih Role</option>
              <option value="member">Member</option>
              <option value="mitra">Mitra</option>
              <option value="admin">Admin</option>
            </select>
          </div>
          <div class="form-group">
            <label>Nama<span style="color:#f00">*</span></label>
            <input type="text" class="form-control" name="name" aria-describedby="emailHelp" placeholder="Name" maxlength="150"  value="" required>
          </div>
          <div class="form-group">
            <label>Email<span style="color:#f00">*</span></label>
            <input type="email" class="form-control" name="email" id="email" aria-describedby="emailHelp" placeholder="Ketik Email" maxlength="100"  value="" required>
          </div>
          <div class="form-group">
            <label>Alamat<span style="color:#f00">*</span></label>
            <input type="text" class="form-control" name="alamat" id="alamat" aria-describedby="emailHelp" placeholder="Ketik Alamat" maxlength="100"  value="" required>
          </div>
          <div class="form-group ">
            <label>Provinsi<span style="color:#f00">*</span></label>
            <select class="form-control" name="provinceId" id="provinceId" data-url="<?php echo base_url('api/regencies');?>" required>
              <option>Pilih Provinsi</option>
              <?php
              $data_list = get_province_list();
              if($data_list!=false){
                  $num=0;
                  foreach($data_list->result() as $data){
                    echo '<option value="'.$data->id.'">'.$data->name.'</option>';
                  }
                }
              ?>
            </select>
          </div>
          
          <div class="form-group ">
            <label>Kota/Kabupaten<span style="color:#f00">*</span></label>
            <select class="form-control" name="regencyId" id="regencyId" data-url="<?php echo base_url('api/districts');?>" required>
              <option>Pilih Kota/Kabupaten</option>
            </select>
          </div>


          <!-- <div class="form-group ">
            <label>Kecamatan<span style="color:#f00">*</span></label>
            <select class="form-control" name="districtId" id="districtId" required>
              <option>Pilih Kecamatan</option>
            </select>
          </div> -->
          <div  class="form-group">
            <label>Phone Number<span style="color:#f00">*</span></label>
            <input type="text" class="form-control" name="phoneNumber" placeholder="Ketik No Telp" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" required>
          </div>
          <div  class="form-group " id="no_ktp_group">
            <label>No. KTP<span style="color:#f00">*</span></label>
            <input type="text" class="form-control" name="ktp" id="no_ktp" value="" onkeyup="this.value=this.value.replace(/[^\d]/,'')">
            <!-- <input type="text" class="form-control" name="phoneNumber" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" > -->
          </div>
          <div  class="form-group " id="no_sim_group">
            <label>No. SIM<span style="color:#f00">*</span></label>
            <input type="text" class="form-control" name="sim" id="no_sim" value="" onkeyup="this.value=this.value.replace(/[^\d]/,'')">
            <!-- <input type="text" class="form-control" name="phoneNumber" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" > -->
          </div>
          <div  class="form-group" id="masa_berlaku_sim_group">
            <label>Masa Berlaku SIM<span style="color:#f00">*</span></label>
            <input type="text" class="form-control datepicker" id="no_ktp" name="masa_berlaku_sim" value="">
            <!-- <input type="text" class="form-control" name="phoneNumber" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" > -->
          </div>
          <div  class="form-group " id="no_plat_kendaraan_group">
            <label>No. Plat Kendaran<span style="color:#f00">*</span></label>
            <input type="text" class="form-control " name="no_plat_kendaraan" id="no_plat_kendaraan" value="">
            <!-- <input type="text" class="form-control" name="phoneNumber" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" > -->
          </div>
          <div class="form-group " id="tipe_kendaraan_group">
            <label>Tipe Kendaraan<span style="color:#f00">*</span></label>
            <select class="form-control" name="tipe_kendaraan" id="tipe_kendaraan" value="">
              <option>Pilih Tipe kendaraan</option>
              <option value="motor">Motor</option>
              <option value="mobil">Mobil</option>
              <option value="truk">Truk</option>
              <option value="difabel">Kendaraan Difabel</option>
            </select>
          </div>
          <div class="form-group">
            <label>Status<span style="color:#f00">*</span></label>
            <select class="form-control" name="status" aria-describedby="emailHelp"  value="" required>
                <option value="">Pilih Status</option>
                <option value="active">Active</option>
                <option value="nonactive">Nonactive</option>
              </select>
          </div>
          <div class="form-group">
            <label>Password<span style="color:#f00">*</span></label>
            <input type="password" class="form-control" name="password" id="password" aria-describedby="emailHelp" placeholder="Ketik Password" value="" required>
          </div>
          <div class="form-group">
            <label>Ketik Ulang Password<span style="color:#f00">*</span></label>
            <input type="password" class="form-control"name="retype_password" id="retype_password" aria-describedby="emailHelp" placeholder="ReKetik Password" value="" required>
              <span class="label label-warning" id="password_warning">Password doesn't match</span>
          </div>
          <div class="footer-form">
              <button type="submit" class="btn btn-success">Simpan</button>
          </div>
      </form>
    </div>
  </div>
</div>
</div>
</div>
    </div>
  </div>
    </div>
</section>
<div id="delete_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Delete Data</h4>
        </div>
        <div class="modal-body">
          Aoakah anda yakin untuk menghapus data ini
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
          <a  class="btn btn-danger" id="delete_footer" href="#">Ya</a>
        </div>
      </div>
    </div>
  </div>
<style>
  .table-striped>tbody>tr:nth-of-type(odd) {
    background:#d2d2d2;
  }
</style>