<section class="content list-content">
    <div class="row">
  <div class="col-md-12 pos-con">
    <div class="head-title">
      <h2><span class="fa fa-usd"style="padding-right:10px"></span> Penjualan Detail</h2>
      <hr>
    </div>
      <?php if(!empty($this->session->userdata('message'))) echo $this->session->userdata('message');?>
    <div class="col-md-12 datatble-content">
      <a href="<?php echo base_url('penjualan');?>" class="btn btn-primary"><span class="fa fa-arrow-left"></span> Back</a>
      <div class="clearfix">
      <div class="tabbable-panel margin-tops4  datatble-content">
      <div class="content-datatable table-responsive">
        <table id="example" class="table table-striped table-bordered" style="width:100%">
          <thead>
            <tr class="title-datable">
              <th>NO</th>
              <th>Nama Produk</th>
              <th>Harga Produk</th>
              <th>Harga Jual Produk</th>
              <th>Qty Produk</th>
              <th>Total Harga</th>
            </tr>
          </thead>
          <tbody>
              <?php
              if($penjualan_detail->num_rows()!=0){
                  $num=0;
                  foreach($penjualan_detail->result() as $data){
                      $num++;
                      ?>
            <tr>
              <td><?php echo $num;?></td>
              <td><?php echo $data->nama_produk;?></td>
              <td><?php echo money($data->harga_produk);?></td>
              <td><?php echo money($data->harga_jual);?></td>
              <td><?php echo $data->qty;?></td>
              <td><?php echo money($data->total_harga);?></td>
            </tr>
              <?php }} ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
</div>
</div>
</section>
<div id="delete_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Delete Data</h4>
        </div>
        <div class="modal-body">
          Aoakah anda yakin untuk menghapus data ini
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
          <a  class="btn btn-danger" id="delete_footer" href="#">Ya</a>
        </div>
      </div>
    </div>
  </div>
<style>
  .table-striped>tbody>tr:nth-of-type(odd) {
    background:#d2d2d2;
  }
</style>
