<html>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="shortcut icon" type="image/png" href="<?php echo ASSETS;?>img/Favicon-02.png"/>
<title><?php echo $title;?></title>
<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- Bootstrap 3.3.7 -->
<link rel="stylesheet" href="<?php echo base_url('assets/');?>bower_components/bootstrap/dist/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet" href="<?php echo base_url('assets/');?>bower_components/font-awesome/css/font-awesome.min.css">
<body>
  <div class="container">
    <div class="row">
      <div class="col-md-2">
        <img src="<?php echo base_url('assets/img/');?>kecil_logo_banget.jpeg" style="max-width:200px;">
      </div>
      <div class="col-md-10" style="text-align:right">
        <h3>Toko Besi "Anyar Jaya"</h3>
        <h4>Jual Bahan2 Bangunan</h4>
        <h5>Kayu, Besi, Keramik, Kaca, Bata, Pasir-Batu, Genteng, Pipa, Ledeng, DLL.<br>
          Jl.Raya Anyar No.6 Telp (0254) 601062 Fax. (0254) 601063 <br>
          HP. 087871611333 - 082114786890 - Anyar</h5>
      </div>
      <div class="col-md-12">
        <hr>
        <div class="col-md-8 col-md-offset-4">
          <p><label>Anyar, </label> <?php echo date('d-m-yy', strtotime($tanggal_penjualan));?></p>
          <p><label>Tuan/Toko </label> <?php echo $nama_pelanggan;?><br>
            <label><?php echo $alamat_pelanggan;?><label>
            </label>
              </label>
          </p>
        </div>
      </div>
      <div class="col-md-12">
        NOTA No. <?php echo $id_nota;?>
      </div>
      <div class="col-md-12">
      <table class="table table-striped table-bordered" style="width:100%">
        <thead>
          <tr class="title-datable">
            <th>BANYAKNYA</th>
            <th>NAMA BARANG</th>
            <th>HARGA JUAL</th>
            <th>JUMLAH</th>
          </tr>
        </thead>
        <tbody>
            <?php
            if($nota->num_rows()!=0){
                $num=0;
                foreach($nota->result() as $data){
                    $num++;
                    ?>
          <tr>
            <td><?php echo $data->qty;?></td>
            <td><?php echo $data->nama_produk;?></td>
            <td><?php echo money($data->harga_jual);?></td>
            <td><?php echo money($data->total_harga);?></td>
          </tr>
            <?php }} ?>
        </tbody>
      </table>
      </div>
      <div class="col-md-12">
        <div class="col-md-2">
          Tanda Terima
        </div>
        <div class="col-md-2">
          Perhatian!<br>
          Barang2 yang sudah dibeli tidak dapat ditukar/dikembalikan
        </div>
        <div class="col-md-8">
          <p>TOTAL KESELURUHAN : <b><?php echo money($total_keseluruhan);?></b></p>
          <p>Hormat Kami</p>
        </div>
      </div>
    </div>
  </div>
</body>
<style>
p{
  text-align: right;
}
</style>
<script>
window.print();
</script>
</html>
