<section class="content list-content">
  <div class="row">
    <div class="col-md-12 pos-con">
      <div class="head-title">
        <h2><span class="fa fa-pencil" style="padding-right:10px"></span> Edit Unit</h2>
        <hr>
      </div>
      <a href="<?php echo base_url('unit');?>" class="btn btn-primary"><span class="fa fa-arrow-left"></span> Kembali</a>
      <div class="col-md-12 datatble-content">
        <form class="login100-form validate-form" id="edit_unit_form" method="post" action="<?php echo base_url('backend/unit_edit_process');?>" enctype="multipart/form-data" novalidate>
          <input name="id" type="hidden" value="<?php echo $data_edit->id;?>">

          <div class="form-group">
            <label>Nama<span style="color:#f00">*</span></label>
            <input type="text" class="form-control" name="name" aria-describedby="emailHelp"  maxlength="150" value="<?php echo $data_edit->name;?>">
          </div>
          <div class="footer-form">
            <button type="submit" class="btn btn-success">Simpan</button>
          </div>
        </form>

      </div>
    </div>
  </div>
</section>