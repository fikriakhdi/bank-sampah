<section class="content list-content">
    <div class="row">
    <div class="col-md-12 pos-con">
        <div class="head-title">
            <h2><span class="fa fa-pencil" style="padding-right:10px"></span> Edit Trash Category</h2>
            <hr>
        </div>
        <a href="<?php echo base_url('trashcategory');?>" class="btn btn-primary"><span class="fa fa-arrow-left"></span> Kembali</a>
        <div class="col-md-12 datatble-content">
            <form class="login100-form validate-form" method="post" action="<?php echo base_url('backend/trashcategory_edit_process');?>" enctype="multipart/form-data">
                    <input name="id" type="hidden" value="<?php echo $data_edit->id;?>">
                      <div class="form-group">
                        <label>Name<span style="color:#f00">*</span></label>
                        <input type="text" class="form-control" name="name" id="name" value="<?php echo $data_edit->name;?>">
                      </div>
                      <div class="form-group">
                        <label>Unit<span style="color:#f00">*</span></label>
                        <select type="text" class="form-control" name="unit" id="unit" required>
                        <option value="">Select Unit</option>
                        <?php 
                        $data_list = get_all_unit_list();
                        if($data_list!=false){
                          foreach($data_list->result() as $row){
                            echo '<option value="'.$row->id.'">'.strtoupper($row->name).'</option>';
                          }
                        }
                        ?>
                        </select>
                      </div>
                      <script>
                      $("#unit").val("<?php echo $data_edit->unitId;?>").change();
                      </script>
                      <div class="form-group">
                        <label>Point Value<span style="color:#f00">*</span></label>
                        <input type="number" class="form-control" name="point_value" id="point_value" step="0.01" value="<?php echo $data_edit->point_value;?>" required>
                      </div>
                      <div class="form-group">
                          <label>Minimum Berat<span style="color:#f00">*</span></label>
                          <input name="min_weight" type="number" class="form-control" value="<?php echo $data_edit->min_weight;?>" min="1" required>
                      </div>
                      <div class="footer-form">
                          <button type="submit" class="btn btn-success">Simpan</button>
                      </div>
            </form>

        </div>
    </div>
    </div>
</section>
