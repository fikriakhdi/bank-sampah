<section class="content list-content">
    <div class="row">
  <div class="col-md-12 pos-con">
    <div class="head-title">
      <h2><span class="fa fa-bar-chart"style="padding-right:10px"></span> Point Report</h2>
      <hr>
    </div>
      <?php if(!empty($this->session->userdata('message'))) echo $this->session->userdata('message');?>
    
        <div class="col-md-12 datatble-content">
      <div class="clearfix">
      <div class="content-datatable table-responsive">
      
      <table id="example" class="table table-striped table-bordered" style="width:100%">
          <thead>
            <tr class="title-datable">
              <th>NO</th>
              <th>Point Owner</th>
              <th>Points</th>
              <th>Type</th>
              <th>Date</th>
            </tr>
          </thead>
          <tbody>
              <?php
              $data_list = get_all_point();
              if($data_list!=false){
                  $num=0;
                  foreach($data_list->result() as $data){
                      $num++;
                      ?>
            <tr>
              <td><?php echo $num;?></td>
              <td><?php echo ($data->user_name?$data->user_name:"ISO Wallet");?></td>
              <td><?php echo $data->pointValue;?></td>
              <td><?php echo ($data->type=="in"?'<span class="label label-success">IN</span>':'<span class="label label-danger">OUT</span>');?></td>
              <td><?php echo date('Y-m-d H:i:s', strtotime($data->dateCreated));?></td>
        </tr>
              <?php }} ?>
          </tbody>
        </table>
      </div>
</div>
    </div>
  </div>
    </div>
</section>

<style>
  .table-striped>tbody>tr:nth-of-type(odd) {
    background:#d2d2d2;
  }
</style>