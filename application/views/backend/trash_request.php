<section class="content list-content">
    <div class="row">
  <div class="col-md-12 pos-con">
    <div class="head-title">
      <h2><span class="fa fa-trash"style="padding-right:10px"></span> Trash Request</h2>
      <hr>
    </div>
      <?php if(!empty($this->session->userdata('message'))) echo $this->session->userdata('message');?>
    <div class="col-md-12 datatble-content">
      <div class="clearfix">
      <div class="tabbable-panel margin-tops4  datatble-content">
        <div class="tabbable-line">
          <ul class="nav nav-tabs tabtop  tabsetting" class="align=center">
         <li> <a id="TrashRequest_list_menu" href="#TrashRequest_list" class="active" data-toggle="tab">Pending</a> </li>
         <li> <a id="TrasjRequestCompleted_menu" href="#TrasjRequestCompleted" data-toggle="tab">Completed </a> </li>
         <li> <a id="TrasjRequestCanceled_menu" href="#TrasjRequestCanceled" data-toggle="tab">Canceled </a> </li>
          </ul>
          <div class="tab-content margin-tops">
          <!--Tab1-->
            <div class="tab-pane active fade in" id="TrashRequest_list">
      <div class="content-datatable table-responsive">
        <table id="example" class="table table-striped table-bordered" style="width:100%">
          <thead>
            <tr class="title-datable">
              <th>NO</th>
              <th>Nama Customer</th>
              <th>Nama Mitra</th>
              <th>No Telp</th>
              <th>Email</th>
              <th>Alamat</th>
              <th>Lat Lon</th>
              <th>Status</th>
              <th>Tanggal</th>
              <th>TTD</th>
              <th>Berat</th>
              <th>Kategori</th>
              <th>Konfirmasi</th>
              <!-- <th>Delete</th> -->
            </tr>
          </thead>
          <tbody>
              <?php
              $data_list = get_all_trash_request();
              if($data_list!=false){
                  $num=0;
                  foreach($data_list->result() as $data){
                      $num++;
                      if($data->status=="pending"){
                      ?>
            <tr>
              <td><?php echo $num;?></td>
              <td><?php echo $data->customer_name;?></td>
              <td><?php echo ($data->mitra_name?$data->mitra_name:"Belum Ada");?></td>
              <td><?php echo $data->customer_phone;?></td>
              <td><?php echo $data->customer_email;?></td>
              <td><?php echo $data->customer_address;?></td>
              <td><?php echo "lat:".$data->lat."<br>"."lon:".$data->lon;?></td>
              <td><?php echo strtoupper($data->status);?></td>
              <td><?php echo date('Y-m-d', strtotime($data->dateCreated));?></td>
              <td><?php echo ($data->signature!=""?"<img src='".base_url($data->signature)."' style='max-width:50px'>":"-");?></td>
              <td><?php echo strtoupper($data->weight);?></td>
              <td><?php echo strtoupper($data->category_name);?></td>
                <td>
                    <?php if($data->status=="pending" && $data->mitraId!=NULL && $data->signature!="") { ?> 
                    <p data-placement="top" data-toggle="tooltip" title="Konfirmasi"><button class="btn btn-success btn-xs confirm_btn" data-toggle="modal" data-target="#confirm_modal" id_url="<?php echo base_url('backend/confirm_trash_request/'.$data->id);?>"><span class="fa fa-check"></span></button></p>
                    <?php }  else { ?>
                        -
                    <?php } ?> 
                </td>
                <!-- <td><p data-placement="top" data-toggle="tooltip" title="Hapus"><button class="btn btn-danger btn-xs delete_btn" data-toggle="modal" data-target="#delete_modal" id_url="<?php echo base_url('backend/delete_trash_request/'.$data->id);?>"><span class="glyphicon glyphicon-trash"></span></button></p></td> -->
            </tr>
              <?php }}} ?>
          </tbody>
        </table>
      </div>
    </div>
    <div class="tab-pane  fade in" id="TrasjRequestCompleted">
      <div class="content-datatable table-responsive">
        <table id="example" class="table table-striped table-bordered" style="width:100%">
          <thead>
            <tr class="title-datable">
              <th>NO</th>
              <th>Nama Customer</th>
              <th>Nama Mitra</th>
              <th>No Telp</th>
              <th>Email</th>
              <th>Alamat</th>
              <th>Lat Lon</th>
              <th>Status</th>
              <th>Tanggal</th>
              <th>Ttd</th>
              <th>Berat</th>
              <th>Kategori</th>
              <th>Konfirmasi</th>
              <!-- <th>Delete</th> -->
            </tr>
          </thead>
          <tbody>
              <?php
              $data_list = get_all_trash_request();
              if($data_list!=false){
                  $num=0;
                  foreach($data_list->result() as $data){
                      $num++;
                      if($data->status=="completed"){
                      ?>
            <tr>
              <td><?php echo $num;?></td>
              <td><?php echo $data->customer_name;?></td>
              <td><?php echo ($data->mitra_name?$data->mitra_name:"Belum Ada");?></td>
              <td><?php echo $data->customer_phone;?></td>
              <td><?php echo $data->customer_email;?></td>
              <td><?php echo $data->customer_address;?></td>
              <td><?php echo "lat:".$data->lat."<br>"."lon:".$data->lon;?></td>
              <td><?php echo strtoupper($data->status);?></td>
              <td><?php echo date('Y-m-d', strtotime($data->dateCreated));?></td>
              <td><?php echo ($data->signature!=""?"<img src='".base_url($data->signature)."' style='max-width:50px'>":"-");?></td>
              <td><?php echo strtoupper($data->weight);?></td>
              <td><?php echo strtoupper($data->category_name);?></td>
                <td>
                    <?php if($data->status=="pending" && $data->mitraId!=NULL) { ?> 
                    <p data-placement="top" data-toggle="tooltip" title="Konfirmasi"><button class="btn btn-success btn-xs confirm_btn" data-toggle="modal" data-target="#confirm_modal" id_url="<?php echo base_url('backend/confirm_trash_request/'.$data->id);?>"><span class="fa fa-check"></span></button></p>
                    <?php }  else { ?>
                        -
                    <?php } ?> 
                </td>
                <!-- <td><p data-placement="top" data-toggle="tooltip" title="Hapus"><button class="btn btn-danger btn-xs delete_btn" data-toggle="modal" data-target="#delete_modal" id_url="<?php echo base_url('backend/delete_trash_request/'.$data->id);?>"><span class="glyphicon glyphicon-trash"></span></button></p></td> -->
            </tr>
              <?php }} }?>
          </tbody>
        </table>
      </div>
    </div>
    <div class="tab-pane fade in" id="TrasjRequestCanceled">
    <div class="content-datatable table-responsive">
        <table id="example" class="table table-striped table-bordered" style="width:100%">
          <thead>
            <tr class="title-datable">
              <th>NO</th>
              <th>Nama Customer</th>
              <th>Nama Mitra</th>
              <th>No Telp</th>
              <th>Email</th>
              <th>Alamat</th>
              <th>Lat Lon</th>
              <th>Status</th>
              <th>Tanggal</th>
              <th>Ttd</th>
              <th>Berat</th>
              <th>Kategori</th>
            </tr>
          </thead>
          <tbody>
              <?php
              $data_list = get_all_trash_request();
              if($data_list!=false){
                  $num=0;
                  foreach($data_list->result() as $data){
                      if($data->status=="canceled"){
                      $num++;
                      ?>
            <tr>
              <td><?php echo $num;?></td>
              <td><?php echo $data->customer_name;?></td>
              <td><?php echo ($data->mitra_name?$data->mitra_name:"Belum Ada");?></td>
              <td><?php echo $data->customer_phone;?></td>
              <td><?php echo $data->customer_email;?></td>
              <td><?php echo $data->customer_address;?></td>
              <td><?php echo "lat:".$data->lat."<br>"."lon:".$data->lon;?></td>
              <td><?php echo strtoupper($data->status);?></td>
              <td><?php echo date('Y-m-d', strtotime($data->dateCreated));?></td>
              <td><?php echo ($data->signature!=""?"<img src='".base_url($data->signature)."' style='max-width:50px'>":"-");?></td>
              <td><?php echo strtoupper($data->weight);?></td>
              <td><?php echo strtoupper($data->category_name);?></td>
            </tr>
              <?php }}} ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
</div>
</div>
    </div>
  </div>
    </div>
</section>
<div id="delete_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Delete Data</h4>
        </div>
        <div class="modal-body">
          Aoakah anda yakin untuk menghapus data ini?
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
          <a  class="btn btn-danger" id="delete_footer" href="#">Ya</a>
        </div>
      </div>
    </div>
  </div>
  <div id="confirm_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Confirm Data</h4>
        </div>
        <div class="modal-body">
          Aoakah anda yakin untuk mengkonfirmasi data ini?
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
          <a  class="btn btn-danger" id="confirm_footer" href="#">Ya</a>
        </div>
      </div>
    </div>
  </div>
<style>
  .table-striped>tbody>tr:nth-of-type(odd) {
    background:#d2d2d2;
  }
</style>