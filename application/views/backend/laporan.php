<section class="content list-content">
  <div class="row">
    <div class="col-md-12 pos-con">
      <div class="head-title">
        <h2><span class="fa fa-file" style="padding-right:10px"></span> Laporan</h2>
        <hr>
      </div>
      <?php if(!empty($this->session->userdata('message'))) echo $this->session->userdata('message');?>
      <div class="col-md-12 datatble-content">
        <div class="clearfix">
          <div class="tabbable-panel margin-tops4  datatble-content">
            <div class="tabbable-line">
              <ul class="nav nav-tabs tabtop  tabsetting" class="align=center">
                <li> <a href="#penerimaan_tab" class="active" data-toggle="tab">List</a> </li>
<!--                 <li> <a href="#pembelian_tab" data-toggle="tab"> Pembelian</a> </li>
                <li> <a href="#penjualan_tab" data-toggle="tab"> Penjualan</a> </li> -->
              </ul>
              <div class="tab-content margin-tops">
                <!--Tab1-->
                <div class="tab-pane active fade in" id="penerimaan_tab">
                  <div class="content-datatable table-responsive">
                    <table id="example" class="table table-striped table-bordered" style="width:100%">
                      <thead>
                        <tr class="title-datable">
                          <th>NO</th>
                          <th>Nama Produk</th>
                          <th>Tanggal</th>
                          <th>Kuantitas</th>
                          <th>Tipe</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
              $data_list = get_transaksi();
              if($data_list!=false){
                  $num=0;
                  foreach($data_list->result() as $data){
                      $num++;
                      ?>
                          <tr>
                            <td>
                              <?php echo $num;?>
                            </td>
                            <td>
                              <?php echo $data->nama_produk;?>
                            </td>
                            <td>
                              <?php echo date('m-d-Y', strtotime($data->date));?>
                            </td>
                            <td>
                              <?php echo $data->qty;?>
                            </td>
                            <td>
                              <?php echo $data->type;?>
                            </td>
                          </tr>
                          <?php }} ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<div id="delete_modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete Data</h4>
      </div>
      <div class="modal-body">
        Aoakah anda yakin untuk menghapus data ini
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <a class="btn btn-danger" id="delete_footer" href="#">Ya</a>
      </div>
    </div>
  </div>
</div>
<style>
  .table-striped>tbody>tr:nth-of-type(odd) {
    background: #d2d2d2;
  }
</style>

<script>
  $("#create_penjualan_form").submit(function(e) {
    var jumlah_produk = $("#jumlah_product").val();
    var filled = 0;
    for (var i = 1; i <= jumlah_produk; i++) {
      if ($("#qty_" + i).val() && $("#harga_jual_" + i).val()) filled++;
      console.log(jumlah_produk);
      var data_id = $("#qty_" + i).data('id');
      var stock = $("#qty_" + i).data('stock');
      console.log(stock)
      if ($("#qty_" + i).val() > stock) {
        $("#warn_box").text("Kuantitas tidak boleh melebihi stock");
        $("#warn_box").removeClass("hidden");
        $("#qty_" + i).focus();
        e.preventDefault();
      }
    }
    if (filled == 0) {
      e.preventDefault();
      $("#qty_1").focus();
      $("#warn_box").text("Mohon isi form dengan lengkap");
      $("#warn_box").removeClass("hidden");
    }
  })
</script>