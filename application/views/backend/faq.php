<section class="content list-content">
  <div class="row">
    <div class="col-md-12 pos-con">
      <div class="head-title">
        <h2><span class="fa fa-question"style="padding-right:10px"></span> FAQ Manager</h2>
        <hr>
      </div>
        <?php if(!empty($this->session->userdata('message'))) echo $this->session->userdata('message');?>
      <div class="col-md-12">
        <div class="clearfix">
          <div class="tabbable-panel margin-tops4  datatble-content">
            <div class="tabbable-line">
              <ul class="nav nav-tabs tabtop  tabsetting" class="align=center">
                <li> <a id="member_list_menu" href="#member_list" class="active" data-toggle="tab">FAQ List</a> </li>
                <li> <a id="add_member_menu" href="#add_member" data-toggle="tab"> Add FAQ</a> </li>
              </ul>
              <div class="tab-content margin-tops">
                <!--Tab1-->
                <div class="tab-pane active fade in" id="member_list">
                  <div class="content-datatable table-responsive">
                    <table id="example" class="table table-striped table-bordered" style="width:100%">
                      <thead>
                        <tr class="title-datable">
                          <th>NO</th>
                          <th>Question</th>
                          <th>Answer</th>
                          <th>Edit</th>
                          <th>Delete</th>
                        </tr>
                      </thead>
                      <tbody>
                          <?php $faq_list = get_all_faq_list();
                          if($faq_list!=false){
                              $num=0;
                              foreach($faq_list->result() as $faq_data){
                                  $num++;
                                  ?>
                        <tr>
                          <td><?php echo $num;?></td>
                          <td><?php echo $faq_data->question;?></td>
                          <td><?php echo $faq_data->answer;?></td>
                            <td><p data-placement="top" data-toggle="tooltip" title="Edit"><a href="<?php echo base_url('faq_edit/'.$faq_data->id);?>" class="btn btn-primary btn-xs" data-title="Edit"  ><span class="glyphicon glyphicon-pencil"></span></a></p></td>
                            <td><p data-placement="top" data-toggle="tooltip" title="Hapus"><button class="btn btn-danger btn-xs delete_btn" data-toggle="modal" data-target="#delete_modal" id_url="<?php echo base_url('backend/delete_faq/'.$faq_data->id);?>" id_data="<?php echo $faq_data->id;?>"><span class="glyphicon glyphicon-trash"></span></button></p></td>
                        </tr>
                          <?php }} ?>
                      </tbody>
                    </table>
                  </div>
                </div>
                <div class="tab-pane fade in" id="add_member">
                  <form class="login100-form validate-form" method="post" action="<?php echo base_url('backend/create_faq');?>" id="create_user_form" enctype="multipart/form-data">
                      <input name="id" type="hidden" value="">
                      <div class="form-group">
                        <label>Question<span style="color:#f00">*</span></label>
                        <textarea class="form-control" name="question" id="question" rows="3" placeholder="Enter ..."></textarea>
                      </div>
                      <div class="form-group">
                        <label>Answer<span style="color:#f00">*</span></label>
                        <textarea class="form-control" name="answer" id="answer" rows="3" placeholder="Enter ..."></textarea>
                      </div>
                      <div class="footer-form">
                          <button type="submit" class="btn btn-success">Simpan</button>
                      </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>



</section>

<div id="delete_modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete Data</h4>
      </div>
      <div class="modal-body">
        Aoakah anda yakin untuk menghapus data ini
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <a  class="btn btn-danger" id="delete_footer" href="#">Ya</a>
      </div>
    </div>
  </div>
</div>
<style>
  .table-striped>tbody>tr:nth-of-type(odd) {
    background:#d2d2d2;
  }
</style>
