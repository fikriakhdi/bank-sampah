<section class="content list-content">
    <div class="row">
    <div class="col-md-12 pos-con">
        <div class="head-title">
            <h2><span class="fa fa-pencil" style="padding-right:10px"></span> Edit Pembelian</h2>
            <hr>
        </div>
        <a href="<?php echo base_url('pembelian');?>" class="btn btn-primary"><span class="fa fa-arrow-left"></span> Kembali</a>
        <div class="col-md-12 datatble-content">
            <form class="login100-form validate-form" method="post" action="<?php echo base_url('backend/pembelian_edit_process');?>" enctype="multipart/form-data">
                            <input name="id" type="hidden" value="<?php echo $data_edit->id_pembelian;?>">
                            <div class="form-group">
                              <label>Supplier<span style="color:#f00">*</span></label>
                            <select class="form-control" name="id_supplier" id="id_supplier" required>
                              <?php
                              $supplier_list = get_all_supplier_list();
                              if($supplier_list!=false){
                                echo '<option>Pilih Supplier</option>';
                                foreach($supplier_list->result() as $supplier){
                                  echo '<option value="'.$supplier->id_supplier.'">'.$supplier->nama_supplier.'</option>';
                                }
                              } else echo '<option>Maaf tidak ada pilihan supplier</option>';
                              ?>
                            </select>
                            <script>
                            $("#id_supplier").val("<?php echo $data_edit->id_supplier;?>").change();
                            </script>
                          </div>
                          <div class="form-group">
                            <label>Nama<span style="color:#f00">*</span></label>
                            <input type="text" class="form-control" name="nama_produk" placeholder="Ketik Nama" maxlength="150"  value="<?php echo $data_edit->nama_produk;?>" required>
                          </div>
                          <div class="form-group">
                            <label for="no_telepon">Jenis<span style="color:#f00">*</span></label>
                            <input type="text" class="form-control" name="jenis_produk" placeholder="Ketik Jenis" maxlength="150"  value="<?php echo $data_edit->jenis_produk;?>" required>
                          </div>
                          <div>
                            <label>Harga<span style="color:#f00">*</span></label>
                            <input type="number" class="form-control" name="harga_produk" placeholder="Ketik Harga"  value="<?php echo $data_edit->harga_produk;?>" required>
                          </div>
                          <div>
                            <label>Stok<span style="color:#f00">*</span></label>
                            <input type="number" class="form-control" name="stok_produk" placeholder="Ketik Stok"  value="<?php echo $data_edit->stok_produk;?>" required>
                          </div>
                          <div>
                            <label>Tanggal Pembelian<span style="color:#f00">*</span></label>
                            <input type="text" class="form-control datepicker" name="tanggal_pembelian" placeholder="Ketik Tanggal Pembelian" autocomplete="off" value="<?php echo $data_edit->tanggal_pembelian;?>" required>
                          </div>
                            <div class="footer-form">
                              <br>
                                <button type="submit" class="btn btn-success">Simpan</button>
                            </div>
            </form>

        </div>
    </div>
    </div>
</section>
