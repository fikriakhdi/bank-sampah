<section class="content list-content">
  <div class="row">
    <div class="col-md-12 pos-con">
      <div class="head-title">
        <h2><span class="fa fa-pencil" style="padding-right:10px"></span> Edit User</h2>
        <hr>
      </div>
      <a href="<?php echo base_url('user_manager');?>" class="btn btn-primary"><span class="fa fa-arrow-left"></span> Kembali</a>
      <div class="col-md-12 datatble-content">
        <form class="login100-form validate-form" id="edit_user_form" method="post" action="<?php echo base_url('backend/user_edit_process');?>" enctype="multipart/form-data" novalidate>
          <input name="id" type="hidden" value="<?php echo $data_edit->id;?>">
          <div class="form-group">
            <label>Pilih Role<span style="color:#f00">*</span></label>
            <select class="form-control" name="role" id="role" value="" required>
              <option>Pilih Role</option>
              <option value="member">Member</option>
              <option value="mitra">Mitra</option>
              <option value="admin">Admin</option>
            </select>
            <script>
              $(document).ready(function() {
                $("#role").val("<?php echo $data_edit->role;?>").change();
              });
            </script>
          </div>
          <div class="form-group">
            <label>Nama<span style="color:#f00">*</span></label>
            <input type="text" class="form-control" name="name" aria-describedby="emailHelp"  maxlength="150" value="<?php echo $data_edit->name;?>">
          </div>
          <div class="form-group">
            <label>Email<span style="color:#f00">*</span></label>
            <input type="email" class="form-control" name="email" id="email" aria-describedby="emailHelp"  maxlength="100"  value="<?php echo $data_edit->email;?>">
        </div>
          <div class="form-group">
            <label>Alamat<span style="color:#f00">*</span></label>
            <input type="text" class="form-control" name="alamat" id="alamat" aria-describedby="emailHelp" placeholder="Ketik Alamat" maxlength="100" value="<?php echo $data_edit->address;?>" required>
          </div>
          <div class="form-group ">
            <label>Provinsi<span style="color:#f00">*</span></label>
            <select class="form-control" name="provinceId" id="provinceId" data-url="<?php echo base_url('api/regencies');?>" required>
              <option>Pilih Provinsi</option>
              <?php
              $data_list = get_province_list();
              if($data_list!=false){
                  $num=0;
                  foreach($data_list->result() as $data){
                    $selected = '';
                    if($data_edit->provinceId==$data->id) $selected='selected';
                    echo '<option value="'.$data->id.'" '.$selected.'>'.$data->name.'</option>';
                  }
                }
              ?>
            </select>
          </div>
          
          <div class="form-group ">
            <label>Kota/Kabupaten<span style="color:#f00">*</span></label>
            <select class="form-control" name="regencyId" id="regencyId" data-url="<?php echo base_url('api/districts');?>" required>
              <option>Pilih Kota/Kabupaten</option>
              <?php
              $data_list = get_regency_list();
              if($data_list!=false){
                  $num=0;
                  foreach($data_list->result() as $data){
                    $selected = '';
                    if($data_edit->regencyId==$data->id) $selected='selected';
                    echo '<option value="'.$data->id.'" '.$selected.'>'.$data->name.'</option>';
                  }
                }
              ?>
            </select>
          </div>


          <!-- <div class="form-group ">
            <label>Kecamatan<span style="color:#f00">*</span></label>
            <select class="form-control" name="districtId" id="districtId" required>
              <option>Pilih Kecamatan</option>
              <?php
              // $data_list = get_province_list();
              // if($data_list!=false){
              //     $num=0;
              //     foreach($data_list->result() as $data){
              //       $selected = '';
              //       if($data_edit->provinceId==$data->id) $selected='selected';
              //       echo '<option value="'.$data->id.'" '.$selected.'>'.$data->name.'</option>';
              //     }
              //   }
              ?>
            </select>
          </div> -->
          <div  class="form-group">
            <label>Phone Number<span style="color:#f00">*</span></label>
            <input type="text" class="form-control" name="phoneNumber" value="<?php echo $data_edit->phoneNumber;?>">
            <!-- <input type="text" class="form-control" name="phoneNumber" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" > -->
          </div>
          <div  class="form-group <?php echo ($data_edit->role=='mitra'?'':'hidden');?>" id="no_ktp_group">
            <label>No. KTP<span style="color:#f00">*</span></label>
            <input type="text" class="form-control" name="ktp" id="no_ktp" value="<?php echo $data_edit->ktp;?>" onkeyup="this.value=this.value.replace(/[^\d]/,'')">
            <!-- <input type="text" class="form-control" name="phoneNumber" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" > -->
          </div>
          <div  class="form-group <?php echo ($data_edit->role=='mitra'?'':'hidden');?>" id="no_sim_group">
            <label>No. SIM<span style="color:#f00">*</span></label>
            <input type="text" class="form-control" name="sim" id="no_sim" value="<?php echo $data_edit->sim;?>" onkeyup="this.value=this.value.replace(/[^\d]/,'')">
            <!-- <input type="text" class="form-control" name="phoneNumber" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" > -->
          </div>
          <div  class="form-group<?php echo ($data_edit->role=='mitra'?'':'hidden');?>" id="masa_berlaku_sim_group">
            <label>Masa Berlaku SIM<span style="color:#f00">*</span></label>
            <input type="text" class="form-control datepicker" id="masa_berlaku_sim" name="masa_berlaku_sim" value="<?php echo date('d-m-Y', strtotime($data_edit->masa_berlaku_sim));?>">
            <!-- <input type="text" class="form-control" name="phoneNumber" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" > -->
          </div>
          <div  class="form-group <?php echo ($data_edit->role=='mitra'?'':'hidden');?>" id="no_plat_kendaraan_group">
            <label>No. Plat Kendaraan<span style="color:#f00">*</span></label>
            <input type="text" class="form-control " name="no_plat_kendaraan" id="no_plat_kendaraan" value="<?php echo $data_edit->no_plat_kendaraan;?>">
            <!-- <input type="text" class="form-control" name="phoneNumber" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" > -->
          </div>
          <div class="form-group <?php echo ($data_edit->role=='mitra'?'':'hidden');?>" id="tipe_kendaraan_group">
            <label>Tipe Kendaraan<span style="color:#f00">*</span></label>
            <select class="form-control" name="jenis_kendaraan" id="tipe_kendaraan" value="">
              <option>Pilih Tipe kendaraan</option>
              <option value="motor">Motor</option>
              <option value="mobil">Mobil</option>
              <option value="truk">Truk</option>
              <option value="difabel">Kendaraan Difabel</option>
            </select>
            <script>
              $(document).ready(function() {
                $("#tipe_kendaraan").val("<?php echo $data_edit->jenis_kendaraan;?>").change();
              });
            </script>
          </div>
          <div class="form-group">
            <label for="id_level">Status</label>
            <select class="form-control" name="status" id="status" aria-describedby="emailHelp" axlength="150" value="" required>
              <option value="">Pilih Status</option>
              <option value="active">Active</option>
              <option value="nonactive">Nonactive</option>
            </select>
            <script>
              $(document).ready(function() {
                $("#status").val("<?php echo $data_edit->status;?>").change();
              });
            </script>
          </div>
          <div class="footer-form">
            <button type="submit" class="btn btn-success">Simpan</button>
          </div>
        </form>

      </div>
    </div>
  </div>
</section>