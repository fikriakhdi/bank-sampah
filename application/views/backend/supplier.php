<section class="content list-content">
    <div class="row">
  <div class="col-md-12 pos-con">
    <div class="head-title">
      <h2><span class="fa fa-user"style="padding-right:10px"></span> Pemasok</h2>
      <hr>
    </div>
      <?php if(!empty($this->session->userdata('message'))) echo $this->session->userdata('message');?>
    <div class="col-md-12 datatble-content">
      <div class="clearfix">
      <div class="tabbable-panel margin-tops4  datatble-content">
        <div class="tabbable-line">
          <ul class="nav nav-tabs tabtop  tabsetting" class="align=center">
         <li> <a id="Pelanggan_list_menu" href="#Pelanggan_list" class="active" data-toggle="tab">List Pemasok</a> </li>
         <li> <a id="add_Pelanggan_menu" href="#add_Pelanggan" data-toggle="tab"> Tambah Pemasok</a> </li>
          </ul>
          <div class="tab-content margin-tops">
          <!--Tab1-->
            <div class="tab-pane active fade in" id="Pelanggan_list">
      <div class="content-datatable table-responsive">
        <table id="example" class="table table-striped table-bordered" style="width:100%">
          <thead>
            <tr class="title-datable">
              <th>NO</th>
              <th>Nama Perusahaan</th>
              <th>Nama</th>
              <th>No Telp</th>
              <th>Email</th>
              <th>Alamat</th>
              <th>Supplier</th>
              <th>Edit</th>
              <th>Delete</th>
            </tr>
          </thead>
          <tbody>
              <?php
              $data_list = get_all_supplier_list();
              if($data_list!=false){
                  $num=0;
                  foreach($data_list->result() as $data){
                      $num++;
                      ?>
            <tr>
              <td><?php echo $num;?></td>
              <td><?php echo $data->nama_perusahaan;?></td>
              <td><?php echo $data->nama_supplier;?></td>
              <td><?php echo $data->telp_supplier;?></td>
              <td><?php echo $data->email_supplier;?></td>
              <td><?php echo $data->alamat_supplier;?></td>
              <td><?php echo strtoupper($data->status);?></td>
                <td><p data-placement="top" data-toggle="tooltip" title="Edit"><a href="<?php echo base_url('backend/supplier_edit/'.$data->id_supplier);?>" class="btn btn-primary btn-xs" data-title="Edit"  ><span class="glyphicon glyphicon-pencil"></span></a></p></td>
                <td><p data-placement="top" data-toggle="tooltip" title="Hapus"><button class="btn btn-danger btn-xs delete_btn" data-toggle="modal" data-target="#delete_modal" id_url="<?php echo base_url('backend/delete_supplier/'.$data->id_supplier);?>"><span class="glyphicon glyphicon-trash"></span></button></p></td>
            </tr>
              <?php }} ?>
          </tbody>
        </table>
      </div>
    </div>
    <div class="tab-pane fade in" id="add_Pelanggan">
      <form class="login100-form validate-form" method="post" id="create_supplier_form" action="<?php echo base_url('backend/create_supplier');?>" enctype="multipart/form-data">
        <input name="id" type="hidden" value="">
        <div class="form-group">
          <label>Nama Perusahaan<span style="color:#f00">*</span></label>
          <input type="text" class="form-control" id="title" name="nama_perusahaan" aria-describedby="emailHelp" placeholder="Ketik nama perusahaan" maxlength="150"  value="" required>
        </div>
        <div class="form-group">
          <label>Nama<span style="color:#f00">*</span></label>
          <input type="text" class="form-control" id="title" name="nama_supplier" aria-describedby="emailHelp" placeholder="Ketik nama" maxlength="150"  value="" required>
        </div>
        <div class="form-group">
          <label for="no_telepon">Alamat<span style="color:#f00">*</span></label>
          <input type="text" class="form-control" id="alamat" name="alamat_supplier" aria-describedby="emailHelp" placeholder="Ketik Alamat" maxlength="150"  value="" required>
        </div>
        <div class="form-group">
          <label for="no_telepon">Email<span style="color:#f00">*</span></label>
          <input type="email" class="form-control" id="email" name="email_supplier" aria-describedby="emailHelp" placeholder="Ketik Email" maxlength="150"  value="" required>
          <span class="label label-danger" id="email_warn">Mohon hanya mengisi dengan email .com atau .co.id</span>
        </div>
        <div>
          <label for="telp">No. Telepon<span style="color:#f00">*</span></label>
          <input type="text" class="form-control" id="telp" name="telp_supplier" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" aria-describedby="emailHelp" placeholder="Ketik No Telepon" maxlength="13"  value="" required>
        </div>
          <div class="form-group">
            <label>Status<span style="color:#f00">*</span></label>
            <select class="form-control" name="status" aria-describedby="emailHelp"   value="" required>
                <option value="">Pilih Status</option>
                <option value="active">Active</option>
                <option value="nonactive">Nonactive</option>
              </select>
          </div>
          <div class="footer-form">
            <br>
              <button type="submit" class="btn btn-success">Simpan</button>
          </div>
      </form>
    </div>
  </div>
</div>
</div>
</div>
    </div>
  </div>
    </div>
</section>
<div id="delete_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Delete Data</h4>
        </div>
        <div class="modal-body">
          Aoakah anda yakin untuk menghapus data ini
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
          <a  class="btn btn-danger" id="delete_footer" href="#">Ya</a>
        </div>
      </div>
    </div>
  </div>
<style>
  .table-striped>tbody>tr:nth-of-type(odd) {
    background:#d2d2d2;
  }
</style>
<script>
  $(document).ready(function(){
    $("#email_warn").hide();
  })
$("#email").keyup(function(){
  var domain_co_id = $(this).val().substr($(this).length - 6); 
  var domain_com = $(this).val().substr($(this).length - 4); 
//   console.log(domain_com)
  if(domain_co_id=="co.id"){
    $("#email_warn").hide();
  } else if(domain_com=="com"){
    $("#email_warn").hide();
  } else {
    $("#email_warn").show();
  }
})
  
  $("#create_supplier_form").submit(function(e){
    var domain_co_id = $("#email").val().substr($("#email").length - 6); 
    var domain_com = $("#email").val().substr($("#email").length - 4); 
//     console.log(domain_com)
    if(domain_co_id=="co.id"){
      $("#email_warn").hide();
    } else if(domain_com=="com"){
      $("#email_warn").hide();
    } else {
      e.preventDefault();
      $("#email").focus();
      $("#email_warn").show();
    }
  })
</script>
