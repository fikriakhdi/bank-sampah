<section class="content list-content">
  <div class="row">
    <div class="col-md-12 pos-con">
      <div class="head-title">
        <h2><span class="fa fa-bars"style="padding-right:10px"></span> Trash Category</h2>
        <hr>
      </div>
        <?php if(!empty($this->session->userdata('message'))) echo $this->session->userdata('message');?>
      <div class="col-md-12">
        <div class="clearfix">
          <div class="tabbable-panel margin-tops4  datatble-content">
            <div class="tabbable-line">
              <ul class="nav nav-tabs tabtop  tabsetting" class="align=center">
                <li> <a id="member_list_menu" href="#member_list" class="active" data-toggle="tab">Trash Category List</a> </li>
                <li> <a id="add_member_menu" href="#add_member" data-toggle="tab"> Add Trash Category</a> </li>
              </ul>
              <div class="tab-content margin-tops">
                <!--Tab1-->
                <div class="tab-pane active fade in" id="member_list">
                  <div class="content-datatable table-responsive">
                    <table id="example" class="table table-striped table-bordered" style="width:100%">
                      <thead>
                        <tr class="title-datable">
                          <th>NO</th>
                          <th>Name</th>
                          <th>Unit</th>
                          <th>Point Value</th>
                          <th>Minimum Berat</th>
                          <th>Edit</th>
                          <th>Delete</th>
                        </tr>
                      </thead>
                      <tbody>
                          <?php $trashcategory_list = get_all_trashcategory_list();
                          if($trashcategory_list!=false){
                              $num=0;
                              foreach($trashcategory_list->result() as $trashcategory_data){
                                  $num++;
                                  ?>
                        <tr>
                          <td><?php echo $num;?></td>
                          <td><?php echo $trashcategory_data->name;?></td>
                          <td><?php echo strtoupper($trashcategory_data->unit_name);?></td>
                          <td><?php echo $trashcategory_data->point_value;?></td>
                          <td><?php echo $trashcategory_data->min_weight;?></td>
                            <td><p data-placement="top" data-toggle="tooltip" title="Edit"><a href="<?php echo base_url('trashcategory_edit/'.$trashcategory_data->id);?>" class="btn btn-primary btn-xs" data-title="Edit"  ><span class="glyphicon glyphicon-pencil"></span></a></p></td>
                            <td><p data-placement="top" data-toggle="tooltip" title="Hapus"><button class="btn btn-danger btn-xs delete_btn" data-toggle="modal" data-target="#delete_modal" id_url="<?php echo base_url('backend/delete_trashcategory/'.$trashcategory_data->id);?>" id_data="<?php echo $trashcategory_data->id;?>"><span class="glyphicon glyphicon-trash"></span></button></p></td>
                        </tr>
                          <?php }} ?>
                      </tbody>
                    </table>
                  </div>
                </div>
                <div class="tab-pane fade in" id="add_member">
                  <form class="login100-form validate-form" method="post" action="<?php echo base_url('backend/create_trashcategory');?>" id="create_user_form" enctype="multipart/form-data">
                      <input name="id" type="hidden" value="">
                      <div class="form-group">
                        <label>Name<span style="color:#f00">*</span></label>
                        <input type="text" class="form-control" name="name" id="name" required>
                      </div>
                      <div class="form-group">
                        <label>Unit<span style="color:#f00">*</span></label>
                        <select type="text" class="form-control" name="unit" id="unit" required>
                        <option value="">Select Unit</option>
                        <?php 
                        $data_list = get_all_unit_list();
                        if($data_list!=false){
                          foreach($data_list->result() as $row){
                            echo '<option value="'.$row->id.'">'.strtoupper($row->name).'</option>';
                          }
                        }
                        ?>
                        </select>
                      </div>
                      <div class="form-group">
                        <label>Point Value<span style="color:#f00">*</span></label>
                        <input type="number" class="form-control" name="point_value" id="point_value" step="0.01" required>
                      </div>
                      <div class="form-group">
                          <label>Minimum Berat<span style="color:#f00">*</span></label>
                          <input name="min_weight" type="number" class="form-control" value="" min="1" required>
                      </div>
                      <div class="footer-form">
                          <button type="submit" class="btn btn-success">Simpan</button>
                      </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>



</section>

<div id="delete_modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete Data</h4>
      </div>
      <div class="modal-body">
        Aoakah anda yakin untuk menghapus data ini
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <a  class="btn btn-danger" id="delete_footer" href="#">Ya</a>
      </div>
    </div>
  </div>
</div>
<style>
  .table-striped>tbody>tr:nth-of-type(odd) {
    background:#d2d2d2;
  }
</style>
