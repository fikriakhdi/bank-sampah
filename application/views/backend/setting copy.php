<section class="content list-content">
    <div class="row">
    <div class="col-md-12 pos-con">
        <div class="head-title">
            <h2><span class="fa fa-cog" style="padding-right:10px"></span> Setting</h2>
            <hr>
        </div>
      <?php if(!empty($this->session->userdata('message'))) echo $this->session->userdata('message');?>
        <div class="col-md-12 datatble-content">
            <form class="login100-form validate-form" method="post" action="<?php echo base_url('backend/save_setting');?>" enctype="multipart/form-data">
                            <div class="form-group">
                                <label>Pembagian Mitra<span style="color:#f00">*</span></label>
                                <input name="pembagian_mitra" type="number" class="form-control" value="<?php echo get_settings('pembagian_mitra');?>" min="1" required>
                            </div>
                            <div class="form-group">
                                <label>Pembagian Customer<span style="color:#f00">*</span></label>
                                <input name="pembagian_customer" type="number" class="form-control" value="<?php echo get_settings('pembagian_customer');?>" min="1" required>
                            </div>
                            <div class="form-group">
                                <label>Pembagian Pengepul<span style="color:#f00">*</span></label>
                                <input name="pembagian_pengepul" type="number" class="form-control" value="<?php echo get_settings('pembagian_pengepul');?>" min="1" required>
                            </div>
                            <div class="form-group">
                                <label>Harga Point<span style="color:#f00">*</span></label>
                                <input name="nilai_point" type="number" class="form-control" value="<?php echo get_settings('nilai_point');?>" min="1" required>
                            </div>
                            <div class="form-group">
                                <label>Maksimum Pickup<span style="color:#f00">*</span></label>
                                <input name="max_pickup" type="number" class="form-control" value="<?php echo get_settings('max_pickup');?>" min="1" required>
                            </div>
                            <div class="footer-form">
                              <br>
                                <button type="submit" class="btn btn-success">Save Changes</button>
                            </div>
            </form>

        </div>
    </div>
    </div>
</section>
