    <li id="dashboard">
      <a href="<?php echo base_url('');?>">
        <i class="fa fa-dashboard"></i> <span>Dashboard</span>
        <span class="pull-right-container">
        </span>
      </a>
    </li>

    <li><a href="<?php echo base_url('trash_request');?>"><i class="fa fa-trash"></i> Trash Request</a></li>
    <li><a href="<?php echo base_url('redeem');?>"><i class="fa fa-gift"></i> Redeem</a></li>
    <li><a href="<?php echo base_url('setting');?>"><i class="fa fa-cog"></i> Setting</a></li>
    <li class="treeview" style="height: auto;">
        <a href="">
        <i class="fa fa-folder"></i> <span>Mastert Data</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
        </a>
        <ul class="treeview-menu" style="display: none;">
          <li><a href="<?php echo base_url('user_manager');?>"><i class="fa fa-circle-o"></i> User</a></li>
          <li><a href="<?php echo base_url('news');?>"><i class="fa fa-circle-o"></i> News</a></li>
          <li><a href="<?php echo base_url('faq');?>"><i class="fa fa-circle-o"></i> FAQ</a></li>
          <li><a href="<?php echo base_url('redeemcategory');?>"><i class="fa fa-circle-o"></i>Redeem Category</a></li>
          <li><a href="<?php echo base_url('unit');?>"><i class="fa fa-circle-o"></i>Unit</a></li>
          <li><a href="<?php echo base_url('trashcategory');?>"><i class="fa fa-circle-o"></i>Trash Category</a></li>
        </ul>
    </li>
    
        <li class="treeview" style="height: auto;">
        <a href="">
        <i class="fa fa-file"></i> <span>Report</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
        </a>
        <ul class="treeview-menu" style="display: none;">
          <!-- <li><a href="<?php echo base_url('report/user_manager');?>"><i class="fa fa-circle-o"></i> User</a></li> -->
          <li><a href="<?php echo base_url('report/point');?>"><i class="fa fa-circle-o"></i> Point</a></li>
          <!-- <li><a href="<?php echo base_url('report/redeem');?>"><i class="fa fa-circle-o"></i> Redeem</a></li> -->
        </ul>
    </li>

