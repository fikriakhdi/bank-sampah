<section class="content list-content">
    <div class="row">
    <div class="col-md-12 pos-con">
        <div class="head-title">
            <h2><span class="fa fa-pencil" style="padding-right:10px"></span> Edit Penerimaan Produk</h2>
            <hr>
        </div>
        <a href="<?php echo base_url('penerimaan_produk');?>" class="btn btn-primary"><span class="fa fa-arrow-left"></span> Kembali</a>
        <div class="col-md-12 datatble-content">
            <form class="login100-form validate-form" method="post" action="<?php echo base_url('backend/penerimaan_produk_edit_process');?>" enctype="multipart/form-data">
                            <input name="id" type="hidden" value="<?php echo $data_edit->id_produk;?>">
                            <input name="id_pembelian_before" type="hidden" value="<?php echo $data_edit->id_pembelian;?>">
                            <div class="form-group">
                              <label>Pembelian<span style="color:#f00">*</span></label>
                              <select class="form-control" name="id_pembelian" id="id_pembelian" required>
                                <?php
                                $data_list = get_all_pembelian_list();
                                if($data_list!=false){
                                  echo '<option>Pilih Pembelian</option>';
                                  foreach($data_list->result() as $data){
                                    if($data->id_pembelian==$data_edit->id_pembelian)
                                    echo '<option value="'.$data->id_pembelian.'">'.$data->nama_produk.' (Jenis: '.$data->jenis_produk.', Rp.'.$data->harga_produk.', Qty : '.$data->kuantitas.')</option>';
                                    if($data->status==0 && $data->id_pembelian!=$data_edit->id_pembelian)
                                    echo '<option value="'.$data->id_pembelian.'">'.$data->nama_produk.' (Jenis: '.$data->jenis_produk.', Rp.'.$data->harga_produk.', Qty : '.$data->kuantitas.')</option>';
                                  }
                                } else echo '<option>Maaf tidak ada pilihan pembelian</option>';
                                ?>
                              </select>
                              <script>
                              $("#id_pembelian").val('<?php echo $data_edit->id_pembelian;?>').change();
                              </script>
                            </div>
                            <div class="footer-form">
                              <br>
                                <button type="submit" class="btn btn-success">Simpan</button>
                            </div>
            </form>

        </div>
    </div>
    </div>
</section>
