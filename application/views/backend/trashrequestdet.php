<section class="content list-content">
    <div class="row">
  <div class="col-md-12 pos-con">
    <div class="head-title">
      <h2><span class="fa fa-trash"style="padding-right:10px"></span> Trash Request</h2>
      <hr>
    </div>
      <?php if(!empty($this->session->userdata('message'))) echo $this->session->userdata('message');?>
    
      <a href="<?php echo base_url('trash_request');?>" class="btn btn-primary"><span class="fa fa-arrow-left"></span> Kembali</a>
        <div class="col-md-12 datatble-content">
      <div class="clearfix">
      <div class="content-datatable table-responsive">
      
      <table id="example" class="table table-striped table-bordered" style="width:100%">
          <thead>
            <tr class="title-datable">
              <th>NO</th>
              <th>Kategori</th>
              <th>Satuan</th>
              <th>Berat</th>
            </tr>
          </thead>
          <tbody>
              <?php
              $where = "WHERE A.trashRequestId = '".$id."'";
              $data_list = get_all_trash_request_det($where);
              if($data_list!=false){
                  $num=0;
                  foreach($data_list->result() as $data){
                      $num++;
                      ?>
            <tr>
              <td><?php echo $num;?></td>
              <td><?php echo $data->category_name;?></td>
              <td><?php echo strtoupper($data->unit_name);?></td>
              <td><?php echo $data->weight;?></td>
        </tr>
              <?php }} ?>
          </tbody>
        </table>
      </div>
</div>
    </div>
  </div>
    </div>
</section>

<style>
  .table-striped>tbody>tr:nth-of-type(odd) {
    background:#d2d2d2;
  }
</style>