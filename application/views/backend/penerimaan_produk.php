<section class="content list-content">
  <div class="col-md-12 pos-con">
    <div class="head-title">
      <h2><span class="fa fa-product-hunt"style="padding-right:10px"></span>Penerimaan Produk</h2>
      <hr>
    </div>
      <?php if(!empty($this->session->userdata('message'))) echo $this->session->userdata('message');?>
    <div class="col-md-12 datatble-content">
      <div class="clearfix">
      <div class="tabbable-panel margin-tops4  datatble-content">
        <div class="tabbable-line">
          <ul class="nav nav-tabs tabtop  tabsetting" class="align=center">
         <li> <a id="penerimaan_produk_list_menu" href="#penerimaan_produk_list" class="active" data-toggle="tab">Penerimaan Produk List</a> </li>
         <li> <a id="add_penerimaan_produk_menu" href="#add_penerimaan_produk" data-toggle="tab"> Add Penerimaan Produk</a> </li>
          </ul>
          <div class="tab-content margin-tops">
          <!--Tab1-->
            <div class="tab-pane active fade in" id="penerimaan_produk_list">
      <div class="content-datatable table-responsive">
        <table id="example" class="table table-striped table-bordered" style="width:100%">
          <thead>
            <tr class="title-datable">
              <th>NO</th>
              <th>Nama Produk</th>
              <th>Jenis Produk</th>
              <th>Satuan Produk</th>
              <th>Harga Produk</th>
              <th>Stok Produk</th>
<!--               <th>Tanggal Pembelian</th> -->
              <th>Tanggal Penerimaan</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
              <?php
              $data_list = get_all_produk_list();
              if($data_list!=false){
                  $num=0;
                  foreach($data_list->result() as $data){
                      $num++;
                      ?>
            <tr>
              <td><?php echo $num;?></td>
              <td><?php echo $data->nama_produk;?></td>
              <td><?php echo $data->jenis_produk;?></td>
              <td><?php echo $data->satuan_produk;?></td>
              <td><?php echo money($data->harga_produk);?></td>
              <td><?php echo $data->stok_produk;?></td>
<!--               <td><?php //echo date('d-m-Y', strtotime($data->tanggal_pembelian));?></td> -->
              <td><?php echo date('d-m-Y', strtotime($data->tanggal_penerimaan));?></td>
                <td>
                <?php if($data->status_active_penerimaan_produk=="active") { ?>
               <p data-placement="top" data-toggle="tooltip" title="Non Aktif">
                 <a class="btn btn-danger btn-xs"  href="<?php echo base_url('backend/nonactive_penerimaan_produk/'.$data->id_penerimaan_produk);?>"><span class="fa fa-ban"></span></a>
               </p>
               <?php } else { ?>
               <p data-placement="top" data-toggle="tooltip" title="Aktif">
                 <a class="btn btn-success btn-xs" href="<?php echo base_url('backend/active_penerimaan_produk/'.$data->id_penerimaan_produk);?>"><span class="fa fa-check"></span></a>
               </p>
               <?php } ?>
              </td>
            </tr>
              <?php }} ?>
          </tbody>
        </table>
      </div>
    </div>
    <div class="tab-pane fade in" id="add_penerimaan_produk">
      <form class="login100-form validate-form" method="post" action="<?php echo base_url('backend/create_produk');?>" enctype="multipart/form-data">
        <input name="id" type="hidden" value="">
        <div class="form-group">
          <label>Pembelian<span style="color:#f00">*</span></label>
          <select class="form-control" name="id_pembelian" required>
            <?php
            $data_list = get_all_pembelian_list();
            if($data_list!=false){
              echo '<option>Pilih Pembelian</option>';
              foreach($data_list->result() as $data){
                if($data->status_active_pembelian){
                if($data->id_pembelian==$data_edit->id_pembelian)
                echo '<option value="'.$data->id_pembelian.'">N'.$data->nama_produk.' (Jenis: '.$data->jenis_produk.', Rp.'.$data->harga_produk.', Qty : '.$data->kuantitas.')</option>';
                if($data->status==0 && $data->id_pembelian!=$data_edit->id_pembelian)
                echo '<option value="'.$data->id_pembelian.'">'.$data->nama_produk.' (Jenis: '.$data->jenis_produk.', Rp.'.$data->harga_produk.', Qty : '.$data->kuantitas.')</option>';
                }
                }
            } else echo '<option>Maaf tidak ada pilihan pembelian</option>';
            ?>
          </select>
        </div>
        <div class="form-group">
          <label>Tanggal Penerimaan<span style="color:#f00">*</span></label>
          <input value="<?php echo date('Y-m-d');?>" name="tanggal_penerimaan" class="form-control" readonly>
        </div>
          <div class="footer-form">
            <br>
              <button type="submit" class="btn btn-success">Simpan</button>
          </div>
      </form>
    </div>
  </div>
</div>
</div>
</div>
    </div>
  </div>
</section>
<div id="delete_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Delete Data</h4>
        </div>
        <div class="modal-body">
          Aoakah anda yakin untuk menghapus data ini
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
          <a  class="btn btn-danger" id="delete_footer" href="#">Ya</a>
        </div>
      </div>
    </div>
  </div>
<style>
  .table-striped>tbody>tr:nth-of-type(odd) {
    background:#d2d2d2;
  }
</style>
