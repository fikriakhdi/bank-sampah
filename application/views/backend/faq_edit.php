<section class="content list-content">
    <div class="row">
    <div class="col-md-12 pos-con">
        <div class="head-title">
            <h2><span class="fa fa-pencil" style="padding-right:10px"></span> Edit FAQ</h2>
            <hr>
        </div>
        <a href="<?php echo base_url('faq');?>" class="btn btn-primary"><span class="fa fa-arrow-left"></span> Kembali</a>
        <div class="col-md-12 datatble-content">
            <form class="login100-form validate-form" method="post" action="<?php echo base_url('backend/faq_edit_process');?>" enctype="multipart/form-data">
                    <input name="id" type="hidden" value="<?php echo $data_edit->id;?>">
                      <div class="form-group">
                        <label>Question<span style="color:#f00">*</span></label>
                        <textarea class="form-control" name="question" id="question" rows="3" value=""><?php echo $data_edit->question;?></textarea>
                      </div>
                      <div class="form-group">
                        <label>Answer<span style="color:#f00">*</span></label>
                        <textarea class="form-control" name="answer" id="answer" rows="3" value=""><?php echo $data_edit->answer;?></textarea>
                      </div>
                      <div class="footer-form">
                          <button type="submit" class="btn btn-success">Simpan</button>
                      </div>
            </form>

        </div>
    </div>
    </div>
</section>
