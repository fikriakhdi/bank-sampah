<?php
class redeem_model extends CI_Model{

  var $detail_pembelian         = 'detail_pembelian';
  var $redeem                  = 'redeem';
  var $pelanggan                = 'pelanggan';
  var $pembelian                = 'pembelian';
  var $penjualan                = 'penjualan';
  var $product                  = 'product';
  var $suplier                  = 'suplier';
  var $surat_jalan              = 'surat_jalan';
  var $user                     = 'user';
  public function __construct(){
            parent::__construct();
             $this->load->database();
         }
    function create_redeem($data){
        $this->db->insert($this->redeem,$data);
        $flag=$this->db->insert_id();
        return $flag;
    }
    function read_redeem($where=""){
        $this->db->select("*");
        if($where!="")
        $this->db->where($where);
        $this->db->from($this->redeem);
        $query=$this->db->get();
        return $query;
    }
    function update_redeem($data){
        $this->db->where('id',$data['id']);
        $this->db->update($this->redeem,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function delete_redeem($id){
        $this->db->where('id',$id);
        $this->db->delete($this->redeem);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function custom_sql($sql){
      return $this->db->query($sql);
    }
}
?>
