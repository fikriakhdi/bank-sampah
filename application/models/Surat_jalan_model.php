<?php
class surat_jalan_model extends CI_Model{

  var $detail_pembelian         = 'detail_pembelian';
  var $detail_penjualan         = 'detail_penjualan';
  var $pelanggan                = 'pelanggan';
  var $pembelian                = 'pembelian';
  var $penjualan                = 'penjualan';
  var $product                  = 'product';
  var $surat_jalan              = 'surat_jalan';
  public function __construct(){
            parent::__construct();
             $this->load->database();
         }
    function create_surat_jalan($data){
        $this->db->insert($this->surat_jalan,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function read_surat_jalan($where=""){
        $this->db->select("surat_jalan.*, penjualan.*, pelanggan.nama_pelanggan");
        if($where!="")
        $this->db->where($where);
        $this->db->from($this->surat_jalan);
        $this->db->join($this->penjualan, "penjualan.id_penjualan=surat_jalan.id_penjualan");
        $this->db->join($this->pelanggan, 'pelanggan.id_pelanggan = penjualan.id_pelanggan');
        $query=$this->db->get();
        return $query;;
    }
    function update_surat_jalan($data){
        $this->db->where('id_surat_jalan',$data['id_surat_jalan']);
        $this->db->update($this->surat_jalan,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function delete_surat_jalan($id){
        $this->db->where('id',$id);
        $this->db->delete($this->surat_jalan);
        $flag=$this->db->affected_rows();
        return $flag;
    }
  function delete_surat_penjualan_by_penjualan($id){
        $this->db->where('id_penjualan',$id);
        $this->db->delete($this->surat_jalan);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function custom_sql($sql){
      return $this->db->query($sql);
    }
    function read_single_sj($id_penjualan=""){
        $sql = "SELECT * FROM ".$this->surat_jalan." WHERE id_penjualan='".$id_penjualan."'";
        $query = $this->db->query($sql);
        return $query;
    }
}
?>
