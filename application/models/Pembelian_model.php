<?php
class pembelian_model extends CI_Model{

  var $detail_pembelian         = 'detail_pembelian';
  var $detail_penjualan         = 'detail_penjualan';
  var $pelanggan                = 'pelanggan';
  var $pembelian                = 'pembelian';
  var $pembelian_produk         = 'pembelian_produk';
  var $penjualan                = 'penjualan';
  var $product                  = 'product';
  var $pemasok                  = 'pemasok';
  var $surat_jalan              = 'surat_jalan';
  public function __construct(){
            parent::__construct();
             $this->load->database();
         }
    function create_pembelian($data){
        $this->db->insert($this->pembelian,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function read_pembelian($where=""){
        $this->db->select("pembelian.*, pembelian_produk.*, pembelian.status_active status_active_pembelian");
        if($where!="")
        $this->db->where($where);
        $this->db->from($this->pembelian);
        $this->db->join($this->pembelian_produk, 'pembelian_produk.id_pembelian_produk = pembelian.id_pembelian_produk');
        $query=$this->db->get();
        return $query;
    }
    function update_pembelian($data){
        $this->db->where('id_pembelian',$data['id_pembelian']);
        $this->db->update($this->pembelian,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function delete_pembelian($id){
        $this->db->where('id_pembelian',$id);
        $this->db->delete($this->pembelian);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function custom_sql($sql){
      return $this->db->query($sql);
    }
}
?>
