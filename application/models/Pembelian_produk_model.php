<?php
class pembelian_produk_model extends CI_Model{

  var $detail_pembelian_produk         = 'detail_pembelian_produk';
  var $detail_penjualan         = 'detail_penjualan';
  var $pelanggan                = 'pelanggan';
  var $pembelian_produk                = 'pembelian_produk';
  var $penjualan                = 'penjualan';
  var $product                  = 'product';
  var $pemasok                  = 'pemasok';
  var $surat_jalan              = 'surat_jalan';
  public function __construct(){
            parent::__construct();
             $this->load->database();
         }
    function create_pembelian_produk($data){
        $this->db->insert($this->pembelian_produk,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function read_pembelian_produk($where=""){
        $this->db->select("*");
        if($where!="")
        $this->db->where($where);
        $this->db->from($this->pembelian_produk);
        $query=$this->db->get();
        return $query;
    }
  function read_pembelian_produk_detail($where=""){
        $sql = "select pembelian_produk.*, pemasok.nama_perusahaan, pemasok.nama_pemasok, pembelian_produk.status_active status_active_pembelian_produk from pembelian_produk
        
        left join pemasok on pemasok.id_pemasok = pembelian_produk.id_pemasok
        ";
        if($where!="") $sql.=" where ".$where;
        $query = $this->db->query($sql);
        return $query;
    }
    function update_pembelian_produk($data){
        $this->db->where('id_pembelian_produk',$data['id_pembelian_produk']);
        $this->db->update($this->pembelian_produk,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function delete_pembelian_produk($id){
        $this->db->where('id_pembelian_produk',$id);
        $this->db->delete($this->pembelian_produk);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function custom_sql($sql){
      return $this->db->query($sql);
    }
    function read_min_stok(){
        $sql = "SELECT * FROM `pembelian_produk` WHERE stok_produk<=min_stok";
     return $this->db->query($sql);
    }
}
?>
