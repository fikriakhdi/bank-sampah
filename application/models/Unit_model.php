<?php
class unit_model extends CI_Model{
  var $unit                     = 'unit';
  
  public function __construct(){
            parent::__construct();
             $this->load->database();
         }
    function create_unit($data){
        $this->db->insert($this->unit,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function read_unit($where=""){
        $this->db->select("*");
        if($where!="")
        $this->db->where($where);
        $this->db->from($this->unit);
        $query=$this->db->get();
        return $query;
    }
    function update_unit($data){
        $this->db->where('id',$data['id']);
        $this->db->update($this->unit,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function delete_unit($id){
        $this->db->where('id',$id);
        $this->db->delete($this->unit);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function custom_sql($sql){
      return $this->db->query($sql);
    }
}
?>
