<?php
class penjualan_model extends CI_Model{

  var $detail_pembelian         = 'detail_pembelian';
  var $detail_penjualan         = 'detail_penjualan';
  var $pelanggan                = 'pelanggan';
  var $pembelian                = 'pembelian';
  var $penjualan                = 'penjualan';
  var $produk                   = 'produk';
  var $pemasok                 = 'pemasok';
  var $surat_jalan              = 'surat_jalan';
  public function __construct(){
            parent::__construct();
             $this->load->database();
         }
    function create_penjualan($data){
        $this->db->insert($this->penjualan,$data);
        $flag=$this->db->insert_id();
        return $flag;
    }
    function read_penjualan($where=""){
        $this->db->select("penjualan.*, pelanggan.nama_pelanggan ");
        if($where!="")
        $this->db->where($where);
        $this->db->from($this->penjualan);
        $this->db->join($this->pelanggan, 'pelanggan.id_pelanggan = penjualan.id_pelanggan');
        $query=$this->db->get();
        return $query;
    }
    function update_penjualan($data){
        $this->db->where('id_penjualan',$data['id_penjualan']);
        $this->db->update($this->penjualan,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function delete_penjualan($id){
        $this->db->where('id_penjualan',$id);
        $this->db->delete($this->penjualan);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function custom_sql($sql){
      return $this->db->query($sql);
    }
}
?>
