<?php
class districts_model extends CI_Model{
  var $districts                     = 'districts';
  
  public function __construct(){
            parent::__construct();
             $this->load->database();
         }
    function create_districts($data){
        $this->db->insert($this->districts,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function read_districts($where=""){
        $this->db->select("*");
        if($where!="")
        $this->db->where($where);
        $this->db->from($this->districts);
        $query=$this->db->get();
        return $query;
    }
    function update_districts($data){
        $this->db->where('id',$data['id']);
        $this->db->update($this->districts,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function delete_districts($id){
        $this->db->where('id',$id);
        $this->db->delete($this->districts);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function custom_sql($sql){
      return $this->db->query($sql);
    }
}
?>
