<?php
class faq_model extends CI_Model{
  var $faq                     = 'faq';
  
  public function __construct(){
            parent::__construct();
             $this->load->database();
         }
    function create_faq($data){
        $this->db->insert($this->faq,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function read_faq($where=""){
        $this->db->select("*");
        if($where!="")
        $this->db->where($where);
        $this->db->from($this->faq);
        $query=$this->db->get();
        return $query;
    }
    function update_faq($data){
        $this->db->where('id',$data['id']);
        $this->db->update($this->faq,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function delete_faq($id){
        $this->db->where('id',$id);
        $this->db->delete($this->faq);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function custom_sql($sql){
      return $this->db->query($sql);
    }
}
?>
