<?php
class session_model extends CI_Model{

  var $detail_pembelian         = 'detail_pembelian';
  var $detail_penjualan         = 'detail_penjualan';
  var $pelanggan                = 'pelanggan';
  var $pembelian                = 'pembelian';
  var $penjualan                = 'penjualan';
  var $product                  = 'product';
  var $session                  = 'session';
  var $surat_jalan              = 'surat_jalan';
  public function __construct(){
            parent::__construct();
             $this->load->database();
         }
    function create_session($data){
        $this->db->insert($this->session,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function read_session($where=""){
        $this->db->select("*");
        if($where!="")
        $this->db->where($where);
        $this->db->from($this->session);
        $query=$this->db->get();
        return $query;
    }
    function update_session($data){
        $this->db->where('id_session',$data['id_session']);
        $this->db->update($this->session,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function delete_session($key){
        $this->db->where('key',$key);
        $this->db->delete("session");
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function custom_sql($sql){
      return $this->db->query($sql);
    }
}
?>
