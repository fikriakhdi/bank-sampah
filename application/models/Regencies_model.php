<?php
class regencies_model extends CI_Model{
  var $regencies                     = 'regencies';
  
  public function __construct(){
            parent::__construct();
             $this->load->database();
         }
    function create_regencies($data){
        $this->db->insert($this->regencies,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function read_regencies($where=""){
        $this->db->select("*");
        if($where!="")
        $this->db->where($where);
        $this->db->from($this->regencies);
        $query=$this->db->get();
        return $query;
    }
    function update_regencies($data){
        $this->db->where('id',$data['id']);
        $this->db->update($this->regencies,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function delete_regencies($id){
        $this->db->where('id',$id);
        $this->db->delete($this->regencies);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function custom_sql($sql){
      return $this->db->query($sql);
    }
}
?>
