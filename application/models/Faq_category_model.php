<?php
class faq_category_model extends CI_Model{
  var $faq_category                     = 'faq_category';
  
  public function __construct(){
            parent::__construct();
             $this->load->database();
         }
    function create_faq_category($data){
        $this->db->insert($this->faq_category,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function read_faq_category($where=""){
        $this->db->select("*");
        if($where!="")
        $this->db->where($where);
        $this->db->from($this->faq_category);
        $query=$this->db->get();
        return $query;
    }
    function update_faq_category($data){
        $this->db->where('id',$data['id']);
        $this->db->update($this->faq_category,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function delete_faq_category($id){
        $this->db->where('id',$id);
        $this->db->delete($this->faq_category);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function custom_sql($sql){
      return $this->db->query($sql);
    }
}
?>
