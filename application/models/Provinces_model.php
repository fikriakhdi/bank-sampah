<?php
class provinces_model extends CI_Model{
  var $provinces                     = 'provinces';
  
  public function __construct(){
            parent::__construct();
             $this->load->database();
         }
    function create_provinces($data){
        $this->db->insert($this->provinces,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function read_provinces($where=""){
        $this->db->select("*");
        if($where!="")
        $this->db->where($where);
        $this->db->from($this->provinces);
        $query=$this->db->get();
        return $query;
    }
    function update_provinces($data){
        $this->db->where('id',$data['id']);
        $this->db->update($this->provinces,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function delete_provinces($id){
        $this->db->where('id',$id);
        $this->db->delete($this->provinces);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function custom_sql($sql){
      return $this->db->query($sql);
    }
}
?>
