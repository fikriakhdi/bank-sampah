<?php
class pemasok_model extends CI_Model{

  var $detail_pembelian         = 'detail_pembelian';
  var $detail_penjualan         = 'detail_penjualan';
  var $pelanggan                = 'pelanggan';
  var $pembelian                = 'pembelian';
  var $penjualan                = 'penjualan';
  var $product                  = 'product';
  var $pemasok                  = 'pemasok';
  var $surat_jalan              = 'surat_jalan';
  public function __construct(){
            parent::__construct();
             $this->load->database();
         }
    function create_pemasok($data){
        $this->db->insert($this->pemasok,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function read_pemasok($where=""){
        $this->db->select("*");
        if($where!="")
        $this->db->where($where);
        $this->db->from($this->pemasok);
        $query=$this->db->get();
        return $query;
    }
    function update_pemasok($data){
        $this->db->where('id_pemasok',$data['id_pemasok']);
        $this->db->update($this->pemasok,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function delete_pemasok($id){
        $this->db->where('id_pemasok',$id);
        $this->db->delete($this->pemasok);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function custom_sql($sql){
      return $this->db->query($sql);
    }
}
?>
