<?php
class setting_model extends CI_Model{

  var $detail_pembelian         = 'detail_pembelian';
  var $detail_penjualan         = 'detail_penjualan';
  var $pelanggan                = 'pelanggan';
  var $pembelian                = 'pembelian';
  var $penjualan                = 'penjualan';
  var $product                  = 'product';
  var $setting                  = 'setting';
  var $surat_jalan              = 'surat_jalan';
  public function __construct(){
            parent::__construct();
             $this->load->database();
         }
    function create_setting($data){
        $this->db->insert($this->setting,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function read_setting($name){
        $this->db->select("*");
        $where = array('name'=>$name);
        $this->db->where($where);
        $this->db->from($this->setting);
        $query=$this->db->get();
        return $query;
    }
    function update_setting($name, $value){
        $this->db->where('name',$name);
        $data = array("name"=>$name, "value"=>$value);
        $this->db->update($this->setting,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function delete_setting($id){
        $this->db->where('id_setting',$id);
        $this->db->delete($this->setting);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function custom_sql($sql){
      return $this->db->query($sql);
    }
}
?>
