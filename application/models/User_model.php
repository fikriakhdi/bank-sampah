<?php
class user_model extends CI_Model{

  var $detail_pembelian         = 'detail_pembelian';
  var $detail_penjualan         = 'detail_penjualan';
  var $user                     = 'user';
  var $pembelian                = 'pembelian';
  var $penjualan                = 'penjualan';
  var $product                  = 'product';
  var $suplier                  = 'suplier';
  var $surat_jalan              = 'surat_jalan';
  public function __construct(){
            parent::__construct();
             $this->load->database();
         }
    function create_user($data){
        $this->db->insert($this->user,$data);
        $flag=$this->db->insert_id();
        return $flag;
    }
    function read_user($where=""){
        $this->db->select("*");
        if($where!="")
        $this->db->where($where);
        $this->db->from($this->user);
        $query=$this->db->get();
        return $query;
    }
    function update_user($data){
        $this->db->where('id',$data['id']);
        $this->db->update($this->user,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function delete_user($id){
        $this->db->where('id',$id);
        $this->db->delete($this->user);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function custom_sql($sql){
      return $this->db->query($sql);
    }
}
?>
