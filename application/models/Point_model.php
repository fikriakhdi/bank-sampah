<?php
class point_model extends CI_Model{

  var $detail_pembelian         = 'detail_pembelian';
  var $point                  = 'point';
  var $pelanggan                = 'pelanggan';
  var $pembelian                = 'pembelian';
  var $penjualan                = 'penjualan';
  var $product                  = 'product';
  var $suplier                  = 'suplier';
  var $surat_jalan              = 'surat_jalan';
  var $user                     = 'user';
  public function __construct(){
            parent::__construct();
             $this->load->database();
         }
    function create_point($data){
        $this->db->insert($this->point,$data);
        $flag=$this->db->insert_id();
        return $flag;
    }
    function read_point($where=""){
        $this->db->select("*");
        if($where!="")
        $this->db->where($where);
        $this->db->from($this->point);
        $query=$this->db->get();
        return $query;
    }
    function update_point($data){
        $this->db->where('id',$data['id']);
        $this->db->update($this->point,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function delete_point($id){
        $this->db->where('id',$id);
        $this->db->delete($this->point);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function custom_sql($sql){
      return $this->db->query($sql);
    }
}
?>
