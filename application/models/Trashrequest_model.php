<?php
class trashrequest_model extends CI_Model{
    var $trashrequest                     = 'trashrequest';

  public function __construct(){
            parent::__construct();
             $this->load->database();
         }
    function create_trashrequest($data){
        $this->db->insert($this->trashrequest,$data);
        $flag=$this->db->insert_id();
        return $flag;
    }
    function read_trashrequest($where=""){
        $this->db->select("*");
        if($where!="")
        $this->db->where($where);
        $this->db->from($this->trashrequest);
        $query=$this->db->get();
        return $query;
    }

    function read_user($where=""){
        $this->db->select("*");
        if($where!="")
        $this->db->where($where);
        $this->db->from($this->user);
        $query=$this->db->get();
        return $query;
    }

    function update_trashrequest($data){
        $this->db->where('id',$data['id']);
        $this->db->update($this->trashrequest,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function delete_trashrequest($id){
        $this->db->where('id',$id);
        $this->db->delete($this->trashrequest);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function custom_sql($sql){
      return $this->db->query($sql);
    }
}
?>
