<?php
class villages_model extends CI_Model{
  var $villages                     = 'villages';
  
  public function __construct(){
            parent::__construct();
             $this->load->database();
         }
    function create_villages($data){
        $this->db->insert($this->villages,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function read_villages($where=""){
        $this->db->select("*");
        if($where!="")
        $this->db->where($where);
        $this->db->from($this->villages);
        $query=$this->db->get();
        return $query;
    }
    function update_villages($data){
        $this->db->where('id',$data['id']);
        $this->db->update($this->villages,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function delete_villages($id){
        $this->db->where('id',$id);
        $this->db->delete($this->villages);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function custom_sql($sql){
      return $this->db->query($sql);
    }
}
?>
