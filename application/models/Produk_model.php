<?php
class produk_model extends CI_Model{

  var $produk         = 'produk';
  var $detail_penjualan         = 'detail_penjualan';
  var $pelanggan                = 'pelanggan';
  var $pembelian                = 'pembelian';
  var $pembelian_produk         = 'pembelian_produk';
  var $penjualan                = 'penjualan';
  var $product                  = 'product';
  var $suplier                  = 'suplier';
  var $surat_jalan              = 'surat_jalan';
  public function __construct(){
            parent::__construct();
             $this->load->database();
         }
    function create_produk($data){
        $this->db->insert($this->produk,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function read_produk($where=""){
        $this->db->select("produk.*, pembelian.*, pembelian_produk.*");
        if($where!="")
        $this->db->where($where);
        $this->db->from($this->produk);
        $this->db->join($this->pembelian, "pembelian.id_pembelian=produk.id_pembelian");
        $this->db->join($this->pembelian_produk, 'pembelian_produk.id_pembelian_produk = pembelian.id_pembelian_produk');
        $query=$this->db->get();
        return $query;
    }
    function update_produk($data){
        $this->db->where('id_produk',$data['id_produk']);
        $this->db->update($this->produk,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function delete_produk($id){
        $this->db->where('id_produk',$id);
        $this->db->delete($this->produk);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function custom_sql($sql){
      return $this->db->query($sql);
    }
}
?>
