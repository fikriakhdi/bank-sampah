<?php
class redeemcategory_model extends CI_Model{
    var $redeemcategory                     = 'redeemcategory';

  public function __construct(){
            parent::__construct();
             $this->load->database();
         }
    function create_redeemcategory($data){
        $this->db->insert($this->redeemcategory,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function read_redeemcategory($where=""){
        $this->db->select("*");
        if($where!="")
        $this->db->where($where);
        $this->db->from($this->redeemcategory);
        $query=$this->db->get();
        return $query;
    }

    function read_user($where=""){
        $this->db->select("*");
        if($where!="")
        $this->db->where($where);
        $this->db->from($this->user);
        $query=$this->db->get();
        return $query;
    }

    function update_redeemcategory($data){
        $this->db->where('id',$data['id']);
        $this->db->update($this->redeemcategory,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function delete_redeemcategory($id){
        $this->db->where('id',$id);
        $this->db->delete($this->redeemcategory);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function custom_sql($sql){
      return $this->db->query($sql);
    }
}
?>
