<?php
class pelanggan_model extends CI_Model{

  var $detail_pembelian         = 'detail_pembelian';
  var $detail_pelanggan         = 'detail_pelanggan';
  var $pelanggan                = 'pelanggan';
  var $pembelian                = 'pembelian';
  var $product                  = 'product';
  var $suplier                  = 'suplier';
  var $surat_jalan              = 'surat_jalan';
  var $point                    = 'point';
  public function __construct(){
            parent::__construct();
             $this->load->database();
         }
    function create_pelanggan($data){
        $this->db->insert($this->pelanggan,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function read_pelanggan($where=""){
      $sql = "
      SELECT pelanggan.* from pelanggan";
      $query = $this->db->query($sql);
        return $query;
    }
    function update_pelanggan($data){
        $this->db->where('id_pelanggan',$data['id_pelanggan']);
        $this->db->update($this->pelanggan,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function delete_pelanggan($id){
        $this->db->where('id_pelanggan',$id);
        $this->db->delete($this->pelanggan);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function custom_sql($sql){
      return $this->db->query($sql);
    }
}
?>
