<?php
class transaksi_model extends CI_Model{

  var $detail_pembelian         = 'detail_pembelian';
  var $transaksi                = 'transaksi';
  var $pelanggan                = 'pelanggan';
  var $pembelian                = 'pembelian';
  var $penjualan                = 'penjualan';
  var $product                  = 'product';
  var $suplier                  = 'suplier';
  var $surat_jalan              = 'surat_jalan';
  var $user                     = 'user';
  public function __construct(){
            parent::__construct();
             $this->load->database();
         }
    function create_transaksi($data){
        $this->db->insert($this->transaksi,$data);
        $flag=$this->db->insert_id();
        return $flag;
    }
    function read_transaksi($where=""){
        $this->db->select("transaksi.*, user.username username");
        if($where!="")
        $this->db->where($where);
        $this->db->from($this->transaksi);
        $this->db->join($this->user, "user.id_user=transaksi.id_user");
        $query=$this->db->get();
        return $query;
    }
    function update_transaksi($data){
        $this->db->where('id_transaksi',$data['id_transaksi']);
        $this->db->update($this->transaksi,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function delete_transaksi($id){
        $this->db->where('id_transaksi',$id);
        $this->db->delete($this->transaksi);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function custom_sql($sql){
      return $this->db->query($sql);
    }
}
?>
