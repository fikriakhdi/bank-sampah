<?php
class trashcategory_model extends CI_Model{
    var $trashcategory                     = 'trashcategory';

  public function __construct(){
            parent::__construct();
             $this->load->database();
         }
    function create_trashcategory($data){
        $this->db->insert($this->trashcategory,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function read_trashcategory($where=""){
        $this->db->select("*");
        if($where!="")
        $this->db->where($where);
        $this->db->from($this->trashcategory);
        $query=$this->db->get();
        return $query;
    }

    function read_user($where=""){
        $this->db->select("*");
        if($where!="")
        $this->db->where($where);
        $this->db->from($this->user);
        $query=$this->db->get();
        return $query;
    }

    function update_trashcategory($data){
        $this->db->where('id',$data['id']);
        $this->db->update($this->trashcategory,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function delete_trashcategory($id){
        $this->db->where('id',$id);
        $this->db->delete($this->trashcategory);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function custom_sql($sql){
      return $this->db->query($sql);
    }
}
?>
