<?php
class penjualan_detail_model extends CI_Model{

  var $penjualan_detail         = 'penjualan_detail';
  var $pelanggan                = 'pelanggan';
  var $pembelian                = 'pembelian';
  var $pembelian_produk         = 'pembelian_produk';
  var $penjualan                = 'penjualan';
  var $produk                   = 'penerimaan_produk';
  var $suplier                  = 'suplier';
  var $surat_jalan              = 'surat_jalan';
  public function __construct(){
            parent::__construct();
             $this->load->database();
         }
    function create_penjualan_detail($data){
        $this->db->insert($this->penjualan_detail,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function read_penjualan_detail($where=""){
        $this->db->select("penjualan_detail.*, penerimaan_produk.*, pembelian.*, pembelian_produk.*");
        if($where!="")
        $this->db->where($where);
        $this->db->from($this->penjualan_detail);
        $this->db->join($this->produk, "penerimaan_produk.id_produk=penjualan_detail.id_produk");
        $this->db->join($this->pembelian, "pembelian.id_pembelian=penerimaan_produk.id_pembelian");
        $this->db->join($this->pembelian_produk, 'pembelian_produk.id_pembelian_produk = pembelian.id_pembelian_produk');
        $query=$this->db->get();
        return $query;
    }
    function update_penjualan_detail($data){
        $this->db->where('id_penjualan_detail',$data['id_penjualan_detail']);
        $this->db->update($this->penjualan_detail,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function delete_penjualan_detail($id){
        $this->db->where('id_penjualan_detail',$id);
        $this->db->delete($this->penjualan_detail);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function custom_sql($sql){
      return $this->db->query($sql);
    }
}
?>
