<?php
class news_model extends CI_Model{
  var $news                     = 'news';
  
  public function __construct(){
            parent::__construct();
             $this->load->database();
         }
    function create_news($data){
        $this->db->insert($this->news,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function read_news($where=""){
        $this->db->select("*");
        if($where!="")
        $this->db->where($where);
        $this->db->from($this->news);
        $query=$this->db->get();
        return $query;
    }
    function update_news($data){
        $this->db->where('id',$data['id']);
        $this->db->update($this->news,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function delete_news($id){
        $this->db->where('id',$id);
        $this->db->delete($this->news);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function custom_sql($sql){
      return $this->db->query($sql);
    }
}
?>
