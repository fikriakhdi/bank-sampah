$(".add_pelanggan").change(function(e){
  e.preventDefault();
  var point = $('option:selected', this).data('point');
  $("#total_point").val(point)
})

$("#provinceId").change(function(e){
  var province_id = $(this).val()
  var url = $(this).data('url')+"?province_id="+province_id
  if(email){
    $.get(url,  function (data) {
      var result = JSON.parse(data)
      if (result.status == 200) {
        var data = result.data
  $("#regencyId").empty();
  $("#regencyId").
   append($('<option/>')  // Create new <option> element
   .val("")            // Set value as "Hello"
   .text("Select a Kota"))
   console.log(result.data)
   result.data.map((o, i)=>{
    $("#regencyId").
    append($('<option/>')  // Create new <option> element
    .val(o.id)            // Set value as "Hello"
    .text(o.name))
   })
      }
    })
  }
})

$("#regencyId").change(function(e){
  var regency_id = $(this).val()
  var url = $(this).data('url')+"?regency_id="+regency_id
  if(email){
    $.get(url, function (data) {
      var result = JSON.parse(data)
      if (result.status == 200) {
        var data = result.data
  $("#districtId").empty();
  $("#districtId").
   append($('<option/>')  // Create new <option> element
   .val("")            // Set value as "Hello"
   .text("Pilih Kecamatan"))
   data.map((o, i)=>{
    $("#districtId").
    append($('<option/>')  // Create new <option> element
    .val(o.id)            // Set value as "Hello"
    .text(o.name))
   })
      }
    })
  }
})

$(".count_total_penjualan").keyup(function(e){
  e.preventDefault();
  var total_product = $("#jumlah_product").val();
  var total_price = 0;
  for(var i=1;i<=total_product;i++){
    if($("#harga_jual_"+i).val() && $("#qty_"+i).val()){
    var price = parseInt($("#harga_jual_"+i).val());
    console.log(price)
    var qty = parseInt($("#qty_"+i).val());
    console.log(qty)
    var total = price*qty;
    total_price+=total;
    }
  }
  $("#total_penjualan").val("Rp"+formatMoney(total_price));
})
$("#role").change(function(){
  var value = $(this).val()
  if(value=="member"){
  $("#no_ktp_group").hide()
  $("#no_ktp").attr("required", "")
  $("#no_sim_group").hide()
  $("#no_sim").attr("required", "")
  $("#masa_berlaku_sim_group").hide()
  $("#masa_berlaku_sim").attr("required", "")
  $("#no_plat_kendaraan_group").hide()
  $("#no_plat_kendaraan").attr("required", "")
  $("#tipe_kendaraan_group").hide()
  $("#tipe_kendaraan").attr("required", "")
  } else {
    $("#no_ktp_group").show()
    $("#no_ktp").attr("required", "required")
    $("#no_sim_group").show()
    $("#no_sim").attr("required", "required")
    $("#masa_berlaku_sim_group").show()
    $("#masa_berlaku_sim").attr("required", "required")
    $("#no_plat_kendaraan_group").show()
    $("#no_plat_kendaraan").attr("required", "required")
    $("#tipe_kendaraan_group").show()
    $("#tipe_kendaraan").attr("required", "required")
  }
})
$(".count_total_penjualan").change(function(e){
  e.preventDefault();
  var total_product = $("#jumlah_product").val();
  var total_price = 0;
  for(var i=1;i<=total_product;i++){
    if($("#harga_jual_"+i).val() && $("#qty_"+i).val()){
    var price = parseInt($("#harga_jual_"+i).val());
    console.log(price)
    var qty = parseInt($("#qty_"+i).val());
    console.log(qty)
    var total = price*qty;
    total_price+=total;
    }
  }
  $("#total_penjualan").val("Rp."+formatMoney(total_price));
})
function formatMoney(amount, decimalCount = 0, decimal = ".", thousands = ",") {
  try {
    decimalCount = Math.abs(decimalCount);
    decimalCount = isNaN(decimalCount) ? 0 : decimalCount;

    const negativeSign = amount < 0 ? "-" : "";

    let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
    let j = (i.length > 3) ? i.length % 3 : 0;

    return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
  } catch (e) {
    console.log(e)
  }
}

$(".check_stock").keyup(function(e){
  e.preventDefault();
  var data_id = $(this).data('id');
  console.log(data_id)
  var stock = $(this).data('stock');
    console.log(stock)
  if($(this).val()>stock){
    $("#warn_box").text("Kuantitas tidak boleh melebihi stock");
    $("#warn_box").removeClass("hidden");
  } else {
    $("#warn_box").addClass("hidden");
  }
})

$(".set_pembelian").change(function(){
  var idpembelian = $(this).data('idpembelian');
  $("#id_pembelian").val(idpembelian)
})
