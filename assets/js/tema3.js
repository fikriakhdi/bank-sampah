window.onscroll = function() {myFunction()};

var navbar = document.getElementById("navbar");
var sticky = navbar.offsetTop;

function myFunction() {
  if (window.pageYOffset >= sticky) {
    navbar.classList.add("sticky")
  } else {
    navbar.classList.remove("sticky");
  }
}
$(".pos-card-pro-tema2").hover(function(e){
    e.preventDefault();
    var id_desc = $(this).attr("data-desc");
    var id_title = $(this).attr("data-title");
    var id_icon = $(this).attr("data-icon");
    $("#"+id_desc).show();
    $("#"+id_desc).css("color", "white");
    $(".body-card-tema2").css("color", "white");
    $("#"+id_title).css("color", "white");
    $("#"+id_icon).css("-webkit-filter", "brightness(1000%)");
    $(".body-card-tema2").css("text-decoration", "none");
    
},function(e){
    e.preventDefault();
    var id_desc = $(this).attr("data-desc");
    var id_title = $(this).attr("data-title");
    var id_icon = $(this).attr("data-icon");
    $("#"+id_desc).hide();
    $("#"+id_title).css("color", "#333");
    $("#"+id_icon).css("-webkit-filter", "brightness(100%)");
    $(".body-card-tema2").css("color", "#333");
})
